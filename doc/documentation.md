# Ein Jump'n'Run

## Framework

Der Einstieg in eine Programm im Framework für das WP Computergrafik erfolgt über die *main()*-Methode in der Klasse `Application`. Dort wird ein Fenster erstellt. Der eigentliche Inhalt und der Zugriff auf OpenGL sind über ein Objekt der Klasse `Scene` umgesetzt. In der `Scene` wiederum wird ein Szenengraph verwaltet. Beispiele für Knoten im Szenengraph sind `RotationNode` (für Rotationen) oder `TriangleMeshNode` zur Repräsentation eines Dreiecknetzes.

Für das Spiel gibt es bereits eine Klasse `PlatformerScene` die von `Scene` erbt und die Grundlagen des Spiels beinhaltet.  

## Einleitung

Bei dem Spiel handelt es sich um ein Jump'n'Run. Man steuert eine
Spielfigur (dargestellt durch ein Eichhörnchen-Dreiecksnetz). Parallel dazu wird der Fortlauf des Spiels durch die Bewegung der Kamera
dargestellt. Im Spiel gibt es Segmente linearer Bewegung (`GameSegment`), die durch einen
Startpunkt und eine Richtung vorgegeben werden. Im Kern des Spiels steht die
Klasse `Game`, die alle Komponenten des Spiels zusammenführt.

## Framework

Die Integration
in das umschließende Framework wird durch das `Scene`-Objekt `PlatformerScene`
umgesetzt. Diese Szene lässt sich dann in einer beliebigen Anwendung des
Frameworks verwenden. Es ist zu empfehlen, in der Desktop-Umgebung zu arbeiten,
da die Implementierung auf Keyboard-Input setzt und dort das Kompilieren und
Debuggen dort einfacher funktioniert.

## Die Welt

Die Welt des Spiels besteht aus einem Gitter aus Klötzchen, sogenannten Bricks (`Brick`).
Alle Bricks haben die gleiche Kantenlänge. Das Gitter ist definiert durch
die Anzahl der Zeilen (in x-Richtung), die Anzahl der Spalten (in z-Richtung)
und einen maximalen Offset, der angibt, wie viele Bricks maximal auf den Ebenen
über bzw. unter der Nullebene liegen (in y-Richtung). Der Brick (4,3,-1) liegt
demnach in Zeile 4, Spalte 3 und eine Ebene unterhalb der Nullebene. Es gibt unterschiedliche Typen von Bricks. Die Unterscheidung erfolgt durch den Typ. Einige Typen sind bereits vorgegeben: `Brick.Type`. Für die Bricks, die von einem Spielsegment auf das nächste wechseln, gibt es eine eigene Klasse: `SwitchBrick`.

![Die Welt als Gitter aus Bricks](world.png)

## Zustand des Spiel

Der dynamische Zustand eines Spiels wird in dem `DynamicGameState` festgehalten. Im dynamischen Zustand wird insbesondere das Fortlaufen der Segmente repräsentiert. Aus dem Zustand kann man direkt die aktuellen Position und die Orientierung für die Festlegung der dynamischen Kamera abfragen.

## Ereignisse

Im Spiel treten unterschiedliche Ereignisse auf, die vielleicht an einer ganz anderen Stelle verarbeitet werden. Solche Ereignisse werden durch Objekte der Klasse `GameEvent` repräsentiert. Um ein Ereignis abzusetzen erzeugt man ein `GameEvent`-Objekt übergibt es an den `GameEventQueue`-Singleton: `GameEventQueue.getInstance().emitEvent(...)`


## GameObjects

Neben den statischen Bricks der Welt gibt es auch dynamische Objekte: `GameObjects`. Das `Game`-Objekt verwaltet dazu eine Liste von `GameObjects`. Einige `GameObjects`
sind bereits vorgegeben: `Player`, `Monster`, `Coin`, `Hazelnut`. Ein `GameObject` kombiniert die Logik hinter dem Objekt und die Darstellung. Für zweiteres erbt sie direkt von dem Szenengraph-Knoten `TranslationNode`. Somit kann ein `GameObject` direkt in den Szenengraphen gehängt werden und seine Darstellung, z.B. ein Dreiecksnetz, beinhalten.

### Player
Die Klasse `Player`representiert die Spielfigur.

### Coin
Die Klasse `Coin`representiert die Münzen, die im Laufe des Spiels eingesammelt werden können.

### Monster
`Monster` sind die Gegner der Spielfigur. Kollidiert die Spielfigur mit einerm Monster, so verliert sie eins ihrer Leben. Monster können mit Haselnüssen bekämpft werden.

### Hazelnut
Die Spielfigur kann Haselnüsse werfen. Trifft eine Haselnuss auf ein Monster, so verschwindet das Monster aus dem Spiel.

## Plugins

Das Spiel hat eine Plugin-Architektur. Zusätzliche Funktionalität kann dem Spiel
durch die Registrierung einer Plugin-Instanz injiziert werden. Plugins erben
von der abstrakten Klasse `PlatformerPlugin`. Ein Plugin ist bereits vorgegeben: das `BackgroundPlugin` dient der Darstellung des Hintergrundbildes. 

## Heartbeat

Das Spiel hat einen Herzschlag, eine innere Endlosschleife. In jedem Durchlauf
wird dazu die Methode `updateGameState()` des `Game`-Objektes aufgerufen. Diese
reicht den Aufruf an alle Plugins und alle verwalteten `GameObjects` weiter.

## Steuerung

* Pfeil nach rechts: rechts
* Pfeil nach links: links
* Pfeil nach oben: nach oben springen
* Pfeil nach rechts + oben: nach rechts springen
* Pfeil nach links + oben: nach links springen
* P: Pause
* Space: Haselnuss werfen

## Serialisierung, JSON, I/O

Die Komponenten eines Spiels lassen sich in einer JSON-Datei ablegen. Alle Objekte eines Spiels, die Informationen aus einer JSON-Datei lesen können, implementieren das Interface `JsonSerializable`.