/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;

import wp_cg.backend.jogl.ComputergraphicsWindow;
import wp_cg.backend.misc.Constants;
import wp_cg.backend.misc.Logger;
import wp_cg.backend.misc.Scene;
import wp_cg.platformer.PlatformerScene;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFrame;

/**
 * Central frame for all applications
 *
 * @author Philipp Jenke
 */
public class Application extends JFrame {
    private static final long serialVersionUID = 2322862744369751274L;

    /**
     * 3D view object.
     */
    private final ComputergraphicsWindow view;

    private Scene scene = null;

    /**
     * Constructor
     */
    public Application() {
        Logger.getInstance().setup(
                msg -> System.out.println("[" + Constants.LOGTAG + "] " + FORMAT.format(new Date()) +  " (" + Thread.currentThread().getId() + ") " + msg));

        scene = new PlatformerScene("shader/vertex_shader_jogl.glsl",
                "shader/fragment_shader_jogl.glsl");

        GLCapabilities capabilities = new GLCapabilities(GLProfile.getDefault());
        view = new ComputergraphicsWindow(capabilities, scene);

        getContentPane().add(view);
        view.requestFocusInWindow();

        // Setup JFrame
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("WP Computergrafik");
        setSize(960, 540);
        setVisible(true);
    }

    /**
     * Program entry point.
     */
    public static void main(String[] args) {
        new Application();
    }
    
    private static final DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
}

