/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.jogl;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.math.FloatUtil;
import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.misc.Logger;
import wp_cg.backend.rendering.Shader;

/**
 * Generic interface to encapsulate platform specific OpenGL commands.
 */
public class OpenGL {

  private static OpenGL instance = null;

  public static OpenGL instance() {
    if ( instance == null ){
      instance = new OpenGL();
    }
    return  instance;
  }

  public enum DataType {
        UNSIGNED_INT, FLOAT
    }

    public enum Primitive {
        TRIANGLES,
        LINES, POINTS, LINE_LOOP
    }

    private GL2 gl = null;

    public Vector fixTexCoord(Vector texCoord) {
        return new Vector(texCoord.x(), texCoord.y());
    }

    public String getVersionString() {
        String version_string = gl.glGetString(GL2.GL_VERSION);
        String result = "OpenGL-Version: " + version_string + ", ";
        int[] number = {0};
        gl.glGetIntegerv(GL2.GL_STENCIL_BITS, number, 0);
        result += "Stencil buffer bits: " + number[0] + ", ";
        gl.glGetIntegerv(GL2.GL_DEPTH_BITS, number, 0);
        result += "Depth buffer bits: " + number[0] + ", ";
        gl.glGetIntegerv(GL2.GL_RED_BITS, number, 0);
        result += "Red buffer bits: " + number[0] + ", ";
        gl.glGetIntegerv(GL2.GL_GREEN_BITS, number, 0);
        result += "Green buffer bits: " + number[0] + ", ";
        gl.glGetIntegerv(GL2.GL_BLUE_BITS, number, 0);
        result += "Blue buffer bits: " + number[0];
        return result;
    }

    public void enableStencilTest(boolean flag) {
        if (flag) {
            gl.glEnable(GL2.GL_STENCIL_TEST);
            Logger.getInstance().log("Stencil test: on");
        } else {
            gl.glDisable(GL2.GL_STENCIL_TEST);
            Logger.getInstance().log("Stencil test: off");
        }
    }

    public void setupCulling() {
        gl.glCullFace(GL2.GL_BACK);
        gl.glFrontFace(GL2.GL_CCW);
    }

    public void enableDepthTest(boolean flag) {
        if (flag) {
            gl.glEnable(GL2.GL_DEPTH_TEST);
            Logger.getInstance().log("Depth test: on");
        } else {
            gl.glDisable(GL2.GL_DEPTH_TEST);
            Logger.getInstance().log("Depth test: off");
        }
    }

    public void enableCulling(boolean flag) {
        if (flag) {
            gl.glEnable(GL2.GL_CULL_FACE);
            Logger.getInstance().log("Culling: on");
        } else {
            gl.glDisable(GL2.GL_CULL_FACE);
            Logger.getInstance().log("Culling: off");
        }
    }

    public void enableBlending(boolean flag) {
        if (flag) {
            gl.glEnable(GL2.GL_BLEND);
            gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
            Logger.getInstance().log("Blending: on");
        } else {
            gl.glDisable(GL2.GL_BLEND);
            Logger.getInstance().log("Blending: off");
        }
    }

    public void setupBlending() {
        gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
    }

    public void setGLObject(Object gl) {
        if (gl instanceof GL2) {
            this.gl = (GL2) gl;
        } else {
            Logger.getInstance().log("Invalid GL2 object");
        }
    }

    public void enableVertexAttribArray(int location) {
        gl.glEnableVertexAttribArray(location);
    }

    public void glVertexAttribPointer(int location, int size, DataType type, boolean normalized,
                                      int stride, FloatBuffer buffer) {
        gl.glVertexAttribPointer(
                location, size, convert2JoglDataType(type),
                normalized, stride, buffer);
    }

    public void glDrawElements(Primitive primitiveType, int size, DataType dataType, IntBuffer buffer) {
        gl.glDrawElements(convert2JoglPrimitiveType(primitiveType), size, convert2JoglDataType(dataType), buffer);
    }

    public void glUseProgram(int program) {
        gl.glUseProgram(program);
    }

    private int convert2JoglDataType(DataType type) {
        switch (type) {
            case FLOAT:
                return GL2.GL_FLOAT;
            case UNSIGNED_INT:
                return GL2.GL_UNSIGNED_INT;
            default:
                throw new IllegalArgumentException("Invalid data type: " + type);
        }
    }

    private int convert2JoglPrimitiveType(Primitive primitive) {
        switch (primitive) {
            case TRIANGLES:
                return GL2.GL_TRIANGLES;
            case LINE_LOOP:
                return GL2.GL_LINE_LOOP;
            case LINES:
                return GL2.GL_LINES;
            case POINTS:
                return GL2.GL_POINTS;
            default:
                throw new IllegalArgumentException("Unsupported primitive: " + primitive);
        }
    }

    /**
     * Shader constants.
     */
    private static final int COMPILE_STATUS_OK = 1;

    public int glCreateProgram() {
        return gl.glCreateProgram();
    }

    public void glAttachShader(int shaderProgram, int shaderId) {
        gl.glAttachShader(shaderProgram, shaderId);
    }

    public void glLinkProgram(int shaderProgram) {
        gl.glLinkProgram(shaderProgram);
    }

    public void glValidateProgram(int shaderProgram) {
        gl.glValidateProgram(shaderProgram);
    }

    public int glCreateShader(Shader.ShaderType shaderType) {
        switch (shaderType) {
            case VERTEX:
                return gl.glCreateShader(GL2.GL_VERTEX_SHADER);
            case FRAGMENT:
                return gl.glCreateShader(GL2.GL_FRAGMENT_SHADER);
            default:
                throw new IllegalArgumentException("Invalid shader type");
        }
    }

    public void glShaderSource(int id, String shaderSource) {
        gl.glShaderSource(id, 1, new String[]{shaderSource}, (int[]) null, 0);
    }

    public void glCompileShader(int id) {
        gl.glCompileShader(id);
    }

    public boolean checkCompileError(int id) {
        IntBuffer intBuffer = IntBuffer.allocate(1);
        gl.glGetShaderiv(id, GL2.GL_COMPILE_STATUS, intBuffer);
        boolean error = intBuffer.get(0) != COMPILE_STATUS_OK;
        if (error) {
            Logger.getInstance().log(getCompileErrorMessage(id));
        }
        return error;

    }

    public int glGetAttribLocation(int shaderProgram, String name) {
        return gl.glGetAttribLocation(shaderProgram, name);
    }

    public int glGetUniformLocation(int shaderProgram, String name) {
        return gl.glGetUniformLocation(shaderProgram, name);
    }

    public void glUniform3f(int locationCameraPosition, float x, float y, float z) {
        gl.glUniform3f(locationCameraPosition, x, y, z);
    }

    public void glUniformMatrix4fv(int location, int i, boolean b, float[] floats, int i1) {
        gl.glUniformMatrix4fv(location, i, b, floats, i1);
    }

    public void glUniform1i(int location, int value) {
        gl.glUniform1i(location, value);
    }

    public Matrix generateViewMatrix(Vector eye, Vector ref, Vector up) {
        float[] m = new float[16];
        float[] tmp = new float[16];
        FloatUtil.makeLookAt(m, 0, eye.floatData(), 0, ref.floatData(),
                0, up.floatData(), 0, tmp);
        return new Matrix(m);
    }

    public Matrix generateOrthoMatrix(int i, int i1, int i2, int i3, int i4, int i5, int i6) {
        throw new IllegalArgumentException("not implemented yet.");
    }

    public void glLineWidth(int width) {
        gl.glLineWidth(width);
    }

    public void checkGlError() {
        checkGlError("");
    }

    public void clearColorBuffer() {
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
    }

    public void glBindTexture(int textureId) {
        gl.glBindTexture(GL2.GL_TEXTURE_2D, textureId);
    }

    public void glDeleteTextures(int textureId) {
        final int[] textureHandle = new int[1];
        textureHandle[0] = textureId;
        gl.glDeleteTextures(1, textureHandle, 0);
    }

    public GL2 getOpenGLObject() {
        return gl;
    }

    public void checkGlError(String msg) {
        Map<Integer, String> glErrorMap = new HashMap<Integer, String>();
        glErrorMap.put(GL2.GL_NO_ERROR, "GL_NO_ERROR");
        glErrorMap.put(GL2.GL_INVALID_ENUM, "GL_INVALID_ENUM");
        glErrorMap.put(GL2.GL_INVALID_VALUE, "GL_INVALID_VALUE");
        glErrorMap.put(GL2.GL_INVALID_OPERATION, "GL_INVALID_OPERATION");
        glErrorMap.put(GL2.GL_OUT_OF_MEMORY, "GL_OUT_OF_MEMORY");
        glErrorMap.put(GL2.GL_INVALID_FRAMEBUFFER_OPERATION,
                "GL_INVALID_FRAMEBUFFER_OPERATION");
        int err = GL2.GL_NO_ERROR;
        do {
            err = gl.glGetError();
            if (err != GL2.GL_NO_ERROR) {
                if (glErrorMap.containsKey(err)) {
                    Logger.getInstance().log("OpenGL ES error (" + msg + "): " + glErrorMap.get(err));
                } else {
                    Logger.getInstance().log("OpenGL ES error (" + msg + "): " + err);
                }
            }
        } while (err != GL2.GL_NO_ERROR);
    }

    /**
     * Extract the error message.
     */
    private String getCompileErrorMessage(int id) {
        IntBuffer intBuffer = IntBuffer.allocate(1);
        gl.glGetShaderiv(id, GL2.GL_INFO_LOG_LENGTH, intBuffer);
        int size = intBuffer.get(0);
        String errorMessage = "";
        if (size > 0) {
            ByteBuffer byteBuffer = ByteBuffer.allocate(size);
            gl.glGetShaderInfoLog(id, size, intBuffer, byteBuffer);
            for (byte b : byteBuffer.array()) {
                errorMessage += (char) b;
            }
        }
        return errorMessage;
    }
}
