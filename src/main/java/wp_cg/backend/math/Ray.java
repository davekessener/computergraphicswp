/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.math;

/**
 * Represents a ray from a starting point towards a direction.
 * @author Philipp Jenke
 */
public class Ray {
    /**
     * Ray start
     */
    private Vector start;

    /**
     * Normalized direction vector.
     */
    private Vector dir;

    public Ray(Vector start, Vector dir){
        this.start = start;
        this.dir = dir.getNormalized();
    }

    /**
     * Return distance between ray and point
     */
    public double getDistance(Vector p){
        double lambda = p.subtract(start).multiply(dir);
        Vector q = start.add(dir.multiply(lambda));
        double distance = q.subtract(p).getNorm();
        return distance;
    }
}
