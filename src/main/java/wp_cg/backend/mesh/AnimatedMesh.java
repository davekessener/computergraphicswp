/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */
package wp_cg.backend.mesh;

import wp_cg.backend.math.Matrix;
import wp_cg.backend.scenegraph.InnerNode;

public class AnimatedMesh extends InnerNode {
  /**
   * Indicates the current frame
   */
  private int currentFrameIndex = 0;

  /**
   * Number of redraws the keyframe stays the same.
   */
  private int numIterationsSameFrame = 5;

  /**
   * Counts the redraws, used for frame update.
   */
  private int iterationCounter = 0;

  @Override
  public synchronized void traverse(Matrix modelMatrix) {
    if (!isActive()) {
      return;
    }

    iterationCounter++;
    if (iterationCounter > numIterationsSameFrame) {
      iterationCounter = 0;
      currentFrameIndex++;
      if (currentFrameIndex >= getNumberOfChildren()) {
        currentFrameIndex = 0;
      }
    }

    getChild(currentFrameIndex).traverse(modelMatrix);
  }
}
