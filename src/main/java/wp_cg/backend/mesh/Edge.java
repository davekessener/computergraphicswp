/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */
package wp_cg.backend.mesh;

/**
 * Represents an edge in a triangle mesh.
 */
public class Edge {
  /**
   * Index of teh first vertex.
   */
  public int a;

  /**
   * Index of the second vertex.
   */
  public int b;

  public Edge(int a, int b) {
    this.a = a;
    this.b = b;
  }

  @Override
  public boolean equals(Object other) {
    if (!(other instanceof Edge)) {
      return false;
    }
    Edge otherEdge = (Edge) other;
    return (a == otherEdge.a && b == otherEdge.b)
        || (a == otherEdge.b && b == otherEdge.a);
  }

  @Override
  public int hashCode() {
    return a + b;
  }

  @Override
  public String toString() {
    return "(" + a + ", " + b + ")";
  }

  /**
   * Flip end vertices.
   */
  public void Flip() {
    int tmp = b;
    b = a;
    a = tmp;
  }
}
