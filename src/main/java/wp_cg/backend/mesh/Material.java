/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */
package wp_cg.backend.mesh;

import wp_cg.backend.math.Vector;

/**
 * Represents an OBJ-file material.
 */
public class Material {
    private String name;
    private String textureFilename = null;
    private Vector color = new Vector(1, 1, 1, 1);

    public Material(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Vector getColor() {
        return color;
    }

    public void setColor(Vector color) {
        this.color = color;
    }

    public String getTextureFilename() {
        return textureFilename;
    }

    public void setTextureFilename(String filename) {
        this.textureFilename = filename;
    }

    @Override
    public String toString() {
        return name + ": " + color + ", " + textureFilename;
    }
}
