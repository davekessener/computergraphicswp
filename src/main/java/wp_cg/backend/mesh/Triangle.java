/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 * <p>
 * Basis Framework für das "WP Computergrafik".
 */
package wp_cg.backend.mesh;

import wp_cg.backend.math.Vector;
import wp_cg.backend.misc.Logger;

/**
 * Representation of a triangle consisting of three indices. The indices
 * reference vertices in the vertex list in a triangle mesh.
 *
 * @author Philipp Jenke
 */
public class Triangle {

  /**
   * Indices of the vertices.
   */
  private int[] vertexIndices = {-1, -1, -1};

  /**
   * Triangle color in RGBA format.
   */
  protected Vector color = new Vector(0.5, 0.5, 0.5, 1);

  /**
   * Facet normal
   */
  protected Vector normal = new Vector(0, 1, 0);

  /**
   * Indices of the texture coordinates.
   */
  protected int[] texCoordIndices = {-1, -1, -1};


  public Triangle() {
  }

  public Triangle(int a, int b, int c, int tA, int tB, int tC, Vector color, Vector normal) {
    vertexIndices[0] = a;
    vertexIndices[1] = b;
    vertexIndices[2] = c;
    texCoordIndices[0] = tA;
    texCoordIndices[1] = tB;
    texCoordIndices[2] = tC;
    this.color = new Vector(color);
    this.normal = new Vector(normal);
  }

  public Triangle(int a, int b, int c) {
    this(a, b, c, -1, -1, -1, new Vector(1, 0, 0), new Vector(1, 0, 0));
  }

  public Triangle(int a, int b, int c, int tA, int tB, int tC) {
    this(a, b, c, tA, tB, tC, new Vector(1, 0, 0), new Vector(1, 0, 0));
  }


  public Triangle(Triangle triangle) {
    this(triangle.vertexIndices[0],
            triangle.vertexIndices[1],
            triangle.vertexIndices[2],
            triangle.texCoordIndices[0],
            triangle.texCoordIndices[1],
            triangle.texCoordIndices[2],
            triangle.color, triangle.normal);
  }

  @Override
  public String toString() {
    return String.format("Triangle");
  }

  public int getVertexIndex(int index) {
    return vertexIndices[index];
  }

  /**
   * Add an offset to all vertex indices.
   */
  public void addVertexIndexOffset(int offset) {
    for (int i = 0; i < 3; i++) {
      vertexIndices[i] += offset;
    }
  }

  @Override
  public Triangle clone() {
    Triangle triangle = new Triangle();
    triangle.setVertexIndices(vertexIndices[0], vertexIndices[1], vertexIndices[2]);
    triangle.setTextureCoordinates(texCoordIndices[0], texCoordIndices[1], texCoordIndices[2]);
    triangle.setColor(new Vector(color));
    triangle.setNormal(new Vector(normal));
    return triangle;
  }

  public void setVertexIndices(int vertexIndex1, int vertexIndex2, int vertexIndex3) {
    this.vertexIndices[0] = vertexIndex1;
    this.vertexIndices[1] = vertexIndex2;
    this.vertexIndices[2] = vertexIndex3;
  }

  /**
   * i must be in 0, 1, 2
   */
  public int getTexCoordIndex(int i) {
    return texCoordIndices[i];
  }

  /**
   * Add an offset to all texture coordinates.
   */
  public void addTexCoordOffset(int offset) {
    for (int i = 0; i < 3; i++) {
      texCoordIndices[i] += offset;
    }
  }

  /**
   * Set the three texture coordinates at the three triangle corners.
   */
  public void setTextureCoordinates(int texCoordIndex1, int texCoordIndex2, int texCoordIndex3) {
    this.texCoordIndices[0] = texCoordIndex1;
    this.texCoordIndices[1] = texCoordIndex2;
    this.texCoordIndices[2] = texCoordIndex3;
  }

  public Vector getColor() {
    return color;
  }

  /**
   * Color must be a 4D vector in RGBA format.
   */
  public void setColor(Vector color) {
    if (color.getDimension() != 4) {
      Logger.getInstance().log("Color must be given in RGBA format!");
    }
    this.color = color;
  }

  public void setNormal(Vector normal) {
    this.normal.copy(normal);
  }

  public Vector getNormal() {
    return normal;
  }
}
