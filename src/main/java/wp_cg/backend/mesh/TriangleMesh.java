/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */
package wp_cg.backend.mesh;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import wp_cg.backend.math.AxisAlignedBoundingBox;
import wp_cg.backend.math.Vector;
import wp_cg.backend.misc.Logger;
import wp_cg.backend.rendering.Texture;
import wp_cg.backend.rendering.TextureManager;

;

/**
 * Implementation of a indexed vertex list triangle mesh.
 */
public class TriangleMesh {

    /**
     * Vertices.
     */
    private List<Vertex> vertices = new ArrayList<Vertex>();

    /**
     * Triangles.
     */
    private List<Triangle> triangles = new ArrayList<Triangle>();

    /**
     * Texture coordinates.
     */
    private List<Vector> textureCoordinates = new ArrayList<Vector>();

    /**
     * Texture object, leave null if no texture is used.
     */
    private String textureName = null;

    public TriangleMesh() {
    }

    public TriangleMesh(String textureName) {
        this.textureName = textureName;
    }

    /**
     * Copy constructor
     */
    public TriangleMesh(TriangleMesh mesh) {
        // Vertices
        for (int i = 0; i < mesh.getNumberOfVertices(); i++) {
            addVertex(new Vertex(mesh.getVertex(i)));
        }
        // Texture coordinates
        for (int i = 0; i < mesh.getNumberOfTextureCoordinates(); i++) {
            addTextureCoordinate(new Vector(mesh.getTextureCoordinate(i)));
        }
        // Triangles
        for (int i = 0; i < mesh.getNumberOfTriangles(); i++) {
            addTriangle(new Triangle(mesh.getTriangle(i)));
        }

    }

    public void clear() {
        vertices.clear();
        triangles.clear();
        textureCoordinates.clear();
    }

    public int addTriangle(int vertexIndex1, int vertexIndex2, int vertexIndex3) {
        triangles.add(new Triangle(vertexIndex1, vertexIndex2, vertexIndex3));
        return triangles.size() - 1;
    }

    public void addTriangle(Triangle t) {
        if (t instanceof Triangle) {
            triangles.add((Triangle) t);
        } else {
            throw new IllegalArgumentException("Can only add Triangle objects.");
        }
    }

    public int addVertex(Vector position) {
        vertices.add(new Vertex(position));
        return vertices.size() - 1;
    }

    public int addVertex(Vertex vertex) {
        vertices.add(vertex);
        return vertices.size() - 1;
    }

    public Vertex getVertex(int index) {
        return vertices.get(index);
    }

    public int getNumberOfTriangles() {
        return triangles.size();
    }

    public int getNumberOfVertices() {
        return vertices.size();
    }

    public Vertex getVertex(Triangle triangle, int index) {

        if (!(triangle instanceof Triangle)) {
            return null;
        }
        Triangle t = (Triangle) triangle;
        return vertices.get(t.getVertexIndex(index));
    }

    public Triangle getTriangle(int triangleIndex) {
        return triangles.get(triangleIndex);
    }

    public Vector getTextureCoordinate(int texCoordIndex) {
        return textureCoordinates.get(texCoordIndex);
    }

    public void computeTriangleNormals() {
        for (int triangleIndex = 0; triangleIndex < getNumberOfTriangles(); triangleIndex++) {
            Triangle t = triangles.get(triangleIndex);
            Vector a = vertices.get(t.getVertexIndex(0)).getPosition();
            Vector b = vertices.get(t.getVertexIndex(1)).getPosition();
            Vector c = vertices.get(t.getVertexIndex(2)).getPosition();
            Vector normal = b.subtract(a).cross(c.subtract(a));
            double norm = normal.getNorm();
            if (norm > 1e-5) {
                normal = normal.multiply(1.0 / norm);
            }
            t.setNormal(normal);
        }
    }

    public int addTextureCoordinate(Vector t) {
        textureCoordinates.add(t);
        return textureCoordinates.size() - 1;
    }

    public Texture getTexture() {
        return TextureManager.getInstance().getTexture(textureName);
    }

    public void addTriangle(int vertexIndex1, int vertexIndex2, int vertexIndex3, int texCoordIndex1, int texCoordIndex2,
                            int texCoordIndex3) {
        triangles
                .add(new Triangle(vertexIndex1, vertexIndex2, vertexIndex3, texCoordIndex1, texCoordIndex2, texCoordIndex3));
    }

    public int getNumberOfTextureCoordinates() {
        return textureCoordinates.size();
    }

    public boolean hasTexture() {
        return textureName != null;
    }

    /**
     * Compute the silhouette (list of edges) for a given position
     */
    public List<Edge> getSilhouette(Vector position) {
        List<Edge> silhouetteEdges = new ArrayList<Edge>();
        Map<Edge, Integer> edge2FacetMap = new HashMap<Edge, Integer>();
        for (int triangleIndex = 0; triangleIndex < triangles.size(); triangleIndex++) {
            Triangle t = triangles.get(triangleIndex);
            for (int i = 0; i < 3; i++) {
                int a = t.getVertexIndex(i);
                int b = t.getVertexIndex((i + 1) % 3);
                Edge edge = new Edge(a, b);
                if (edge2FacetMap.containsKey(edge)) {
                    // Opposite egde found
                    int oppositeTriangle = edge2FacetMap.get(edge);
                    if (isSilhouetteEdge(position, triangleIndex, oppositeTriangle, edge)) {
                        silhouetteEdges.add(edge);
                    }
                    // Debugging: if edge map is empty in the end, then the surface is
                    // closed.
                    edge2FacetMap.remove(edge);
                } else {
                    // Opposite edge not yet found
                    edge2FacetMap.put(edge, triangleIndex);
                }
            }
        }
        // System.out
        // .println("Created " + silhouetteEdges.size() + " silhouette edges.");
        return silhouetteEdges;
    }

    /**
     * Returns true if the edge between the two given triangles is a silhouette
     * edge for the given position.
     */
    private boolean isSilhouetteEdge(Vector position, int triangle1Index, int triangle2Index, Edge edge) {
        Triangle t1 = triangles.get(triangle1Index);
        Triangle t2 = triangles.get(triangle2Index);
        Vertex v1 = vertices.get(t1.getVertexIndex(0));
        Vertex v2 = vertices.get(t2.getVertexIndex(0));
        double d1 = t1.getNormal().multiply(position) - t1.getNormal().multiply(v1.getPosition());
        double d2 = t2.getNormal().multiply(position) - t2.getNormal().multiply(v2.getPosition());
        if (d1 < 0) {
            edge.Flip();
        }
        return d1 * d2 < 0;
    }

    public void setColor(Vector color) {
        if (color.getDimension() != 4) {
            Logger.getInstance().log("Color must be in RGBA format.");
            return;
        }
        for (Triangle triangle : triangles) {
            triangle.setColor(color);
        }
        for (Vertex vertex : vertices) {
            vertex.setColor(color);
        }
    }

    public void setTransparency(double alpha) {
        for (Triangle triangle : triangles) {
            triangle.getColor().set(3, alpha);
        }
        for (Vertex vertex : vertices) {
            vertex.getColor().set(3, alpha);
        }
    }

    public void clearTriangles() {
        triangles.clear();
    }

    public AxisAlignedBoundingBox getBoundingBox() {
        AxisAlignedBoundingBox bb = new AxisAlignedBoundingBox();
        for (Vertex v : vertices) {
            bb.add(v.getPosition());
        }
        return bb;
    }

    public void setTextureName(String textureFilename) {
        this.textureName = textureFilename;
    }
}
