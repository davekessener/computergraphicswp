/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.mesh;

import java.util.Arrays;
import java.util.List;

import wp_cg.backend.math.Vector;
import wp_cg.backend.misc.Logger;

/**
 * Collection of tools to work on triangle meshes.
 *
 * @author Philipp Jenke
 */
public class TriangleMeshTools {

    /**
     * Recale mesh to max dimension 1 and center at origin.
     */
    public static void fitToUnitBox(List<TriangleMesh> meshes) {
        Vector ll = new Vector(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        Vector ur = new Vector(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);

        for (TriangleMesh mesh : meshes) {
            for (int i = 0; i < mesh.getNumberOfVertices(); i++) {
                Vector pos = mesh.getVertex(i).getPosition();
                for (int j = 0; j < 3; j++) {
                    if (pos.get(j) < ll.get(j)) {
                        ll.set(j, pos.get(j));
                    }
                    if (pos.get(j) > ur.get(j)) {
                        ur.set(j, pos.get(j));
                    }
                }
            }
        }

        Vector center = ll.add(ur).multiply(0.5);
        Vector diag = ur.subtract(ll);
        double scale = Math.max(diag.x(), Math.max(diag.y(), diag.z()));

        for (TriangleMesh mesh : meshes) {
            for (int i = 0; i < mesh.getNumberOfVertices(); i++) {
                Vertex vertex = mesh.getVertex(i);
                vertex.getPosition().copy((vertex.getPosition().subtract(center)).multiply(1.0 / scale));
            }
            mesh.computeTriangleNormals();
        }
        Logger.getInstance().log("Successfully fit mesh into unit box.");
    }

    /**
     * Recale mesh to max dimension 1 and center at origin.
     */
    public static void fitToUnitBox(TriangleMesh mesh) {
        Vector ll = new Vector(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        Vector ur = new Vector(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
        for (int i = 0; i < mesh.getNumberOfVertices(); i++) {
            Vector pos = mesh.getVertex(i).getPosition();
            for (int j = 0; j < 3; j++) {
                if (pos.get(j) < ll.get(j)) {
                    ll.set(j, pos.get(j));
                }
                if (pos.get(j) > ur.get(j)) {
                    ur.set(j, pos.get(j));
                }
            }
        }

        Vector center = ll.add(ur).multiply(0.5);
        Vector diag = ur.subtract(ll);
        double scale = Math.max(diag.x(), Math.max(diag.y(), diag.z()));

        for (int i = 0; i < mesh.getNumberOfVertices(); i++) {
            Vertex vertex = mesh.getVertex(i);
            vertex.getPosition().copy((vertex.getPosition().subtract(center)).multiply(1.0 / scale));
        }
        mesh.computeTriangleNormals();
        Logger.getInstance().log("Successfully fit mesh into unit box.");
    }

    /**
     * Create a unified mesh from all meshes in the list. Not tested for meshes using textures.
     */
    public static TriangleMesh unite(List<TriangleMesh> meshes) {

        if (meshes.size() == 0) {
            return null;
        }

        TriangleMesh mesh = meshes.get(0);

        int vertexOffset = mesh.getNumberOfVertices();
        int texCoordOffset = mesh.getNumberOfTextureCoordinates();
        for (int meshIndex = 1; meshIndex < meshes.size(); meshIndex++) {
            TriangleMesh m = meshes.get(meshIndex);
            // Vertices
            for (int i = 0; i < m.getNumberOfVertices(); i++) {
                mesh.addVertex(new Vertex(m.getVertex(i)));
            }
            for (int i = 0; i < m.getNumberOfTextureCoordinates(); i++) {
                mesh.addTextureCoordinate(new Vector(m.getTextureCoordinate(i)));
            }
            for (int i = 0; i < m.getNumberOfTriangles(); i++) {
                Triangle t = m.getTriangle(i).clone();
                t.addVertexIndexOffset(vertexOffset);
                t.addTexCoordOffset(texCoordOffset);
                mesh.addTriangle(t);
            }

            vertexOffset += m.getNumberOfVertices();
            texCoordOffset += m.getNumberOfTextureCoordinates();
        }

        return mesh;
    }

    /**
     * The minimum y-value is 0.
     */
    public static void placeOnXZPlane(TriangleMesh mesh) {
        placeOnXZPlane(Arrays.asList(mesh));
    }

    /**
     * The minimum y-value is 0.
     */
    public static void placeOnXZPlane(List<TriangleMesh> meshes) {
        double yMin = Double.POSITIVE_INFINITY;

        for (TriangleMesh mesh : meshes) {
            for (int i = 0; i < mesh.getNumberOfVertices(); i++) {
                Vector pos = mesh.getVertex(i).getPosition();
                if (pos.y() < yMin) {
                    yMin = pos.y();
                }
            }
        }

        for (TriangleMesh mesh : meshes) {
            for (int i = 0; i < mesh.getNumberOfVertices(); i++) {
                Vertex vertex = mesh.getVertex(i);
                vertex.getPosition().set(1, vertex.getPosition().y() - yMin);
            }
            mesh.computeTriangleNormals();
        }
        Logger.getInstance().log("Successfully placed object on x-z-plane.");
    }

    /**
     * Scale a mesh by a constant factor.
     */
    public static TriangleMesh scale(TriangleMesh baseMesh, double scale) {
        TriangleMesh mesh = new TriangleMesh(baseMesh);
        for (int i = 0; i < mesh.getNumberOfVertices(); i++) {
            mesh.getVertex(i).getPosition().copy(mesh.getVertex(i).getPosition().multiply(scale));
        }
        mesh.computeTriangleNormals();
        return mesh;
    }
}
