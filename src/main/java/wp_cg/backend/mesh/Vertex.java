/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.mesh;

import wp_cg.backend.math.Vector;

/**
 * Represents a vertex in 3-space with position and normal.
 *
 * @author Philipp Jenke
 */
public class Vertex {

    /**
     * Vertex position in 3-space.
     */
    protected Vector position = new Vector(0, 0, 0);

    /**
     * Vertex normal in 3-space.
     */
    protected Vector normal = new Vector(0, 1, 0);

    /**
     * Color in RBGA format.
     */
    protected Vector color = new Vector(0.5, 0.5, 0.5, 1);

    public Vertex(Vector position) {
        this(position, new Vector(0, 1, 0));
    }

    public Vertex(Vector position, Vector normal) {
        this.position.copy(position);
        this.normal.copy(normal);
    }

    public Vertex(Vertex vertex) {
        this.position = new Vector(vertex.position);
        this.normal = new Vector(vertex.normal);
        this.color = new Vector(vertex.color);
    }

    public Vector getPosition() {
        return position;
    }

    public Vector getNormal() {
        return normal;
    }

    public void setNormal(Vector normal) {
        this.normal = normal;
    }

    public Vector getColor() {
        return color;
    }

    public void setColor(Vector color) {
        this.color = color;
    }
}
