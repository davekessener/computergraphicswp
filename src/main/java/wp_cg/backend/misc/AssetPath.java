/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.misc;

import java.io.*;

/**
 * Computes the relative path to an asset - uses a platform specific implementation.
 *
 * @author Philipp Jenke
 */
public class AssetPath {

    private static AssetPath instance;

    private static String[] assetPaths = {"assets/", "../android/assets/", "assets/meshes/", "assets/textures/"};


    private AssetPath() {
    }

    public static AssetPath getInstance() {
        if (instance == null) {
            instance = new AssetPath();
        }
        return instance;
    }

    public String readTextFileToString(String relativeFilename) {

        String path = getPathToAsset(relativeFilename);
        if (path == null) {
            throw new IllegalArgumentException("Failed to read file " + relativeFilename);
        }
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(path));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            br.close();
            return sb.toString();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
        throw new IllegalArgumentException("Failed to read file " + relativeFilename);
    }

    public InputStream readTextFileToStream(String relativeFilename) {
        String path = getPathToAsset(relativeFilename);
        if (path == null) {
            throw new IllegalArgumentException("Failed to read file " + relativeFilename);
        }
        File initialFile = new File(path);
        try {
            InputStream stream = new FileInputStream(initialFile);
            return stream;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Failed to open file " + relativeFilename);
        }
    }

    public String getPathToAsset(String filename) {
        for (String assetPath : assetPaths) {
            String path = assetPath + filename;
            File file = new File(path);
            if (file.exists()) {
                return path;
            }
        }
        return null;
    }
}