/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.misc;

/**
 * Contains the constants which are backend within the framework.
 */

public interface Constants {
    String LOGTAG = "WPCG";

    public static String TEXURE_DIR = "drawable";
}
