/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.misc;

/**
 * Implementations of this interface provide platform-specific logging functionality.
 */
public interface PlatformLogger {
    /**
     * Log a message
     */
    public void log(String message);
}
