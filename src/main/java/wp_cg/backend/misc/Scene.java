/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.misc;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import wp_cg.backend.jogl.OpenGL;
import wp_cg.backend.math.Matrix;
import wp_cg.backend.rendering.*;
import wp_cg.backend.scenegraph.Camera;
import wp_cg.backend.scenegraph.InnerNode;
import wp_cg.backend.scenegraph.RootNode;

/**
 * Central frame for all applications - derive from this class.
 *
 * @author Philipp Jenke
 */
public abstract class Scene {
  /**
   * Timer object to create a game loop.
   */
  private Timer timer = new Timer();

  /**
   * Root node of the scene graph
   */
  private RootNode root;

  /**
   * Time.
   */
  private int timerCounter = 0;

  /**
   * Indicates a timer tick.
   */
  private boolean timerUpdate = false;

  /**
   * Set this flag if the projection matrix needs to be updated.
   */
  boolean needsUpdateProjectionMatrix = true;

  /**
   * Mapping between resource id and texture id;
   */
  Map<Integer, Integer> textureIds = new HashMap<Integer, Integer>();

  /**
   * Set this flag if the view matrix needs to be updated.
   */
  boolean needsUpdateViewMatrix = true;

  /**
   * Camera interactionController.
   */
  private InteractionController interactionController = new ObserverInteractionController();

  public Scene() {
    this(100);
  }

  /**
   * Constructor
   */
  public Scene(int timerTimeout, String vertexShaderFilename,
      String fragmentShaderFilename) {
    Shader shader = new Shader(vertexShaderFilename, fragmentShaderFilename);
    root = new RootNode(shader);
    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        timerUpdate = true;
        timerCounter++;
      }
    }, timerTimeout, timerTimeout);
  }

  /**
   * Constructor
   */
  public Scene(int timerTimeout) {
    this(timerTimeout, "shader/vertex_shader.glsl",
        "shader/fragment_shader.glsl");
  }

  // +++ THESE METHODS MUST BE IMPLEMENTED IN ALL CLASSE DERIVING FROM SCENE +++

  /**
   * Override this method with all the scene onSetup, e.g. setting up the scene
   * graph
   */
  public abstract void onSetup(InnerNode rootNode);

  /**
   * This method is called at each timer tick.
   */
  public abstract void onTimerTick(int counter);

  /**
   * This method is called once for each scene redraw of OpenGL ES.
   */
  public abstract void onSceneRedraw();

  /**
   * Key pressed event
   */
  public void onKeyPressed(int keyCode) {
    switch (keyCode) {
    case 70: // 'f'
      interactionController.fitToBoundingBox(getRoot().getBoundingBox());
    }
  }

  /**
   * Key pressed event
   */
  public void onKeyReleased(int keyCode) {
  }

  // +++ END: THESE METHODS MUST BE IMPLEMENTED IN ALL CLASSE DERIVING FROM
  // SCENE +++

  /**
   * This method is called once when the OpenGL context is created.
   */
  public void init() {
    Logger.getInstance().log(OpenGL.instance().getVersionString());

    // Stencil test
    OpenGL.instance().enableStencilTest(false);

    // Culling
    OpenGL.instance().setupCulling();

    // Depth Test
    OpenGL.instance().enableDepthTest(true);
    OpenGL.instance().enableCulling(true);
    OpenGL.instance().enableBlending(true);
    OpenGL.instance().enableStencilTest(false);
    needsUpdateProjectionMatrix = true;
    root.getShader().compileAndLink();
    root.getShader().use();
    ShaderAttributes.getInstance().getAttributes(root.getShader().getProgram());
  }

  /**
   * Scene needs to be redrawn.
   */
  public void redraw() {
    if (timerUpdate && root.isAnimated()) {
      root.timerTick(timerCounter);
      onTimerTick(timerCounter);
      timerUpdate = false;
    }

    // Inform inherited scene.
    onSceneRedraw();

    drawRegular();
  }

  /**
   * Render scene regularly
   */
  private void drawRegular() {
    root.getShader().use();
    ShaderAttributes.getInstance()
        .setCameraEyeParameter(Camera.getInstance().getEye());
    ShaderAttributes.getInstance()
        .setLightPositionParameter(root.getLightPosition());

    getRoot().traverse(Matrix.createIdentityMatrix4());
  }

  /**
   * Return the root node of the scene graph.
   */
  public RootNode getRoot() {
    return root;
  }

  public void resize(int width, int height) {
  }

  /**
   * A touch event occurred. Returns true, if the touch was handled.
   */
  public boolean onTouchDown(float x, float y) {
    return false;
  }
}
