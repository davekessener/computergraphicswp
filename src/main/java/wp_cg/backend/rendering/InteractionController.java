/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.rendering;

import wp_cg.backend.math.AxisAlignedBoundingBox;
import wp_cg.backend.math.Vector;
import wp_cg.backend.scenegraph.Camera;

/**
 * The interaction controller handles touch input on a GLView.
 */
public abstract class InteractionController {

    /**
     * Fitting factor for automatic camera adjustment.
     */
    double FIT_FACTOR = 1.5;

    /**
     * Called once at the very beginning.
     */
    public abstract void init();

    /**
     * A touch event was detected which moved across the screen.
     */
    public abstract void touchMoved(int dx, int dy);

    /**
     * Stop view controller
     */
    abstract void stop();

    /**
     * Zoom in and out (based on sign)
     */
    public abstract void zoom(int delta);

    /**
     * Set the camera to observe the complete scene.
     */
    public void fitToBoundingBox(AxisAlignedBoundingBox bbox) {
        if (bbox == null) {
            return;
        }
        Vector ref = bbox.getCenter();
        final double diameter = bbox.getDiameter();
        Vector eyeRef = Camera.getInstance().getRef()
                .subtract(Camera.getInstance().getEye()).getNormalized();
        eyeRef = eyeRef.multiply(diameter * FIT_FACTOR);
        Vector eye =
                Camera.getInstance().getRef().subtract(eyeRef);
        Camera.getInstance().setup(eye, ref, Camera.getInstance().getUp());
    }
}
