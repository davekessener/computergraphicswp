/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.rendering;

/**
 * Default implementation of an InteractionController which does nothing.
 */
public class NoInteractionController extends InteractionController {
    @Override
    public void init() {
        // Do nothing.
    }

    @Override
    public void touchMoved(int dx, int dy) {
        // Do nothing
    }

    @Override
    void stop() {

    }

    @Override
    public void zoom(int delta) {

    }
}
