/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.rendering;

import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.scenegraph.Camera;

/**
 * Control the camera using touch gestures
 */
public class ObserverInteractionController extends InteractionController {
    @Override
    public void init() {
        Camera.getInstance().setup(new Vector(-2, 0, 0), new Vector(0, 0, 0), new Vector(0, 1, 0));
        Camera.getInstance().setViewMatrixFromEyeRefUp();
    }

    @Override
    public void touchMoved(int dx, int dy) {
        float alpha = -dx / 200.0f;
        float beta = -dy / 200.0f;
        Camera cam = Camera.getInstance();
        Vector eye = cam.getEye();
        Vector ref = cam.getRef();
        Vector up = cam.getUp();
        Vector dir = eye.subtract(ref);
        // Rotate around up-vector
        eye = Matrix.createRotationMatrix3(up, alpha).multiply(dir).add(ref);
        // Rotate around side-vector
        dir = eye.subtract(ref);
        Vector side = dir.cross(up);
        side.normalize();
        eye = Matrix.createRotationMatrix3(side, -beta).multiply(dir).add(ref);
        // Fix up-vector
        dir = ref.subtract(eye);
        side = dir.cross(up);
        side.normalize();
        up = side.cross(dir);
        up.normalize();
        cam.setup(eye, ref, up);
        cam.setViewMatrixFromEyeRefUp();
    }

    @Override
    public void stop() {

    }

    @Override
    public void zoom(int delta) {
        double factor = 1.0 - delta / 100.0;
        Camera.getInstance().zoom(factor);
    }
}
