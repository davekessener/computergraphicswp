/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.rendering;

import wp_cg.backend.math.Vector;

/**
 * Helping data structure to represent a render vertex in a @VertexBufferObject.
 * 
 * @author Philipp Jenke
 */
public class RenderVertex {

  public RenderVertex(Vector position, Vector normal, Vector color) {
    this(position, normal, color, new Vector(0,0));
  }
  
  public RenderVertex(Vector position, Vector normal, Vector color, Vector texCoords) {
    this.position = position;
    this.normal = normal;
    this.color = color;
    this.texCoords = texCoords;
  }

  /**
   * 3D position.
   */
  public Vector position;

  /**
   * 3D normal.
   */
  public Vector normal;

  /**
   * 4D color.
   */
  public Vector color;
  
  /**
   * 2D Texture coordinate
   */
  public Vector texCoords;

}
