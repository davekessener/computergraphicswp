/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.rendering;

import wp_cg.backend.jogl.OpenGL;
import wp_cg.backend.misc.AssetPath;
import wp_cg.backend.misc.Logger;

/**
 * Representation of GLSL shader.
 *
 * @author Philipp Jenke
 */
public class Shader {

    public static final String LOGTAG = "WP Computer Graphics AR";
    
    public static enum ShaderType {
        VERTEX, FRAGMENT
    }

    ;

    public static enum ShaderMode {
        PHONG, TEXTURE, NO_LIGHTING, AMBIENT_ONLY, TEXTURE_NO_LIGHTING
    }

    /**
     * Flag for the state of the shaders
     */
    private boolean compiled = false;

    /**
     * ID of the shader program.
     */
    private int shaderProgram = -1;

    /**
     * Filename of the vertex shader source
     */
    private String vertexShaderFilename = "";

    /**
     * Filename of the pixel shader source
     */
    private String fragmentShaderFilename = "";

    public Shader(String vertexShaderFilename, String fragmentShaderFilename) {
        this.vertexShaderFilename = vertexShaderFilename;
        this.fragmentShaderFilename = fragmentShaderFilename;
    }

    /**
     * Compile and link the shaders.
     */
    public void compileAndLink() {
        compiled = true;

        // Compile
        int v = compileShader(ShaderType.VERTEX,
                vertexShaderFilename);
        int f = compileShader(ShaderType.FRAGMENT,
                fragmentShaderFilename);
        if (v < 0 || f < 0) {
            Logger.getInstance().log("Shader not created.");
            return;
        }

        // Link
        shaderProgram = linkProgram(v, f);
        if (shaderProgram < 0) {
            Logger.getInstance().log("Shader not created.");
            return;
        }

        Logger.getInstance().log("Successfully created shader from vertex shader filename "
                + vertexShaderFilename + " and fragment shader fileame "
                + fragmentShaderFilename);
    }

    /**
     * Activate the shader
     */
    public void use() {
        if (!isCompiled()) {
            compileAndLink();
        }
        OpenGL.instance().glUseProgram(shaderProgram);
    }

    /**
     * Getter.
     */
    public boolean isCompiled() {
        return compiled;
    }

    /**
     * Read a shader code from a source file to a String.
     */
    private static String readShaderSource(String shaderFilename) {


        return AssetPath.getInstance().readTextFileToString(shaderFilename);


    /*
    String absoluteShaderFilename = shaderFilename;
    String shaderSource = "";
    if (absoluteShaderFilename == null) {
      System.out.println("Shader source " + shaderFilename
          + " not found - cannot read shader.");
      return shaderSource;
    }

    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(absoluteShaderFilename));
      StringBuilder sb = new StringBuilder();
      String line = br.readLine();
      while (line != null) {
        sb.append(line);
        sb.append("\n");
        line = br.readLine();
      }
      br.close();
      shaderSource = sb.toString();
    } catch (FileNotFoundException e) {
      System.out
          .println("Failed to read shader source " + absoluteShaderFilename);
      e.printStackTrace();
    } catch (IOException e) {
      System.out
          .println("Failed to read shader source " + absoluteShaderFilename);
      e.printStackTrace();
    }
    return shaderSource;
    */
    }



    /**
     * Link the vertex and fragment shaders.
     */
    private int linkProgram(int vertexShaderId, int fragmentShaderId) {
        int shaderProgram = OpenGL.instance().glCreateProgram();
        OpenGL.instance().glAttachShader(shaderProgram, vertexShaderId);
        OpenGL.instance().glAttachShader(shaderProgram, fragmentShaderId);
        OpenGL.instance().glLinkProgram(shaderProgram);
        OpenGL.instance().glValidateProgram(shaderProgram);
        return shaderProgram;
    }

    /**
     * Compile the specified shader from the filename and return the OpenGL id.
     */
    private int compileShader(ShaderType shaderType, String shaderFilename) {
        String vsrc = Shader.readShaderSource(shaderFilename);
        int id = compileShaderFromSource(shaderType, vsrc);
        if (id < 0) {
            Logger.getInstance().log("Compile error in shader file " + shaderFilename + " of type " + shaderType);
        }
        return id;
    }

    /**
     * Compile the specified shader from the filename and return the OpenGL id.
     */
    private int compileShaderFromSource(ShaderType shaderType,
                                        String shaderSource) {
        int id = OpenGL.instance().glCreateShader(shaderType);
        OpenGL.instance().glShaderSource(id, shaderSource);
        OpenGL.instance().glCompileShader(id);
        if (OpenGL.instance().checkCompileError(id)) {
            Logger.getInstance().log("Shader " + shaderType + " compile error!\n");
            return -1;
        }
        return id;
    }

    public int getProgram() {
        return shaderProgram;
    }
}
