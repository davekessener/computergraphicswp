/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.rendering;

import wp_cg.backend.jogl.OpenGL;
import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.rendering.Shader.ShaderMode;

/**
 * Singleton class to provide access to the shader program locations.
 *
 * @author Philipp Jenke
 */
public class ShaderAttributes {

    /**
     * Uniform and attribute parameter location in the shader program.
     */
    private static int locationShaderMode = -1;
    private static int locationCameraPosition = -1;
    private static int locationLightPosition = -1;
    private static int locationModelMatrix = -1;
    private static int locationViewMatrix = -1;
    private static int locationProjectionMatrix = -1;
    private static int locationVertex = -1;
    private static int locationColor = -1;
    private static int locationNormal = -1;
    private static int locationTexCoords = -1;

    /**
     * Singleton instance
     */
    private static ShaderAttributes instance = null;

    private ShaderAttributes() {
    }

    /**
     * Getter for the singleton instance.
     */
    public static ShaderAttributes getInstance() {
        if (instance == null) {
            instance = new ShaderAttributes();
        }
        return instance;
    }

    /**
     * Read the current attribute locations from the shader, must be called after
     * shader link!
     */
    public void getAttributes(int shaderProgram) {
        assert (shaderProgram >= 0);
        locationVertex = OpenGL.instance().glGetAttribLocation(shaderProgram, "inVertex");
        locationNormal = OpenGL.instance().glGetAttribLocation(shaderProgram, "inNormal");
        locationColor = OpenGL.instance().glGetAttribLocation(shaderProgram, "inColor");
        locationTexCoords = OpenGL.instance().glGetAttribLocation(shaderProgram, "inTexCoords");
        locationCameraPosition = OpenGL.instance().glGetUniformLocation(shaderProgram, "camera_position");
        locationShaderMode = OpenGL.instance().glGetUniformLocation(shaderProgram, "shaderMode");
        locationLightPosition = OpenGL.instance().glGetUniformLocation(shaderProgram, "lightPosition");
        locationModelMatrix = OpenGL.instance().glGetUniformLocation(shaderProgram, "modelMatrix");
        locationViewMatrix = OpenGL.instance().glGetUniformLocation(shaderProgram, "viewMatrix");
        locationProjectionMatrix = OpenGL.instance().glGetUniformLocation(shaderProgram, "projectionMatrix");
    }

    public int getVertexLocation() {
        return locationVertex;
    }

    public int getNormalLocation() {
        return locationNormal;
    }

    public int getColorLocation() {
        return locationColor;
    }

    public int getTexCoordsLocation() {
        return locationTexCoords;
    }

    public void setCameraEyeParameter(Vector eye) {
        if (locationCameraPosition >= 0) {
            OpenGL.instance().glUniform3f(locationCameraPosition, (float) eye.x(), (float) eye.y(), (float) eye.z());
        }
    }

    public void setLightPositionParameter(Vector lightPosition) {
        if (locationLightPosition >= 0) {
            OpenGL.instance().glUniform3f(locationLightPosition, (float) lightPosition.x(), (float) lightPosition.y(),
                    (float) lightPosition.z());
        }
    }

    public void setModelMatrixParameter(Matrix modelMatrix) {
        if (locationModelMatrix >= 0) {
            OpenGL.instance().glUniformMatrix4fv(locationModelMatrix, 1, false, modelMatrix.floatData(), 0);
            // System.out.println("model matrix " + modelMatrix);
        }
    }

    public void setViewMatrixParameter(Matrix viewMatrix) {
        if (locationViewMatrix >= 0) {
            OpenGL.instance().glUniformMatrix4fv(locationViewMatrix, 1, false, viewMatrix.floatData(), 0);
            // System.out.println("view matrix " + viewMatrix);
        }
    }

    public void setProjectionMatrixParameter(Matrix projectionMatrix) {
        if (locationProjectionMatrix >= 0) {
            OpenGL.instance().glUniformMatrix4fv(locationProjectionMatrix, 1, false, projectionMatrix.floatData(), 0);
            // System.out.println("projection matrix " + projectionMatrix);
        }
    }

    public void setShaderModeParameter(ShaderMode mode) {
        if (locationShaderMode < 0) {
            return;
        }
        int value = 0;
        switch (mode) {
            case PHONG:
                value = 0;
                break;
            case TEXTURE:
                value = 1;
                break;
            case NO_LIGHTING:
                value = 2;
                break;
            case AMBIENT_ONLY:
                value = 3;
                break;
            case TEXTURE_NO_LIGHTING:
                value = 4;
                break;
        }
        OpenGL.instance().glUniform1i(locationShaderMode, value);
    }
}