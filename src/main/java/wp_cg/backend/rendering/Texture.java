/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.rendering;

import wp_cg.backend.jogl.OpenGL;

/**
 * Represents an OpenGL ES texture.
 */
public class Texture {


    private int textureId = -1;

    public Texture(int textureId) {
        this.textureId = textureId;
    }

    /**
     * Bind the texture as current texture.
     */
    public void bind() {
        if (textureId < 0) {
            throw new IllegalArgumentException("Texture not available.");
        }
        OpenGL.instance().glBindTexture(textureId);
    }

    public void delete() {
        if (textureId > 0) {
            OpenGL.instance().glDeleteTextures(textureId);
        }
    }
}
