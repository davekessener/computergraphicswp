/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.rendering;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.util.texture.TextureIO;
import wp_cg.backend.jogl.OpenGL;
import wp_cg.backend.misc.AssetPath;
import wp_cg.backend.misc.Logger;

/**
 * This managers keeps track of all registered textures which can be used in a scene.
 *
 * @author Philipp Jenke
 */
public class TextureManager {

    private Map<String, Texture> textures = new HashMap<String, Texture>();

    private static TextureManager instance = null;

    private TextureManager() {
    }

    public static TextureManager getInstance() {
        if (instance == null) {
            instance = new TextureManager();
        }
        return instance;
    }

    /**
     * Return the texure for the texture name.
     */
    public Texture getTexture(String textureName) {
        if (!textures.containsKey(textureName)) {
            Texture texture = new Texture(load(textureName));
            textures.put(textureName, texture);
        }
        return textures.get(textureName);
    }

    public void deleteAllTextures() {
        for (Texture texture : textures.values()) {
            if (texture != null) {
                texture.delete();
            }
        }
        textures.clear();
        Logger.getInstance().log("Deleted all textures.");
    }



    /**
     * Load texture image from file and create GL texture object.
     */
    public int load(String filename) {
        com.jogamp.opengl.util.texture.Texture texture = null;
        
        Logger.getInstance().log("Loading texture '" + filename + "'");
        
        try {
            texture = TextureIO.newTexture(new File(AssetPath.getInstance().getPathToAsset(filename)), true);
        } catch (GLException | IOException e) {
            System.out.println("Failed to load texture from image.");
            return -1;
        }
        if (texture == null) {
            System.out.println("Failed to load texture from image.");
            return -1;
        }

        GL2 gl = (GL2) OpenGL.instance().getOpenGLObject();

        int textureId = texture.getTextureObject(gl);

        gl.glEnable(GL2.GL_TEXTURE_2D);
        gl.glBindTexture(GL2.GL_TEXTURE_2D, textureId);
        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_REPEAT);
        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_REPEAT);
        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_NEAREST);
        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_NEAREST);
        Logger.getInstance().log("Texture " + filename + " loaded.");
        return textureId;
    }
}
