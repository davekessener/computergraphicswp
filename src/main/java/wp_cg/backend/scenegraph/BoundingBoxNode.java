/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.scenegraph;

import java.util.ArrayList;
import java.util.List;

import wp_cg.backend.math.AxisAlignedBoundingBox;
import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.jogl.OpenGL;
import wp_cg.backend.rendering.RenderVertex;
import wp_cg.backend.rendering.Shader;
import wp_cg.backend.rendering.ShaderAttributes;
import wp_cg.backend.rendering.VertexBufferObject;

/**
 * Represents a bounding box in the scene graph
 *
 * @author Philipp Jenke
 */
public class BoundingBoxNode extends LeafNode {

  /**
   * Cube side length
   */
  private AxisAlignedBoundingBox bbox;

  private Vector color = new Vector(0.25, 0.75, 0.25, 1);

  /**
   * VBO.
   */
  private VertexBufferObject vbo = new VertexBufferObject();

  /**
   * Constructor.
   */
  public BoundingBoxNode(AxisAlignedBoundingBox bbox) {
    this.bbox = bbox;
    createVbo();
  }

  private void createVbo() {
    List<RenderVertex> renderVertices = new ArrayList<RenderVertex>();

    Vector ll = bbox.getLL();
    Vector ur = bbox.getUR();

    Vector p0 = new Vector(ll.x(), ll.y(), ll.z());
    Vector p1 = new Vector(ll.x(), ur.y(), ll.z());
    Vector p2 = new Vector(ur.x(), ur.y(), ll.z());
    Vector p3 = new Vector(ur.x(), ll.y(), ll.z());
    Vector p4 = new Vector(ll.x(), ll.y(), ur.z());
    Vector p5 = new Vector(ll.x(), ur.y(), ur.z());
    Vector p6 = new Vector(ur.x(), ur.y(), ur.z());
    Vector p7 = new Vector(ur.x(), ll.y(), ur.z());
    Vector n = new Vector(0, 1, 0);

    renderVertices.add(new RenderVertex(p0, n, color));
    renderVertices.add(new RenderVertex(p1, n, color));
    renderVertices.add(new RenderVertex(p1, n, color));
    renderVertices.add(new RenderVertex(p2, n, color));
    renderVertices.add(new RenderVertex(p2, n, color));
    renderVertices.add(new RenderVertex(p3, n, color));
    renderVertices.add(new RenderVertex(p3, n, color));
    renderVertices.add(new RenderVertex(p0, n, color));
    renderVertices.add(new RenderVertex(p4, n, color));
    renderVertices.add(new RenderVertex(p5, n, color));
    renderVertices.add(new RenderVertex(p5, n, color));
    renderVertices.add(new RenderVertex(p6, n, color));
    renderVertices.add(new RenderVertex(p6, n, color));
    renderVertices.add(new RenderVertex(p7, n, color));
    renderVertices.add(new RenderVertex(p7, n, color));
    renderVertices.add(new RenderVertex(p4, n, color));
    renderVertices.add(new RenderVertex(p0, n, color));
    renderVertices.add(new RenderVertex(p4, n, color));
    renderVertices.add(new RenderVertex(p1, n, color));
    renderVertices.add(new RenderVertex(p5, n, color));
    renderVertices.add(new RenderVertex(p2, n, color));
    renderVertices.add(new RenderVertex(p6, n, color));
    renderVertices.add(new RenderVertex(p3, n, color));
    renderVertices.add(new RenderVertex(p7, n, color));

    vbo.setup(renderVertices, OpenGL.Primitive.LINES);
  }

  @Override
  public void drawGL(Matrix modelMatrix) {
    ShaderAttributes.getInstance()
        .setShaderModeParameter(Shader.ShaderMode.NO_LIGHTING);
    vbo.draw();
  }

  public void setBoundingBox(AxisAlignedBoundingBox bbox) {
    this.bbox = bbox;
    vbo.invalidate();
    createVbo();
  }

  public void setColor(Vector color) {
    this.color.copy(color);
    vbo.invalidate();
  }

  @Override
  public AxisAlignedBoundingBox getBoundingBox() {
    return bbox;
  }
}
