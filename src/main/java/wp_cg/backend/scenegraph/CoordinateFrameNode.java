/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.scenegraph;

import java.util.ArrayList;
import java.util.List;

import wp_cg.backend.math.AxisAlignedBoundingBox;
import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.jogl.OpenGL;
import wp_cg.backend.rendering.RenderVertex;
import wp_cg.backend.rendering.Shader;
import wp_cg.backend.rendering.ShaderAttributes;
import wp_cg.backend.rendering.VertexBufferObject;

/**
 * Representation of a coordinate frame with lines for the three axis in RBG.
 */
public class CoordinateFrameNode extends LeafNode {
  private VertexBufferObject vbo = new VertexBufferObject();

  public CoordinateFrameNode() {
    this(1);
  }

  public CoordinateFrameNode(float scale) {
    List<RenderVertex> renderVertices = new ArrayList<RenderVertex>();
    renderVertices.add(new RenderVertex(new Vector(0, 0, 0),
        new Vector(0, 1, 0), new Vector(1, 0, 0, 1)));
    renderVertices.add(new RenderVertex(new Vector(scale, 0, 0),
        new Vector(0, 1, 0), new Vector(1, 0, 0, 1)));
    renderVertices.add(new RenderVertex(new Vector(0, 0, 0),
        new Vector(0, 1, 0), new Vector(0, 1, 0, 1)));
    renderVertices.add(new RenderVertex(new Vector(0, scale, 0),
        new Vector(0, 1, 0), new Vector(0, 1, 0, 1)));
    renderVertices.add(new RenderVertex(new Vector(0, 0, 0),
        new Vector(0, 1, 0), new Vector(0, 0, 1, 1)));
    renderVertices.add(new RenderVertex(new Vector(0, 0, scale),
        new Vector(0, 1, 0), new Vector(0, 0, 1, 1)));
    vbo.setup(renderVertices, OpenGL.Primitive.LINES);
  }

  @Override
  public void drawGL(Matrix modelMatrix) {
    ShaderAttributes.getInstance()
        .setShaderModeParameter(Shader.ShaderMode.NO_LIGHTING);
    OpenGL.instance().glLineWidth(6);
    vbo.draw();
  }

  @Override
  public void timerTick(int counter) {
  }

  @Override
  public AxisAlignedBoundingBox getBoundingBox() {
    return new AxisAlignedBoundingBox(new Vector(0, 0, 0), new Vector(1, 1, 1));
  }
}
