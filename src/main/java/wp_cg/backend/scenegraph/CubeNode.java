/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.scenegraph;

import java.util.ArrayList;
import java.util.List;

import wp_cg.backend.math.AxisAlignedBoundingBox;
import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.jogl.OpenGL;
import wp_cg.backend.rendering.RenderVertex;
import wp_cg.backend.rendering.Shader;
import wp_cg.backend.rendering.ShaderAttributes;
import wp_cg.backend.rendering.VertexBufferObject;

/**
 * Representation of a cuboid with different dimensions in x-, y- and
 * z-direction.
 *
 * @author Philipp Jenke
 */
public class CubeNode extends LeafNode {

  /**
   * Cube side length
   */
  private double sideLength;
  private final Vector mColor;

  /**
   * VBO.
   */
  private VertexBufferObject vbo = new VertexBufferObject();

  /**
   * Constructor.
   */
  public CubeNode(double sideLength) {
    this.sideLength = sideLength;
    mColor = new Vector(0.25, 0.75, 0.25, 1);
    createVbo();
  }
  
  public CubeNode(double l, Vector c)
  {
	  if(c.getDimension() != 4)
		  throw new IllegalArgumentException("Color is not in RGBA format: " + c);
	  
	  sideLength = l;
	  mColor = c;
	  
	  createVbo();
  }

  private void createVbo() {
    List<RenderVertex> renderVertices = new ArrayList<RenderVertex>();

    Vector p0 = new Vector(-sideLength / 2.0, -sideLength / 2.0,
        -sideLength / 2.0);
    Vector p1 = new Vector(sideLength / 2.0, -sideLength / 2.0,
        -sideLength / 2.0);
    Vector p2 = new Vector(sideLength / 2.0, sideLength / 2.0,
        -sideLength / 2.0);
    Vector p3 = new Vector(-sideLength / 2.0, sideLength / 2.0,
        -sideLength / 2.0);
    Vector p4 = new Vector(-sideLength / 2.0, -sideLength / 2.0,
        sideLength / 2.0);
    Vector p5 = new Vector(sideLength / 2.0, -sideLength / 2.0,
        sideLength / 2.0);
    Vector p6 = new Vector(sideLength / 2.0, sideLength / 2.0,
        sideLength / 2.0);
    Vector p7 = new Vector(-sideLength / 2.0, sideLength / 2.0,
        sideLength / 2.0);
    Vector n0 = new Vector(0, 0, -1);
    Vector n1 = new Vector(1, 0, 0);
    Vector n2 = new Vector(0, 0, 1);
    Vector n3 = new Vector(-1, 0, 0);
    Vector n4 = new Vector(0, 1, 0);
    Vector n5 = new Vector(0, -1, 0);

    AddSideVertices(renderVertices, p0, p1, p2, p3, n0, mColor);
    AddSideVertices(renderVertices, p1, p5, p6, p2, n1, mColor);
    AddSideVertices(renderVertices, p4, p7, p6, p5, n2, mColor);
    AddSideVertices(renderVertices, p0, p3, p7, p4, n3, mColor);
    AddSideVertices(renderVertices, p2, p6, p7, p3, n4, mColor);
    AddSideVertices(renderVertices, p5, p1, p0, p4, n5, mColor);

    vbo.setup(renderVertices, OpenGL.Primitive.TRIANGLES);
  }

  /**
   * Add 4 vertices to the array
   */
  private void AddSideVertices(List<RenderVertex> renderVertices, Vector p0,
      Vector p1, Vector p2, Vector p3, Vector normal, Vector color) {
    renderVertices.add(new RenderVertex(p3, normal, color));
    renderVertices.add(new RenderVertex(p2, normal, color));
    renderVertices.add(new RenderVertex(p0, normal, color));
    renderVertices.add(new RenderVertex(p2, normal, color));
    renderVertices.add(new RenderVertex(p1, normal, color));
    renderVertices.add(new RenderVertex(p0, normal, color));
  }

  @Override
  public void drawGL(Matrix modelMatrix) {
    ShaderAttributes.getInstance()
        .setShaderModeParameter(Shader.ShaderMode.PHONG);
    vbo.draw();
  }

  @Override
  public AxisAlignedBoundingBox getBoundingBox() {
    return new AxisAlignedBoundingBox(
        new Vector(-sideLength, -sideLength, -sideLength),
        new Vector(sideLength, sideLength, sideLength));
  }
}
