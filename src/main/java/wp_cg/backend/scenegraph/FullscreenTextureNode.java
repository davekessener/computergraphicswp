/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.scenegraph;

import wp_cg.backend.math.AxisAlignedBoundingBox;
import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.mesh.Triangle;
import wp_cg.backend.mesh.TriangleMesh;
import wp_cg.backend.rendering.ShaderAttributes;

/**
 * Renders a texture as a fullscreen image.
 */
public class FullscreenTextureNode extends LeafNode {

  /**
   * The texture is mapped to a simple 2-triangle-mesh, which is represented in
   * this node.
   */
  private TriangleMeshNode meshNode;

  public FullscreenTextureNode(String filename) {
    double z = 0.99;
    TriangleMesh fullscreenBackgroundMesh = new TriangleMesh();
    fullscreenBackgroundMesh.addVertex(new Vector(-1, -1, z));
    fullscreenBackgroundMesh.addVertex(new Vector(1, -1, z));
    fullscreenBackgroundMesh.addVertex(new Vector(1, 1, z));
    fullscreenBackgroundMesh.addVertex(new Vector(-1, 1, z));
    fullscreenBackgroundMesh.addTextureCoordinate(new Vector(0, 0));
    fullscreenBackgroundMesh.addTextureCoordinate(new Vector(1, 0));
    fullscreenBackgroundMesh.addTextureCoordinate(new Vector(1, 1));
    fullscreenBackgroundMesh.addTextureCoordinate(new Vector(0, 1));
    fullscreenBackgroundMesh.addTriangle(new Triangle(0, 1, 2, 0, 1, 2));
    fullscreenBackgroundMesh.addTriangle(new Triangle(0, 2, 3, 0, 2, 3));
    fullscreenBackgroundMesh.computeTriangleNormals();
    fullscreenBackgroundMesh.setTextureName(filename);
    meshNode = new TriangleMeshNode(fullscreenBackgroundMesh);
    meshNode.setTextureLighting(TriangleMeshNode.TextureLighting.NO_LIGHTING);
  }

  @Override
  public void traverse(Matrix modelMatrix) {
    if (!isActive()) {
      return;
    }
    Matrix identity = Matrix.createIdentityMatrix4();
    // Model matrix
    ShaderAttributes.getInstance().setModelMatrixParameter(identity);
    // Projection matrix
    ShaderAttributes.getInstance().setProjectionMatrixParameter(identity);
    // View matrix
    ShaderAttributes.getInstance().setViewMatrixParameter(identity);
    drawGL(identity);
  }

  @Override
  public void drawGL(Matrix modelMatrix) {
    meshNode.drawGL(null);
  }

  @Override
  public AxisAlignedBoundingBox getBoundingBox() {
    return null;
  }
}
