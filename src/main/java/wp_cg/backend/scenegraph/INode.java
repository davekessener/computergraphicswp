/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.scenegraph;

import wp_cg.backend.math.AxisAlignedBoundingBox;
import wp_cg.backend.math.Matrix;

/**
 * Parent class for all scene graph nodes.
 *
 * @author Philipp Jenke
 */
public abstract class INode {

  /**
   * Parent node, null if root
   */
  private INode parentNode = null;

  /**
   * This flag indicates of the node is shown/traversed
   */
  private boolean active = true;

  /**
   * This method is called to draw the node using OpenGL commands. Override in
   * implementing nodes. Do not forget to call the same method for the children.
   */
  public abstract void traverse(Matrix modelMatrix);

  /**
   * Timer tick event.
   */
  public abstract void timerTick(int counter);

  /**
   * Every node must know its root node
   */
  public RootNode getRootNode() {
    return (parentNode == null) ? null : parentNode.getRootNode();
  }

  /**
   * Every node must know its root node
   */
  public void setParentNode(INode parentNode) {
    this.parentNode = parentNode;
  }

  /**
   * Return parent node, null if root.
   */
  protected INode getParentNode() {
    return parentNode;
  }

  /**
   * Return the combined transformation starting at the root node.
   * Transformation from the node coordinate system into the global coordinate
   * system.
   */
  public abstract Matrix getCombinedTransformation();

  /**
   * Return the transformation of the individual node (not combined with the
   * parent transformations). Transformation from the node coordinate system
   * into the global coordinate system.
   */
  public abstract Matrix getTransformation();

  /**
   * Set to true for inactive nodes.
   */
  public void setActive(boolean active) {
    this.active = active;
  }

  /**
   * Returns true for inactive nodes
   */
  public boolean isActive() {
    return active;
  }

  /**
   * Return the bounding
   *
   * @return
   */
  public abstract AxisAlignedBoundingBox getBoundingBox();
}
