/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.scenegraph;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import wp_cg.backend.math.AxisAlignedBoundingBox;
import wp_cg.backend.math.Matrix;

/**
 * Inner node in the scene graph which might have child nodes.
 */
public class InnerNode extends INode {

  /**
   * List of child nodes.
   */
  private List<INode> children = new ArrayList<INode>();

  @Override
  public synchronized void traverse(Matrix modelMatrix) {
    if (!isActive()) {
      return;
    }

    for (int i = 0; i < children.size(); i++) {
      children.get(i).traverse(modelMatrix);
    }
  }

  @Override
  public void timerTick(int counter) {
    for (INode child : children) {
      child.timerTick(counter);
    }
  }

  /**
   * Add new child node.
   **/
  public synchronized void addChild(INode child) {
    child.setParentNode(this);
    children.add(child);
  }

  /**
   * Remove child from children list.
   */
  public synchronized boolean removeChild(INode node) {
    return children.remove(node);
  }

  @Override
  public Matrix getCombinedTransformation() {
    return (getParentNode() == null) ? null
        : getParentNode().getCombinedTransformation();
  }

  @Override
  public Matrix getTransformation() {
    throw new IllegalArgumentException(
        "Needs to be implemented in deriving classes.");
  }

  /**
   * Return the bounding
   *
   * @return
   */
  public AxisAlignedBoundingBox getBoundingBox() {
    AxisAlignedBoundingBox bbox = null;
    for (INode child : children) {
      if (bbox == null) {
        bbox = new AxisAlignedBoundingBox(child.getBoundingBox());
      } else {
        bbox.add(child.getBoundingBox());
      }
    }
    return bbox;
  }

  public int getNumberOfChildren() {
    return children.size();
  }

  public INode getChild(int index) {
    return children.get(index);
  }

  public void clearChildren() {
    children.clear();
  }
  
  public Stream<INode> children() { return children.stream(); }
}
