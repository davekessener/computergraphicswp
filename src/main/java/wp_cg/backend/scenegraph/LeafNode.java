/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.scenegraph;

import wp_cg.backend.math.Matrix;
import wp_cg.backend.rendering.ShaderAttributes;

/**
 * A leaf node allows to draw OpenGl content.
 */
public abstract class LeafNode extends INode {

  public static final String LOGTAG = "WP Computer Graphics AR";

  @Override
  public void traverse(Matrix modelMatrix) {

    if (!isActive()) {
      return;
    }

    // Model matrix
    Matrix openGLModelMatrix = modelMatrix.getTransposed();
    ShaderAttributes.getInstance().setModelMatrixParameter(openGLModelMatrix);
    // Log.i(LOGTAG, "modelview (cg):\n " + modelMatrix.toString());

    // Projection matrix
    ShaderAttributes.getInstance().setProjectionMatrixParameter(
        Camera.getInstance().getProjectionMatrix());
    // Log.i(LOGTAG, "projection (cg):\n " +
    // Camera.getInstance().getProjectionMatrix().toString());

    // View matrix
    Matrix viewMatrix = Camera.getInstance().getViewMatrix();
    ShaderAttributes.getInstance().setViewMatrixParameter(viewMatrix);

    drawGL(openGLModelMatrix);
  }

  @Override
  public void timerTick(int counter) {
  }

  /**
   * Draw GL content.
   */
  public abstract void drawGL(Matrix modelMatrix);

  @Override
  public Matrix getCombinedTransformation() {
    return (getParentNode() == null) ? null
        : getParentNode().getCombinedTransformation();
  }

  @Override
  public Matrix getTransformation() {
    return Matrix.createIdentityMatrix4();
  }
}
