/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.scenegraph;

import java.util.HashMap;
import java.util.Map;

import wp_cg.backend.math.AxisAlignedBoundingBox;
import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.mesh.Triangle;
import wp_cg.backend.mesh.TriangleMesh;
import wp_cg.backend.jogl.OpenGL;
import wp_cg.backend.rendering.ShaderAttributes;

/**
 * Render a String to the screen
 */
public class RenderTextNode extends LeafNode {

  /**
   * The texture is mapped to a simple 2-triangle-mesh, which is represented in
   * this node.
   */
  private TriangleMeshNode meshNode;

  /**
   * Filename of the texture containing the font characters.
   */
  private static final String FONT_TEXTURE_FILENAME = "font_texture.png";

  /**
   * Texture coordinate size of a character in the texture
   */
  private static final double CHAR_SIZE_IN_TEXTURE_COORDINATES = 1.0 / 8.0;

  /**
   * Depth value of the sprite, must be in [-1; 1]
   */
  private final double DEPTH;

  /*
   * Height of the texture in [0;2]. Attention: absolute value depends on the
   * aspect ratio.
   */
  private final double textHeight;

  /*
   * Width of the texture in [0;2]. Attention: absolute value depends on the
   * aspect ratio.
   */
  private final double textWidth;

  /**
   * Mesh with the squares for the characters.
   */
  private TriangleMesh textMesh = new TriangleMesh();

  /**
   * Position of the text mesh on screen. Must be in [-1;1]^2
   */
  private final double x, y;

  /**
   * Texture coordinates for each character (which is in the texture).
   */
  private Map<Character, Vector> charTextureCoordinates = new HashMap<Character, Vector>();

  public RenderTextNode(double x, double y, double textHeight, String text,
      double depth) {
    this.DEPTH = depth;
    this.textHeight = textHeight;
    this.textWidth = textHeight / Camera.getInstance().getAspectRatio();
    this.x = x;
    this.y = y;
    setupMap();

    createTextMeshNode(text);
    meshNode = new TriangleMeshNode(textMesh);
    meshNode.setTextureLighting(TriangleMeshNode.TextureLighting.NO_LIGHTING);
  }

  public RenderTextNode(double x, double y, double textHeight, String text) {
    this(x, y, textHeight, text, 0.98);
  }

  /**
   * Create the mesh for the text.
   */
  private void createTextMeshNode(String text) {
    textMesh.clear();
    for (int i = 0; i < text.length(); i++) {
      generateMeshForChar(textMesh, text.charAt(i), x + i * textWidth * 0.9, y);
    }
    textMesh.computeTriangleNormals();
    textMesh.setTextureName(FONT_TEXTURE_FILENAME);
  }

  /**
   * Create the mesh part for a single character.
   */
  private void generateMeshForChar(TriangleMesh mesh, char c, double x,
      double y) {
    Vector charTexCoords = charTextureCoordinates.get(c);
    if (charTexCoords == null) {
      throw new IllegalArgumentException(
          "trying to render unsupported character.");
    }
    int v0 = mesh.addVertex(new Vector(x, y, DEPTH));
    int v1 = mesh.addVertex(new Vector(x + textWidth, y, DEPTH));
    int v2 = mesh.addVertex(new Vector(x + textWidth, y + textHeight, DEPTH));
    int v3 = mesh.addVertex(new Vector(x, y + textHeight, DEPTH));
    int t0 = mesh.addTextureCoordinate(
        OpenGL.instance().fixTexCoord(charTexCoords));
    int t1 = mesh.addTextureCoordinate(OpenGL.instance().fixTexCoord(
        charTexCoords.add(new Vector(CHAR_SIZE_IN_TEXTURE_COORDINATES, 0))));
    int t2 = mesh.addTextureCoordinate(OpenGL.instance().fixTexCoord(
        charTexCoords.add(new Vector(CHAR_SIZE_IN_TEXTURE_COORDINATES,
            CHAR_SIZE_IN_TEXTURE_COORDINATES))));
    int t3 = mesh.addTextureCoordinate(OpenGL.instance().fixTexCoord(
        charTexCoords.add(new Vector(0, CHAR_SIZE_IN_TEXTURE_COORDINATES))));
    mesh.addTriangle(new Triangle(v0, v1, v2, t0, t1, t2));
    mesh.addTriangle(new Triangle(v0, v2, v3, t0, t2, t3));
  }

  /**
   * Setup the map with the character texture coordinates (depends on the font
   * texture!)
   */
  private void setupMap() {
    charTextureCoordinates.put('1',
        new Vector(0, 7 * CHAR_SIZE_IN_TEXTURE_COORDINATES));
    charTextureCoordinates.put('2', new Vector(CHAR_SIZE_IN_TEXTURE_COORDINATES,
        7 * CHAR_SIZE_IN_TEXTURE_COORDINATES));
    charTextureCoordinates.put('3',
        new Vector(2 * CHAR_SIZE_IN_TEXTURE_COORDINATES,
            7 * CHAR_SIZE_IN_TEXTURE_COORDINATES));
    charTextureCoordinates.put('4',
        new Vector(3 * CHAR_SIZE_IN_TEXTURE_COORDINATES,
            7 * CHAR_SIZE_IN_TEXTURE_COORDINATES));
    charTextureCoordinates.put('5',
        new Vector(4 * CHAR_SIZE_IN_TEXTURE_COORDINATES,
            7 * CHAR_SIZE_IN_TEXTURE_COORDINATES));
    charTextureCoordinates.put('6',
        new Vector(5 * CHAR_SIZE_IN_TEXTURE_COORDINATES,
            7 * CHAR_SIZE_IN_TEXTURE_COORDINATES));
    charTextureCoordinates.put('7',
        new Vector(6 * CHAR_SIZE_IN_TEXTURE_COORDINATES,
            7 * CHAR_SIZE_IN_TEXTURE_COORDINATES));
    charTextureCoordinates.put('8',
        new Vector(7 * CHAR_SIZE_IN_TEXTURE_COORDINATES,
            7 * CHAR_SIZE_IN_TEXTURE_COORDINATES));
    charTextureCoordinates.put('9',
        new Vector(0, 6 * CHAR_SIZE_IN_TEXTURE_COORDINATES));
    charTextureCoordinates.put('0', new Vector(CHAR_SIZE_IN_TEXTURE_COORDINATES,
        6 * CHAR_SIZE_IN_TEXTURE_COORDINATES));
    charTextureCoordinates.put('x',
        new Vector(2 * CHAR_SIZE_IN_TEXTURE_COORDINATES,
            6 * CHAR_SIZE_IN_TEXTURE_COORDINATES));
  }

  @Override
  public void traverse(Matrix modelMatrix) {
    if (!isActive()) {
      return;
    }
    Matrix identity = Matrix.createIdentityMatrix4();
    // Model matrix
    Matrix openGLModelMatrix = modelMatrix.getTransposed();
    ShaderAttributes.getInstance().setModelMatrixParameter(openGLModelMatrix);
    // Projection matrix
    ShaderAttributes.getInstance().setProjectionMatrixParameter(identity);
    // View matrix
    ShaderAttributes.getInstance().setViewMatrixParameter(identity);
    drawGL(identity);
  }

  @Override
  public void drawGL(Matrix modelMatrix) {
    meshNode.drawGL(null);
  }

  @Override
  public AxisAlignedBoundingBox getBoundingBox() {
    return null;
  }

  /**
   * Update the text.
   */
  public void setText(String text) {
    createTextMeshNode(text);
    meshNode.updateVbo();
  }
}
