/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.scenegraph;

import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.rendering.Shader;

/**
 * The root node exists only once and is used as the scene graph root. It
 * contains scene information.
 *
 * @author Philipp Jenke
 */
public class RootNode extends InnerNode {
  /**
   * Currently used shader
   */
  private Shader shader;

  /**
   * This flags indicates that the scene should be animated
   */
  private boolean animated;

  /**
   * Position of the light source
   */
  private Vector lightPosition;

  /**
   * Background color
   */
  private Vector backGroundColor;

  public RootNode(Shader shader) {
    this.shader = shader;
    lightPosition = new Vector(1, 1, 0);
    backGroundColor = new Vector(0.95, 0.95, 0.95);
    animated = true;
  }

  @Override
  public void traverse(Matrix modelMatrix) {
    super.traverse(modelMatrix);
  }

  @Override
  public void timerTick(int counter) {
    super.timerTick(counter);
  }

  public RootNode getRootNode() {
    return this;
  }

  public Shader getShader() {
    return shader;
  }

  public boolean isAnimated() {
    return animated;
  }

  public void setAnimated(boolean animated) {
    this.animated = animated;
  }

  public Vector getLightPosition() {
    return lightPosition;
  }

  public Vector getBackgroundColor() {
    return backGroundColor;
  }

  public void setLightPosition(Vector lightPosition) {
    this.lightPosition = lightPosition;
  }

  public void setBackgroundColor(Vector backGroundColor) {
    this.backGroundColor = backGroundColor;
  }

  @Override
  public Matrix getCombinedTransformation() {
    return Matrix.createIdentityMatrix4();
  }
}
