/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.scenegraph;

import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;

/**
 * Scene graph node which scales all its child nodes.
 *
 * @author Philipp Jenke
 */
public class RotationNode extends InnerNode {

  /**
   * Scaling factors in x-, y- and z-direction.
   */
  private Matrix rotationMatrix;

  private Vector axis;
  private double angle;

  /**
   * Constructor.
   */
  public RotationNode(Vector axis, double angle) {
    this.axis = axis;
    this.angle = angle;
    update();
  }

  public void setAngle(double angle) {
    this.angle = angle;
    update();
  }

  public void setAxis(Vector axis) {
    this.axis = axis;
    update();
  }

  private void update() {
    rotationMatrix = Matrix.createRotationMatrix4(axis, angle);
  }

  public void traverse(Matrix modelMatrix) {
    super.traverse(modelMatrix.multiply(rotationMatrix));
  }

  public void timerTick(int counter) {
    super.timerTick(counter);
  }

  @Override
  public Matrix getCombinedTransformation() {
    return (getParentNode() == null) ? rotationMatrix
        : getParentNode().getCombinedTransformation().multiply(rotationMatrix);
  }

  @Override
  public Matrix getTransformation() {
    return rotationMatrix;
  }
}
