/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.scenegraph;

import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;

/**
 * Scene graph node which scales all its child nodes.
 *
 * @author Philipp Jenke
 */
public class ScaleNode extends InnerNode {

  /**
   * Scaling factors in x-, y- and z-direction.
   */
  private Matrix scaleMatrix;

  /**
   * Constructor.
   */
  public ScaleNode(Vector scale) {
    scaleMatrix = Matrix.createScaleMatrix4(scale);
  }

  /**
   * Constructor.
   */
  public ScaleNode(double scale) {
    scaleMatrix = Matrix.createScaleMatrix4(new Vector(scale, scale, scale));
  }

  public void traverse(Matrix modelMatrix) {
    super.traverse(modelMatrix.multiply(scaleMatrix));
  }

  public void timerTick(int counter) {
    super.timerTick(counter);
  }

  @Override
  public Matrix getCombinedTransformation() {
    return (getParentNode() == null) ? scaleMatrix
        : getParentNode().getCombinedTransformation().multiply(scaleMatrix);
  }

  public void setScale(Vector scale) {
    scaleMatrix = Matrix.createScaleMatrix4(scale);
  }

  @Override
  public Matrix getTransformation() {
    return scaleMatrix;
  }
}
