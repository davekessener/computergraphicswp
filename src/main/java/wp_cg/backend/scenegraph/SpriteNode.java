/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.scenegraph;

import wp_cg.backend.math.AxisAlignedBoundingBox;
import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.mesh.Triangle;
import wp_cg.backend.mesh.TriangleMesh;
import wp_cg.backend.jogl.OpenGL;
import wp_cg.backend.rendering.ShaderAttributes;

/**
 * Render a 2D sprite on screen
 */
public class SpriteNode extends LeafNode {

  /**
   * The texture is mapped to a simple 2-triangle-mesh, which is represented in
   * this node.
   */
  private TriangleMeshNode meshNode;

  private double DEPTH = 0.08;

  /**
   * @param x                   in [-1,1]
   * @param y                   in [-1,1]
   * @param width               in [-1,1]
   * @param height              in [-1,1]
   * @param spriteImageFilename 2D sprite image filename
   */
  public SpriteNode(double x, double y, double width, double height,
      String spriteImageFilename) {
    this(x, y, width, height, spriteImageFilename, 0.98);
  }

  /**
   * @param x                   in [-1,1]
   * @param y                   in [-1,1]
   * @param width               in [-1,1]
   * @param height              in [-1,1]
   * @param spriteImageFilename 2D sprite image filename
   */
  public SpriteNode(double x, double y, double width, double height,
      String spriteImageFilename, double depth) {
    this.DEPTH = depth;

    TriangleMesh fullscreenBackgroundMesh = new TriangleMesh();
    fullscreenBackgroundMesh.addVertex(new Vector(x, y, DEPTH));
    fullscreenBackgroundMesh.addVertex(new Vector(x + width, y, DEPTH));
    fullscreenBackgroundMesh
        .addVertex(new Vector(x + width, y + height, DEPTH));
    fullscreenBackgroundMesh.addVertex(new Vector(x, y + height, DEPTH));
    fullscreenBackgroundMesh.addTextureCoordinate(
        OpenGL.instance().fixTexCoord(new Vector(0, 0)));
    fullscreenBackgroundMesh.addTextureCoordinate(
        OpenGL.instance().fixTexCoord(new Vector(1, 0)));
    fullscreenBackgroundMesh.addTextureCoordinate(
        OpenGL.instance().fixTexCoord(new Vector(1, 1)));
    fullscreenBackgroundMesh.addTextureCoordinate(
        OpenGL.instance().fixTexCoord(new Vector(0, 1)));
    fullscreenBackgroundMesh.addTriangle(new Triangle(0, 1, 2, 0, 1, 2));
    fullscreenBackgroundMesh.addTriangle(new Triangle(0, 2, 3, 0, 2, 3));
    fullscreenBackgroundMesh.computeTriangleNormals();
    fullscreenBackgroundMesh.setTextureName(spriteImageFilename);
    meshNode = new TriangleMeshNode(fullscreenBackgroundMesh);
    meshNode.setTextureLighting(TriangleMeshNode.TextureLighting.NO_LIGHTING);
  }

  @Override
  public void traverse(Matrix modelMatrix) {
    if (!isActive()) {
      return;
    }
    Matrix identity = Matrix.createIdentityMatrix4();
    // Model matrix
    Matrix openGLModelMatrix = modelMatrix.getTransposed();
    ShaderAttributes.getInstance().setModelMatrixParameter(openGLModelMatrix);
    // Projection matrix
    ShaderAttributes.getInstance().setProjectionMatrixParameter(identity);
    // View matrix
    ShaderAttributes.getInstance().setViewMatrixParameter(identity);
    drawGL(identity);
  }

  @Override
  public void drawGL(Matrix modelMatrix) {
    meshNode.drawGL(null);
  }

  @Override
  public AxisAlignedBoundingBox getBoundingBox() {
    return null;
  }
}
