/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.scenegraph;

import wp_cg.backend.math.Matrix;
import wp_cg.backend.misc.Logger;

/**
 * This node contains a transformation (4x4 matrix) in the scene graph.
 */
public class TransformationNode extends InnerNode {
  /**
   * Transformation matrix (model matrix), 4x4
   */
  private Matrix transformation;

  public TransformationNode(Matrix transformation) {
    this.transformation = transformation;
  }

  public TransformationNode() {
    this.transformation = Matrix.createIdentityMatrix4();
  }

  public void setTransformation(Matrix t) {
    if (t.getNumberOfRows() == 4 && t.getNumberOfColumns() == 4) {
      this.transformation.copy(t);
    } else {
      Logger.getInstance().log("Invalid dimension.");
      throw new IllegalArgumentException();
    }
  }

  public void traverse(Matrix modelMatrix) {
    super.traverse(modelMatrix.multiply(transformation));
  }

  public void timerTick(int counter) {
    super.timerTick(counter);
  }

  @Override
  public Matrix getCombinedTransformation() {
    return (getParentNode() == null) ? transformation
        : getParentNode().getCombinedTransformation().multiply(transformation);
  }

  /**
   * Return the transformation matrix (non-recursively)
   */
  public Matrix getTransformation() {
    return transformation;
  }
}
