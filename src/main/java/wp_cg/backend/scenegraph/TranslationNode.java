/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.scenegraph;

import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;

/**
 * Translate all child nodes.
 *
 * @author Philipp Jenke
 */
public class TranslationNode extends InnerNode {

  /**
   * Translation matrix (model matrix)
   */
  private Matrix translation;

  public TranslationNode(Vector translation) {
    this.translation = Matrix.createTranslationMatrix4(translation);
  }

  public void setTranslation(Vector t) {
    this.translation = Matrix.createTranslationMatrix4(t);
  }

  public void traverse(Matrix modelMatrix) {
    super.traverse(modelMatrix.multiply(translation));
  }

  public void timerTick(int counter) {
    super.timerTick(counter);
  }

  @Override
  public Matrix getCombinedTransformation() {
    return (getParentNode() == null) ? translation
        : getParentNode().getCombinedTransformation().multiply(translation);
  }

  @Override
  public Matrix getTransformation() {
    return translation;
  }

}
