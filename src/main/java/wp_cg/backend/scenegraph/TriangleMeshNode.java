/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.backend.scenegraph;

import java.util.ArrayList;
import java.util.List;

import wp_cg.backend.jogl.OpenGL;
import wp_cg.backend.math.AxisAlignedBoundingBox;
import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.mesh.Triangle;
import wp_cg.backend.mesh.TriangleMesh;
import wp_cg.backend.mesh.Vertex;
import wp_cg.backend.rendering.*;

/**
 * Scene graph node to render ITriangleMesh meshes.
 */
public class TriangleMeshNode extends LeafNode {

  /**
   * Enum for the render mode: describes which normals are used: triangle
   * normals (flat shading) or vertex normal (phong shading).
   */
  public static enum RenderNormals {
    PER_TRIANGLE_NORMAL, PER_VERTEX_NORMAL
  }

  /**
   * This state can be used to disable lighting for textures.
   */
  public enum TextureLighting {
    LIGHTING, NO_LIGHTING;
  }

  /**
   * Contained triangle mesh to be rendered.
   */
  private TriangleMesh mesh;

  /**
   * Debugging: Show normals.
   */
  private boolean showNormals = false;

  /**
   * Select which normals should be used for rendering: triangle vs. vertex
   * normals.
   */
  private RenderNormals renderNormals = RenderNormals.PER_TRIANGLE_NORMAL;

  /**
   * VBOs: triangles
   */
  private VertexBufferObject vbo = new VertexBufferObject();

  /**
   * VBOs: normals
   */
  private VertexBufferObject vboNormals = new VertexBufferObject();

  /**
   * Disable/enable Texture lighting
   */
  private TextureLighting textureLighting = TextureLighting.LIGHTING;

  public TriangleMeshNode(TriangleMesh mesh) {
    setup(mesh);
  }

  public void setup(TriangleMesh mesh) {
    this.mesh = mesh;
    vbo.setup(createRenderVertices(), OpenGL.Primitive.TRIANGLES);
    vboNormals.setup(createRenderVerticesNormals(),
        OpenGL.Primitive.LINES);
  }

  /**
   * Set the transparency of the mesh.
   */
  public void setTransparency(double transparency) {
    mesh.setTransparency(transparency);
    updateVbo();
  }

  /**
   * Create vbo data for mesh rendering
   */
  private List<RenderVertex> createRenderVertices() {
    List<RenderVertex> renderVertices = new ArrayList<RenderVertex>();
    for (int i = 0; i < mesh.getNumberOfTriangles(); i++) {
      Triangle t = mesh.getTriangle(i);
      for (int j = 0; j < 3; j++) {
        Vertex vertex = mesh.getVertex(t, j);
        Vector normal = (renderNormals == RenderNormals.PER_VERTEX_NORMAL)
            ? vertex.getNormal()
            : t.getNormal();
        RenderVertex renderVertex = null;
        if (t.getTexCoordIndex(j) >= 0) {
          renderVertex = new RenderVertex(vertex.getPosition(), normal,
              t.getColor(), mesh.getTextureCoordinate(t.getTexCoordIndex(j)));
        } else {
          renderVertex = new RenderVertex(vertex.getPosition(), normal,
              t.getColor(), new Vector(0, 0));
        }
        renderVertices.add(renderVertex);
      }
    }
    return renderVertices;
  }

  /**
   * Create vbo data for normal rendering.
   */
  private List<RenderVertex> createRenderVerticesNormals() {
    List<RenderVertex> renderVertices = new ArrayList<RenderVertex>();
    double normalScale = 0.03;
    Vector color = new Vector(0.5, 0.5, 0.5, 1);
    for (int i = 0; i < mesh.getNumberOfTriangles(); i++) {
      Triangle t = mesh.getTriangle(i);
      Vector p = mesh.getVertex(t, 0).getPosition()
          .add(mesh.getVertex(t, 1).getPosition())
          .add(mesh.getVertex(t, 2).getPosition()).multiply(1.0 / 3.0);
      renderVertices.add(new RenderVertex(p, t.getNormal(), color));
      renderVertices.add(new RenderVertex(
          p.add(t.getNormal().multiply(normalScale)), t.getNormal(), color));
    }
    return renderVertices;
  }

  public void drawGL(Matrix modelMatrix) {
    // Use texture if texture object != null

    if (mesh.hasTexture()) {
      Texture texture = mesh.getTexture();
      if (texture != null) {
        texture.bind();
        if (textureLighting == TextureLighting.LIGHTING) {
          ShaderAttributes.getInstance()
              .setShaderModeParameter(Shader.ShaderMode.TEXTURE);
        } else {
          ShaderAttributes.getInstance()
              .setShaderModeParameter(Shader.ShaderMode.TEXTURE_NO_LIGHTING);
        }

      } else {
        ShaderAttributes.getInstance()
            .setShaderModeParameter(Shader.ShaderMode.PHONG);
      }
    } else {
      ShaderAttributes.getInstance()
          .setShaderModeParameter(Shader.ShaderMode.PHONG);
    }

    drawRegular();
  }

  /**
   * Draw mesh regularly.
   */
  public void drawRegular() {
    vbo.draw();
    if (showNormals) {
      vboNormals.draw();
    }
  }

  public void setShowNormals(boolean showNormals) {
    this.showNormals = showNormals;
  }

  public void setRenderNormals(RenderNormals renderNormals) {
    this.renderNormals = renderNormals;
    vbo.setup(createRenderVertices(), OpenGL.Primitive.TRIANGLES);
  }

  public void updateVbo() {
    vbo.setup(createRenderVertices(), OpenGL.Primitive.TRIANGLES);
    vboNormals.setup(createRenderVerticesNormals(),
        OpenGL.Primitive.LINES);
    vbo.invalidate();
  }

  public AxisAlignedBoundingBox getBoundingBox() {
    return mesh.getBoundingBox();
  }

  public void setTextureLighting(TextureLighting textureLighting) {
    this.textureLighting = textureLighting;
  }
}
