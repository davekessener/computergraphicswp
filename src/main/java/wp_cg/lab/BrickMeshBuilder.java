package wp_cg.lab;

import java.util.Map;

import wp_cg.backend.math.Vector;
import wp_cg.backend.scenegraph.INode;
import wp_cg.backend.scenegraph.TriangleMeshNode;
import wp_cg.lab.math.IVec3;
import wp_cg.lab.math.ProtoTriangle;
import wp_cg.lab.util.ImmutableMapBuilder;
import wp_cg.platformer.Brick;
import wp_cg.platformer.Brick.Type;

public class BrickMeshBuilder
{
	private final double mLength;
	private final MeshBuilder mMesh;
	
	public BrickMeshBuilder(String t, double l)
	{
		mLength = l;
		mMesh = new MeshBuilder();
		
		mMesh.setTexture(t);
	}
	
	public void add(Brick.Side s, IVec3 p, Brick.Type t)
	{
		int[] face_indices = FACE_INDICES.get(s);
		int[] tex_offsets = TYPE_OFFSETS.get(t);
		Vector[] vertices = new Vector[CORNERS_PER_FACE];
		Vector[] tex = faceTextureCoords(TEX_INDICES.get(s), tex_offsets[0], tex_offsets[1]);
		
		for(int i = 0 ; i < vertices.length ; ++i)
		{
			vertices[i] = p.toVector().add(CORNERS[face_indices[i]]).multiply(mLength);
		}
		
		mMesh.add(new ProtoTriangle(vertices[0], vertices[1], vertices[2], tex[0], tex[1], tex[2]));
		mMesh.add(new ProtoTriangle(vertices[2], vertices[3], vertices[0], tex[2], tex[3], tex[0]));
	}
	
	public INode build()
	{
		return new TriangleMeshNode(mMesh.build());
	}
	
	private static final Map<Brick.Type, int[]> TYPE_OFFSETS = (new ImmutableMapBuilder<Brick.Type, int[]>())
		.put(Type.GROUND, new int[] { 0, 0 })
		.put(Type.SWITCH, new int[] { 0, 2 })
		.put(Type.BREAKING, new int[] { 3, 0 })
		.build();
	
	private static Vector[] faceTextureCoords(int[] c, int dx, int dy)
	{
		return new Vector[] {
			textureCoords(c[0] + dx + 0, c[1] + dy + 1),
			textureCoords(c[0] + dx + 1, c[1] + dy + 1),
			textureCoords(c[0] + dx + 1, c[1] + dy + 0),
			textureCoords(c[0] + dx + 0, c[1] + dy + 0),
		};
	}
	
	private static final int CORNERS_PER_FACE = 4;
	
	private static final Vector[] CORNERS = new Vector[] {
		new Vector(0, 0, 1),
		new Vector(1, 0, 1),
		new Vector(1, 1, 1),
		new Vector(0, 1, 1),
		new Vector(0, 0, 0),
		new Vector(1, 0, 0),
		new Vector(1, 1, 0),
		new Vector(0, 1, 0)
	};
	
	private static final Map<Brick.Side, int[]> FACE_INDICES = (new ImmutableMapBuilder<Brick.Side, int[]>())
		.put(Brick.Side.X_NEG, new int[] { 4, 0, 3, 7 })
		.put(Brick.Side.X_POS, new int[] { 1, 5, 6, 2 })
		.put(Brick.Side.Y_NEG, new int[] { 4, 5, 1, 0 })
		.put(Brick.Side.Y_POS, new int[] { 3, 2, 6, 7 })
		.put(Brick.Side.Z_NEG, new int[] { 5, 4, 7, 6 })
		.put(Brick.Side.Z_POS, new int[] { 0, 1, 2, 3 })
		.build();

	private static final Map<Brick.Side, int[]> TEX_INDICES = (new ImmutableMapBuilder<Brick.Side, int[]>())
		.put(Brick.Side.X_NEG, new int[] { 2, 0 })
		.put(Brick.Side.X_POS, new int[] { 0, 1 })
		.put(Brick.Side.Y_NEG, new int[] { 0, 0 })
		.put(Brick.Side.Y_POS, new int[] { 1, 0 })
		.put(Brick.Side.Z_NEG, new int[] { 1, 1 })
		.put(Brick.Side.Z_POS, new int[] { 2, 1 })
		.build();

	private static Vector textureCoords(int x, int y)
	{
		return new Vector(x / (double) TEX_W, 1.0 - y / (double) TEX_H);
	}
	
	private static final int TEX_W = 6, TEX_H = 6;
}
