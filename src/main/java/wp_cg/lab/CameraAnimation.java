package wp_cg.lab;

import wp_cg.backend.math.Vector;

public interface CameraAnimation
{
	public abstract boolean done();
	public abstract void step();

	public static final Vector UP = new Vector(0, 1, 0);
}
