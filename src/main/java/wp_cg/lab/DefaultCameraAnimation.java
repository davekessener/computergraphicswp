package wp_cg.lab;

import wp_cg.backend.math.Vector;
import wp_cg.backend.scenegraph.Camera;
import wp_cg.lab.util.Utils;
import wp_cg.platformer.DynamicGameState;

public class DefaultCameraAnimation implements CameraAnimation
{
	private final DynamicGameState mState;
	private final double mPerspective;
	
	public DefaultCameraAnimation(DynamicGameState s, double p)
	{
		mState = s;
		mPerspective = p;
	}

	@Override
	public boolean done()
	{
		return false;
	}

	@Override
	public void step()
	{
		Vector ref = new Vector(mState.getPosition());
		Vector perspective = ref.add(new Vector(0, mPerspective, 0));
		Vector eye = (new Vector(mState.getOrientation())).cross(UP).add(perspective);
		
		Camera.getInstance().setup(Utils.interpolate(ref, eye, SCALE), ref, UP);
	}
	
	private static final double SCALE = 0.75;
}
