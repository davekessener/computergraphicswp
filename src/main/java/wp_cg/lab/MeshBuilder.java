package wp_cg.lab;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.ToIntFunction;
import java.util.stream.Stream;

import wp_cg.backend.math.Vector;
import wp_cg.backend.mesh.TriangleMesh;
import wp_cg.lab.math.ProtoTriangle;
import wp_cg.lab.util.IntArrayCollector;
import wp_cg.lab.util.Property;
import wp_cg.lab.util.SimpleProperty;

public class MeshBuilder
{
	private final Property<String> mTexture;
	private final List<ProtoTriangle> mTriangles;
	
	public MeshBuilder( )
	{
		mTriangles = new ArrayList<>();
		mTexture = new SimpleProperty<>();
	}
	
	public MeshBuilder add(ProtoTriangle t)
	{
		mTriangles.add(t);
		
		return this;
	}
	
	public MeshBuilder setTexture(String tid)
	{
		if(tid == null)
			throw new NullPointerException();
		
		mTexture.set(tid);
		
		return this;
	}
	
	public TriangleMesh build()
	{
		TriangleMesh mesh = new TriangleMesh();
		List<Entry> vertices = new ArrayList<>();
		List<Entry> tex_coords = new ArrayList<>();
		
		BiFunction<ToIntFunction<Vector>, List<Entry>, Function<Vector, Entry>> creator = (f, c) -> {
			return v -> {
				Entry e = new Entry(v, f.applyAsInt(v));
				
				c.add(e);
				
				return e;
			};
		};
		
		if(mTexture.get() != null)
		{
			mesh.setTextureName("textures/" + mTexture.get() + ".png");
		}
		
		mTriangles.forEach(t -> {
			int[] corners = convertVertices(t.vertices, vertices, creator.apply(v -> mesh.addVertex(v), vertices));

			if(mTexture.get() != null && t.tex_coords != null)
			{
				int[] tex = convertVertices(t.tex_coords, tex_coords, creator.apply(v -> mesh.addTextureCoordinate(v), tex_coords));
				
				mesh.addTriangle(corners[0], corners[1], corners[2], tex[0], tex[1], tex[2]);
			}
			else
			{
				mesh.addTriangle(corners[0], corners[1], corners[2]);
			}
		});
		
		mesh.computeTriangleNormals();
		
		if(mTexture.get() == null)
		{
			mesh.setColor(COLOR_WHITE);
		}
		
		return mesh;
	}
	
	private static int[] convertVertices(Vector[] verts, List<Entry> buf, Function<Vector, Entry> f)
	{
		return IntArrayCollector.collect(
			VERTICES_PER_TRIANGLE,
			Stream.of(verts)
				.mapToInt(v -> buf.stream()
					.filter(e -> e.vector.equals(v))
					.findFirst()
					.orElseGet(() -> f.apply(v))
					.index));
	}
	
	private static class Entry
	{
		public final Vector vector;
		public final int index;
		
		public Entry(Vector v, int i)
		{
			vector = v;
			index = i;
		}
	}
	
	private static int VERTICES_PER_TRIANGLE = 3;
	private static final Vector COLOR_WHITE = new Vector(1, 1, 1, 1);
}
