package wp_cg.lab;

import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.scenegraph.Camera;
import wp_cg.lab.math.Line;

public class RotationCameraAnimation implements CameraAnimation
{
	private final double mRotation;
	private final int mSteps, mTime;
	private final Vector mEye;
	private final Line mTrack;
	private int mCounter, mIdx;
	
	public RotationCameraAnimation(double r, Line l, int s, int t)
	{
		Camera c = Camera.getInstance();
		
		mRotation = (r == 0 ? Math.PI : r);
		mSteps = s;
		mTime = t / s;
		mEye = new Vector(c.getEye());
		mTrack = l;
		
		mCounter = mIdx = 0;
	}
	
	@Override
	public boolean done()
	{
		return mIdx == mSteps;
	}

	@Override
	public void step()
	{
		if(++mCounter == mTime)
		{
			mCounter = 0;
			++mIdx;
		}
		
		Camera c = Camera.getInstance();
		double p = fade(mIdx / (double) mSteps);
		
		Matrix r = Matrix.createRotationMatrix3(c.getUp(), mRotation * p);

		Vector offset = mEye.subtract(mTrack.getBase());
		Vector ref = mTrack.apply(p);
		Vector eye = ref.add(r.multiply(offset));
		
		c.setup(eye, ref, c.getUp());
	}
	
	private static double fade(double p)
	{
		if(p < 0 || p > 1)
			throw new IllegalArgumentException();
		
		return (p <= 0.5) ? (2 * p * p) : (1 - 2 * (1 - p) * (1 - p));
	}
}
