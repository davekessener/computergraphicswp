package wp_cg.lab.grammar;

import wp_cg.lab.math.IVec3;
import wp_cg.lab.util.Utils;
import wp_cg.platformer.levelgenerator.Gamefield;
import wp_cg.platformer.levelgenerator.Grammar;

public class Evaluator
{
	private final Grammar mGrammar;
	
	public Evaluator(Grammar g)
	{
		mGrammar = g;
	}
	
	public Gamefield derive(Gamefield f, int n)
	{
		for(; n > 0 ; --n)
		{
			Utils.shuffle(IVec3.stream(IVec3.ORIGIN, new IVec3(f.getResX(), 1, f.getResZ()))).forEach(i -> {
				if(!f.isNew(i.X, i.Z))
				{
					String sym = f.getSymbol(i.X, i.Z);
					
					Utils.shuffle(mGrammar.rules())
						.filter(r -> r.canBeAppliedTo(sym))
						.filter(r -> f.contains(r.calculateEnd(i)))
						.filter(r -> f.areFree(i, r.calculateEnd(i)))
						.findFirst()
						.ifPresent(rule ->
					{
						rule.forEachSucc(i, (j, s) -> f.set(j.X, j.Z, s));
					});
				}
			});
			
			f.reset();
		}
		return f;
	}
}
