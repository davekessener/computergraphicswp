package wp_cg.lab.grammar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import wp_cg.backend.math.Vector;
import wp_cg.lab.math.IVec3;
import wp_cg.lab.util.Callback;
import wp_cg.lab.util.JsonConverter;
import wp_cg.lab.util.ValueRelay;
import wp_cg.platformer.Brick;
import wp_cg.platformer.Constants;
import wp_cg.platformer.levelgenerator.Gamefield;

public final class GamefieldConverter
{
	private final Map<String, Object> mWorld = new HashMap<>();
	private final Map<String, Object> mGamestate = new HashMap<>();
	private final List<Object> mBricks = new ArrayList<>();
	private final List<Object> mSwitchBricks = new ArrayList<>();
	private final List<Object> mEnemies = new ArrayList<>();
	private final List<Object> mCoins = new ArrayList<>();
	private final ValueRelay mRelay = new ValueRelay(this);
	
	private Map<String, Object> doConvert(Gamefield f)
	{
		Map<String, Object> root = new HashMap<>();
		
		IVec3.stream(IVec3.ORIGIN, new IVec3(f.getResX(), 1, f.getResZ())).forEach(i -> {
			mRelay.call(f.getSymbol(i.X, i.Z), i.X, i.Z);
		});
		
		mWorld.put("numRows", f.getResX());
		mWorld.put("numCols", f.getResZ());
		mWorld.put("maxOffset", 1);
		mWorld.put("bricks", mBricks);
		
		mGamestate.put("switchBricks", mSwitchBricks);
		
		root.put("world", mWorld);
		root.put("dynamicGameState", mGamestate);
		root.put("enemies", mEnemies);
		root.put("coins", mCoins);
		
		return root;
	}
	
	@Callback
	private void processEmptyBlock(int x, int z)
	{
	}
	
	@Callback(Constants.GRAMMAR_COIN)
	private void processCoin(int x, int z)
	{
		processBlock(x, z);
		
		mCoins.add(createEntity(x, z));
	}
	
	@Callback(Constants.GRAMMAR_ENEMY)
	private void processEnemy(int x, int z)
	{
		processBlock(x, z);
		
		mEnemies.add(createEntity(x, z));
	}
	
	@Callback(Constants.GRAMMAR_START_BLOCK)
	private void processStartBlock(int x, int z)
	{
		if(mGamestate.containsKey("startBrick"))
			throw new IllegalArgumentException("Duplicate start brick @" + x + "," + z + "!");
		
		Map<String, Object> startBrick = createBrick(x, z, Brick.Type.GROUND);
		
		mBricks.add(startBrick);
		mGamestate.put("startBrick", startBrick);
		mGamestate.put("startOrientation", convertVector(Vector.VECTOR_3_X));
	}
	
	@Callback(Constants.GRAMMAR_REGULAR_BLOCK)
	private void processBlock(int x, int z)
	{
		mBricks.add(createBrick(x, z, Brick.Type.GROUND));
	}
	
	@Callback(Constants.GRAMMAR_BREAKING_BLOCK)
	private void processBreakingBlock(int x, int z)
	{
		mBricks.add(createBrick(x, z, Brick.Type.BREAKING));
	}
	
	@Callback(Constants.GRAMMAR_END_BLOCK)
	private void processEndBlock(int x, int z)
	{
		processBlock(x, z);
	}
	
	@Callback(Constants.GRAMMAR_X_DIR)
	private void processBlockX(int x, int z)
	{
		processBlock(x, z);
	}
	
	@Callback(Constants.GRAMMAR_Z_DIR)
	private void processBlockZ(int x, int z)
	{
		processBlock(x, z);
	}
	
	@Callback(Constants.GRAMMAR_SWITCH_X_Z)
	private void processSwitchX(int x, int z)
	{
		mSwitchBricks.add(createSwitch(x, z, Vector.VECTOR_3_X, Vector.VECTOR_3_Z));
	}
	
	@Callback(Constants.GRAMMAR_SWITCH_Z_X)
	private void processSwitchZ(int x, int z)
	{
		mSwitchBricks.add(createSwitch(x, z, Vector.VECTOR_3_Z, Vector.VECTOR_3_X));
	}
	
	@Callback(Constants.GRAMMAR_LEFTRIGHT)
	private void processTurnX(int x, int z)
	{
		mSwitchBricks.add(createSwitch(x, z, Vector.VECTOR_3_Z, Vector.VECTOR_3_Z.multiply(-1)));
	}
	
	@Callback(Constants.GRAMMAR_UPDOWN)
	private void processTurnZ(int x, int z)
	{
		mSwitchBricks.add(createSwitch(x, z, Vector.VECTOR_3_X, Vector.VECTOR_3_X.multiply(-1)));
	}
	
	private static Map<String, Object> createEntity(int x, int z)
	{
		Map<String, Object> entity = new HashMap<>();
		
		entity.put("brick", createBrick(x, z, Brick.Type.NONE));
		
		return entity;
	}
	
	private static Map<String, Object> createSwitch(int x, int z, Vector a, Vector b)
	{
		Map<String, Object> brick = createBrick(x, z, Brick.Type.SWITCH);
		List<Object> switches = new ArrayList<>();
		
		switches.add(createDirChange(a, b));
		switches.add(createDirChange(b, a));
		
		if(!a.equals(b.multiply(-1)))
		{
			switches.add(createDirChange(a.multiply(-1), b));
			switches.add(createDirChange(b.multiply(-1), a));
		}
		
		brick.put("switches", switches);
		
		return brick;
	}
	
	private static Map<String, Object> createBrick(int x, int z, Brick.Type t)
	{
		Map<String, Object> b = new HashMap<>();
		
		b.put("row", x);
		b.put("column", z);
		b.put("offset", 0);
		b.put("type", t.toString());
		
		return b;
	}

	private static Map<String, Object> createDirChange(Vector a, Vector b)
	{
		Map<String, Object> r = new HashMap<>();
		
		r.put("from", convertVector(a));
		r.put("to", convertVector(b));
		
		return r;
	}
	
	private static List<Object> convertVector(Vector v)
	{
		List<Object> r = new ArrayList<>();
		
		for(int i = 0 ; i < v.getDimension() ; ++i)
		{
			r.add(v.get(i));
		}
		
		return r;
	}
	
	public static JSONObject convert(Gamefield f)
	{
		return JsonConverter.convert((new GamefieldConverter()).doConvert(f));
	}
}
