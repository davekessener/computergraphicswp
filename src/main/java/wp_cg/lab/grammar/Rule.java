package wp_cg.lab.grammar;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import wp_cg.platformer.levelgenerator.IRule;

public class Rule implements IRule
{
	private final String mPred;
	private final List<String> mSucc;
	private final DIR mDir;
	
	public Rule(String p, DIR d, String ... s)
	{
		mPred = p;
		mSucc = Collections.unmodifiableList(Arrays.asList(s));
		mDir = d;
		
		if(mPred == null || mDir == null)
		{
			throw new NullPointerException();
		}
		
		if(mPred.length() != 1 || mSucc.stream().anyMatch(ss -> ss.length() != 1))
		{
			throw new IllegalArgumentException();
		}
	}
	@Override
	public boolean canBeAppliedTo(String s)
	{
		return mPred.equals(s);
	}

	@Override
	public List<String> getSucc()
	{
		return mSucc;
	}

	@Override
	public DIR getDir()
	{
		return mDir;
	}
	
	public static Rule fromString(String s)
	{
		Matcher m = PTRN_RULE.matcher(s);
		
		if(!m.find())
		{
			throw new IllegalArgumentException("Malformed rule: " + s);
		}
		
		String pred = m.group(1);
		String[] succ = m.group(2).trim().split("[ \t]+");
		DIR dir = DIR.valueOf(m.group(3));
		
		return new Rule(pred, dir, succ);
	}
	
	private static final Pattern PTRN_RULE = Pattern.compile("[ \t]*([^ \t])[ \t]*-->[ \t]*((?:[^ \t][ \t]*)+)[ \t]*:[ \t]*([^ \t])[ \t\n]*");
}
