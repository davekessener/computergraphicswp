package wp_cg.lab.math;

import java.util.Arrays;
import java.util.stream.Stream;

public class CombinedSpline implements Spline
{
	private final Spline[] mParts;
	
	public CombinedSpline(Spline[] s)
	{
		if(Stream.of(s).anyMatch(p -> p == null))
			throw new NullPointerException();
		
		mParts = Arrays.copyOf(s, s.length);
	}

	@Override
	public Line apply(double v)
	{
		if(v < 0 || v > 1)
			throw new IllegalArgumentException();
		
		double s = v * mParts.length;
		
		return mParts[Math.min((int) s, mParts.length - 1)].apply(v == 1 ? 1 : (s - (int) s));
	}
}
