package wp_cg.lab.math;

import java.util.function.DoubleUnaryOperator;

import wp_cg.backend.math.Vector;

public class CubicHermiteSpline implements Spline
{
	private final Line mBegin, mEnd;
	
	public CubicHermiteSpline(Line b, Line e)
	{
		mBegin = b;
		mEnd = e;
	}

	@Override
	public Line apply(double v)
	{
		if(v < 0 || v > 1)
			throw new IllegalArgumentException();
		
		double h0 = H[0].applyAsDouble(v);
		double h1 = H[1].applyAsDouble(v);
		double h2 = H[2].applyAsDouble(v);
		double h3 = H[3].applyAsDouble(v);
		
		Vector c0 = mBegin.getBase().multiply(h0);
		Vector c1 = mBegin.getDirection().multiply(h1);
		Vector c2 = mEnd.getDirection().multiply(h2);
		Vector c3 = mEnd.getBase().multiply(h3);

		Vector m0 = mBegin.getDirection().multiply(h0);
		Vector m1 = mEnd.getDirection().multiply(h3);
		
		Vector p = c0.add(c1).add(c2).add(c3);
		Vector t = m0.add(m1);
		
		return new Line(p, t);
	}

	public static Spline ofClosedPath(Vector ... v)
	{
		int n = v.length;
		Line[] params = new Line[n];
		Spline[] parts = new Spline[n];
		
		for(int i = 0 ; i < n ; ++i)
		{
			params[i] = new Line(new Vector(v[i]), v[(i + 1) % n].subtract(v[(i + n - 1) % n]));
		}
		
		for(int i = 0 ; i < n ; ++i)
		{
			parts[i] = new CubicHermiteSpline(params[i], params[(i + 1) % n]);
		}
		
		return new CombinedSpline(parts);
	}
	
	private static final DoubleUnaryOperator[] H = new DoubleUnaryOperator[] {
		t -> (1 - t) * (1 - t) * (1 + 2 * t),
		t -> t * (1 - t) * (1 - t),
		t -> -t * t * (1 - t),
		t -> (3 - 2 * t) * t * t
	};
}
