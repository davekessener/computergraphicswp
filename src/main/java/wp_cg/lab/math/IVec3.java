package wp_cg.lab.math;

import java.util.Iterator;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import wp_cg.backend.math.Vector;
import wp_cg.lab.util.ImmutableMapBuilder;
import wp_cg.platformer.Brick;

public final class IVec3
{
	public static final IVec3 ORIGIN = new IVec3(0, 0, 0);
	
	public final int X, Y, Z;
	
	public IVec3(int x, int y, int z)
	{
		X = x;
		Y = y;
		Z = z;
	}

	public IVec3 add(IVec3 v) { return new IVec3(X + v.X, Y + v.Y, Z + v.Z); }
	public IVec3 sub(IVec3 v) { return new IVec3(X - v.X, Y - v.Y, Z - v.Z); }
	public IVec3 to(IVec3 v) { return v.sub(this); }
	public int dot(IVec3 v) { return X * v.X + Y * v.Y + Z * v.Z; }
	public Vector toVector() { return new Vector(X, Y, Z); }

	public static IVec3 adjacent(Brick.Side s)
	{
		IVec3 v = ADJACENT.get(s);
		
		if(v == null)
			throw new IllegalArgumentException("Unknown side " + s + "!");
		
		return v;
	}
	
	public static Vector normal(Brick.Side s)
	{
		return adjacent(s).toVector();
	}
	
	public static Stream<IVec3> stream(IVec3 u, IVec3 v)
	{
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(new Iterator<IVec3>() {
			private final IVec3 d = u.to(v);
			private int idx = 0;

			@Override
			public boolean hasNext()
			{
				return idx < d.X * d.Y * d.Z;
			}

			@Override
			public IVec3 next()
			{
				int dx = (idx % d.X);
				int dy = ((idx / d.X) % d.Y);
				int dz = (idx / d.X / d.Y);

				++idx;
				
				return u.add(new IVec3(dx, dy, dz));
			}
			
		}, Spliterator.ORDERED), false);
	}
	
	@Override
	public int hashCode()
	{
		return (Integer.hashCode(X) * 3) ^ (Integer.hashCode(Y) * 7) ^ (Integer.hashCode(Z) * 13);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o == this)
		{
			return true;
		}
		else if(o instanceof IVec3)
		{
			IVec3 v = (IVec3) o;
			
			return X == v.X && Y == v.Y && Z == v.Z;
		}
		else
		{
			return false;
		}
	}
	
	@Override
	public String toString()
	{
		return "(" + X + " | " + Y + " | " + Z + ")";
	}
	
	private static final Map<Brick.Side, IVec3> ADJACENT = (new ImmutableMapBuilder<Brick.Side, IVec3>())
		.put(Brick.Side.X_NEG, new IVec3(-1, 0, 0))
		.put(Brick.Side.X_POS, new IVec3( 1, 0, 0))
		.put(Brick.Side.Y_NEG, new IVec3(0, -1, 0))
		.put(Brick.Side.Y_POS, new IVec3(0,  1, 0))
		.put(Brick.Side.Z_NEG, new IVec3(0, 0, -1))
		.put(Brick.Side.Z_POS, new IVec3(0, 0,  1))
		.build();
}
