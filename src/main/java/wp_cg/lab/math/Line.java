package wp_cg.lab.math;

import wp_cg.backend.math.Vector;

public class Line
{
	private final Vector mBase, mDir;
	
	public Line(Vector b, Vector d)
	{
		mBase = new Vector(b);
		mDir = new Vector(d);
	}
	
	public Line translate(Vector v)
	{
		return new Line(mBase.add(v), mDir);
	}
	
	public static Line between(Vector a, Vector b)
	{
		return new Line(a, b.subtract(a));
	}
	
	public Vector getBase() { return mBase; }
	public Vector getDirection() { return mDir; }
	public Vector apply(double t) { return mBase.add(mDir.multiply(t)); }
}
