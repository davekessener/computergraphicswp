package wp_cg.lab.math;

import java.util.stream.Stream;

import wp_cg.backend.math.Vector;

public class ProtoTriangle
{
	public final Vector[] vertices;
	public final Vector[] tex_coords;
	
	public ProtoTriangle(Vector a, Vector b, Vector c) { this(a, b, c, null, null, null); }
	public ProtoTriangle(Vector va, Vector vb, Vector vc, Vector ta, Vector tb, Vector tc)
	{
		vertices = new Vector[] { va, vb, vc };
		
		if(!Stream.of(vertices).allMatch(v -> v.getDimension() == 3))
			throw new IllegalArgumentException();
		
		if(ta != null || tb != null || tc != null)
		{
			tex_coords = new Vector[] { ta, tb, tc };
			
			if(!Stream.of(tex_coords).allMatch(v -> v.getDimension() == 2))
				throw new IllegalArgumentException();
		}
		else
		{
			tex_coords = null;
		}
	}
}
