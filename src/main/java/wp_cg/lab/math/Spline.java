package wp_cg.lab.math;

import java.util.function.DoubleFunction;

public interface Spline extends DoubleFunction<Line>
{
}
