package wp_cg.lab.math;

import java.util.function.Function;

import wp_cg.backend.math.Vector;

public class TransformedSpline implements Spline
{
	private final Spline mSuper;
	private final Function<Vector, Vector> mTransform;
	
	public TransformedSpline(Spline s, Function<Vector, Vector> f)
	{
		mSuper = s;
		mTransform = f;
	}

	@Override
	public Line apply(double v)
	{
		Line r = mSuper.apply(v);
		
		return new Line(mTransform.apply(r.getBase()), r.getDirection().getNormalized());
	}
}
