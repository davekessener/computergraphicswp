package wp_cg.lab.plugins;

import wp_cg.backend.scenegraph.INode;
import wp_cg.platformer.GameEvent;
import wp_cg.platformer.PlatformerPlugin;

public abstract class BasePlugin extends PlatformerPlugin
{
	private final String mName;
	
	protected BasePlugin(String name)
	{
		if(name == null || name.length() == 0)
			throw new IllegalArgumentException();
		
		mName = name;
	}
	@Override
	public void init()
	{
	}

	@Override
	public void updateGameState()
	{
	}

	@Override
	public INode getSceneGraphContent()
	{
		return null;
	}

	@Override
	public void handleJson(String key, Object value)
	{
	}

	@Override
	public String getPluginName()
	{
		return mName;
	}

	@Override
	public void handleGameEvent(GameEvent gameEvent)
	{
	}
}
