package wp_cg.lab.plugins;

import wp_cg.lab.scene.BrickParticleObject;
import wp_cg.platformer.Brick;
import wp_cg.platformer.GameEvent;
import wp_cg.platformer.GameEventQueue;
import wp_cg.platformer.World;

public class BreakingBrickPlugin extends BasePlugin
{
	public BreakingBrickPlugin()
	{
		super(NAME);
	}

	@Override
	public void handleGameEvent(GameEvent e)
	{
		switch(e.getType())
		{
			case BREAKING_BRICK:
				onBrickBreak((Brick) e.getPayload());
				break;
			
			default:
				break;
		}
	}
	
	private void onBrickBreak(Brick b)
	{
		World w = getGame().getWorld();
		
		getGame().addGameObject(new BrickParticleObject(getGame(), w.getBrickOrigin(b)));
		
		w.deleteBrick(b);
		
		GameEventQueue.getInstance().emitEvent(new GameEvent(GameEvent.Type.REGENERATE_WORLD));
	}
	
	private static final String NAME = "Brick Breaking";
}
