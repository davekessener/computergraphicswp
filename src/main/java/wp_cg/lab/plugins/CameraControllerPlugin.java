package wp_cg.lab.plugins;

import wp_cg.backend.math.Vector;
import wp_cg.backend.scenegraph.Camera;
import wp_cg.lab.CameraAnimation;
import wp_cg.lab.DefaultCameraAnimation;
import wp_cg.lab.RotationCameraAnimation;
import wp_cg.lab.math.Line;
import wp_cg.lab.util.Property;
import wp_cg.lab.util.SimpleProperty;
import wp_cg.lab.util.Utils;
import wp_cg.platformer.GameEvent;

public class CameraControllerPlugin extends BasePlugin
{
	private final Property<CameraAnimation> mCtrl;
	private final Property<Vector> mLastOrientation;
	
	public CameraControllerPlugin()
	{
		super(NAME);
		
		mCtrl = new SimpleProperty<>();
		mLastOrientation = new SimpleProperty<>();
	}
	
	@Override
	public void init()
	{
		resetAnimation();
	}

	@Override
	public void updateGameState()
	{
		mLastOrientation.set(new Vector(getGame().getDynamicGameState().getOrientation()));
		
		if(mCtrl.get() == null)
			return; // because update can happen before init for some reason
		
		if(mCtrl.get().done())
		{
			resetAnimation();
		}
		
		mCtrl.get().step();
	}
	
	@Override
	public void handleGameEvent(GameEvent e)
	{
		switch(e.getType())
		{
			case SWITCH:
				getGame().getDynamicGameState().setIsRotating(true);
				mCtrl.set(new RotationCameraAnimation(computeChangeAngle(), getChangeTrack(), STEPS, ANIMATION_TIME));
				break;
				
			default:
		}
	}
	
	private double computeChangeAngle()
	{
		return Utils.computeDirectedAngle(mLastOrientation.get(), getGame().getDynamicGameState().getOrientation());
	}
	
	private Line getChangeTrack()
	{
		return Line.between(Camera.getInstance().getRef(), getGame().getDynamicGameState().getPosition());
	}

	private void resetAnimation()
	{
		getGame().getDynamicGameState().setIsRotating(false);
		mCtrl.set(new DefaultCameraAnimation(getGame().getDynamicGameState(), getGame().getWorld().getBrickSize() * 0.75));
	}

	private static final int STEPS = 15;
	private static final int ANIMATION_TIME = 15;
	
	private static final String NAME = "Camera Controller";
}
