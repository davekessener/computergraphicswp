package wp_cg.lab.plugins;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import wp_cg.backend.math.AxisAlignedBoundingBox;
import wp_cg.lab.math.Line;
import wp_cg.lab.scene.BaseMonster;
import wp_cg.lab.util.Overloaded;
import wp_cg.lab.util.Relay;
import wp_cg.lab.util.Utils;
import wp_cg.platformer.Coin;
import wp_cg.platformer.GameEvent;
import wp_cg.platformer.GameEventQueue;
import wp_cg.platformer.GameObject;
import wp_cg.platformer.GameObject.Type;
import wp_cg.platformer.Hazelnut;
import wp_cg.platformer.Player;

public class CollisionPlugin extends BasePlugin
{
	private final Relay mRelay;
	private final Set<Collision> mCollisions;
	
	public CollisionPlugin()
	{
		super(NAME);
		
		mRelay = new Relay(this);
		mCollisions = new HashSet<>();
	}
	
	@Override
	public void updateGameState()
	{
		AxisAlignedBoundingBox player = getAABB(getGame().getPlayer());
		
		List<GameObject> monsters = getGame().gameObjects()
			.filter(o -> o.getType() == Type.MONSTER)
			.collect(Collectors.toList());

		List<GameObject> coins = getGame().gameObjects()
			.filter(o -> o.getType() == Type.COIN)
			.collect(Collectors.toList());

		List<GameObject> nuts = getGame().gameObjects()
			.filter(o -> o.getType() == Type.HAZELNUT)
			.collect(Collectors.toList());
		
		Stream.concat(monsters.stream(), coins.stream()).forEach(o -> {
			if(Utils.overlaps(player, getAABB(o)))
			{
				collide(getGame().getPlayer(), o);
			}
		});
		
		nuts.forEach(o -> {
			Hazelnut nut = (Hazelnut) o;
			
			monsters.forEach(m -> {
				if(Utils.collides(new Line(nut.getPosition(), nut.getDirection()), getAABB(m)))
				{
					collide(nut, m);
				}
			});
		});
		
		mCollisions.removeIf(Collision::isExpired);
	}
	
	private void collide(GameObject a, GameObject b)
	{
		Collision collision = mCollisions.stream()
				.filter(c -> c.involves(a, b))
				.findFirst()
				.orElseGet(() ->
		{
			Collision c = new Collision(a, b);
			
			mCollisions.add(c);
			mRelay.call(a, b);
			
			return c;
		});
		
		collision.push();
	}
	
	@Overloaded
	private void collidePlayerMonster(Player a, BaseMonster b)
	{
		GameEventQueue.getInstance().emitEvent(new GameEvent(GameEvent.Type.COLLISION_MONSTER, b));
	}
	
	@Overloaded
	private void collidePlayerCoin(Player a, Coin b)
	{
		GameEventQueue.getInstance().emitEvent(new GameEvent(GameEvent.Type.COLLISION_COIN, b));
	}
	
	@Overloaded
	private void collideHazelnutMonster(Hazelnut n, BaseMonster m)
	{
		GameEventQueue.getInstance().emitEvent(new GameEvent(GameEvent.Type.HAZELNUT_HIT_MONSTER, m));
	}
	
	private AxisAlignedBoundingBox getAABB(GameObject o)
	{
		AxisAlignedBoundingBox aabb = o.getBoundingBox().clone();
		
		aabb.transform(o.getCombinedTransformation());
		
		return aabb;
	}

	private static final class Collision
	{
		private final GameObject mFirst, mSecond;
		private int mCount;
		
		public Collision(GameObject a, GameObject b)
		{
			mFirst = a;
			mSecond = b;
			mCount = 1;
		}
		
		public void push() { ++mCount; }
		public boolean isExpired() { return --mCount == 0; }
		public boolean involves(GameObject a, GameObject b)
		{
			return (mFirst.equals(a) && mSecond.equals(b))
				|| (mFirst.equals(b) && mSecond.equals(a));
		}
		
		@Override
		public int hashCode()
		{
			return mFirst.hashCode() ^ mSecond.hashCode();
		}
		
		@Override
		public boolean equals(Object o)
		{
			if(o instanceof Collision)
			{
				return ((Collision) o).involves(mFirst, mSecond);
			}
			
			return false;
		}
	}
	
	private static final String NAME = "Collision";
}
