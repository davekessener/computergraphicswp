package wp_cg.lab.plugins;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import wp_cg.lab.scene.FlyingMonster;

public class FlyingMonsterPlugin extends BasePlugin
{
	public FlyingMonsterPlugin()
	{
		super(NAME);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleJson(String key, Object value)
	{
		if(key.equals(KEY_ENEMIES))
		{
			((JSONArray) value).forEach(e -> createMonster((JSONObject) e));
		}
	}
	
	private void createMonster(JSONObject o)
	{
		FlyingMonster m = new FlyingMonster(getGame());
		
		m.fromJson(o);
		getGame().addGameObject(m);
	}
	
	private static final String KEY_ENEMIES = "enemies";
	
	private static final String NAME = "Flying Monsters";
}
