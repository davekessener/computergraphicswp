package wp_cg.lab.plugins;

import java.util.stream.Stream;

import wp_cg.backend.scenegraph.INode;
import wp_cg.backend.scenegraph.InnerNode;
import wp_cg.backend.scenegraph.TranslationNode;
import wp_cg.lab.BrickMeshBuilder;
import wp_cg.lab.math.IVec3;
import wp_cg.platformer.Brick;
import wp_cg.platformer.GameEvent;
import wp_cg.platformer.World;

public class WorldMeshGeneratorPlugin extends BasePlugin
{
	private final InnerNode mWorldMesh;
	
	public WorldMeshGeneratorPlugin()
	{
		super(NAME);
		
		mWorldMesh = new InnerNode();
	}
	
	@Override
	public INode getSceneGraphContent()
	{
		build(false);
		
		return mWorldMesh;
	}

	@Override
	public void handleGameEvent(GameEvent e)
	{
		switch(e.getType())
		{
			case REGENERATE_WORLD:
				build(true);
				break;
				
			default:
				break;
		}
	}
	
	private void build(boolean force)
	{
		if(force || mWorldMesh.getNumberOfChildren() == 0)
		{
			INode world_mesh = generateWorldMesh(getGame().getWorld());
			
			mWorldMesh.clearChildren();
			mWorldMesh.addChild(world_mesh);
		}
	}
	
	private static INode generateWorldMesh(World w)
	{
		int max_offset = w.getMaxHeightOffset();
		double brick_size = w.getBrickSize();
		
		BrickMeshBuilder b = new BrickMeshBuilder(TEXTURE, brick_size);
		TranslationNode t = new TranslationNode(w.getBrickOrigin(0, 0, 0));
		
		IVec3 min = new IVec3(                  0,    -max_offset,                   0);
		IVec3 max = new IVec3(w.getNumberOfRows(), 1 + max_offset, w.getNumberOfCols());
		
		IVec3.stream(min, max).forEach(v -> {
			Brick brick = w.getBrick(v.X, v.Z, v.Y);
			
			if(brick != null)
			{
				Stream.of(Brick.Side.values()).forEach(s -> {
					if(!w.hasNeighbor(v.X, v.Z, v.Y, s))
					{
						b.add(s, v, brick.getType());
					}
				});
			}
		});
		
		t.addChild(b.build());

		return t;
	}
	
	private static final String NAME = "World Mesh Generation";
	private static final String TEXTURE = "platformer_textures";
}
