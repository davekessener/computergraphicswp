package wp_cg.lab.scene;

import org.json.simple.JSONObject;

import wp_cg.backend.math.Vector;
import wp_cg.platformer.Game;
import wp_cg.platformer.JsonSerializable;

public abstract class BaseMonster extends ModularGameObject implements JsonSerializable
{
	protected BaseMonster(Model model, Game g, Vector p)
	{
		super(Type.MONSTER, g, p);
		
		addComponent(new ModelComponent(this, model));
	}

	@Override
	public void fromJson(JSONObject o)
	{
		require(StorageComponent.class).load(o);
	}
}
