package wp_cg.lab.scene;

import java.util.stream.Stream;

import wp_cg.backend.math.Vector;
import wp_cg.backend.scenegraph.INode;
import wp_cg.lab.BrickMeshBuilder;
import wp_cg.lab.math.IVec3;
import wp_cg.platformer.Brick;
import wp_cg.platformer.Game;

public class BrickParticleObject extends ModularGameObject
{
	public BrickParticleObject(Game g, Vector pos)
	{
		super(Type.RIGID_BODY_BRICK, g, pos);
		
		this.addChild(buildMesh("platformer_textures", g.getWorld().getBrickSize()));
		
		this.addComponent(new MassComponent(MASS));
		this.addComponent(new GravityComponent(GRAVITY));
		this.addComponent(new VoidComponent());
	}
	
	private static INode buildMesh(String fn, double l)
	{
		BrickMeshBuilder b = new BrickMeshBuilder(fn, l);
		
		Stream.of(Brick.Side.values()).forEach(s -> b.add(s, IVec3.ORIGIN, Brick.Type.BREAKING));
		
		return b.build();
	}
	
	private static final double MASS = 1;
	private static final double GRAVITY = -9.81 / 10;
}
