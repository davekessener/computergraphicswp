package wp_cg.lab.scene;

import org.json.simple.JSONObject;

import wp_cg.lab.util.Observable;
import wp_cg.lab.util.Property;
import wp_cg.lab.util.SimpleProperty;

public class BrickStorageComponent extends Observable implements StorageComponent
{
	private Property<Integer> mRow, mColumn, mOffset;
	
	public BrickStorageComponent()
	{
		mRow = new SimpleProperty<>(0);
		mColumn = new SimpleProperty<>(0);
		mOffset = new SimpleProperty<>(0);
	}
	
	public int getRow() { return mRow.get(); }
	public int getColumn() { return mColumn.get(); }
	public int getOffset() { return mOffset.get(); }
	public void setRow(int r) { mRow.set(r); }
	public void setColumn(int c) { mColumn.set(c); }
	public void setOffset(int o) { mOffset.set(o); }

	@Override
	public void load(JSONObject o)
	{
		JSONObject brick = (JSONObject) o.get("brick");
		
		mRow.set(((Long) brick.get("row")).intValue());
		mColumn.set(((Long) brick.get("column")).intValue());
		mOffset.set(((Long) brick.get("offset")).intValue());
		
		change();
	}

	@Override
	public JSONObject save()
	{
		throw new UnsupportedOperationException();
	}
}
