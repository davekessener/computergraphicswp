package wp_cg.lab.scene;

public interface DataComponent extends Component
{
	public default void update() { }
}
