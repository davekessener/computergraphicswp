package wp_cg.lab.scene;

import wp_cg.backend.math.Vector;
import wp_cg.lab.math.CubicHermiteSpline;
import wp_cg.lab.math.Spline;
import wp_cg.lab.math.TransformedSpline;
import wp_cg.lab.util.Utils;
import wp_cg.platformer.Game;

public class FlyingMonster extends BaseMonster
{
	public FlyingMonster(Game g)
	{
		super(new Model(Utils.readModel("monster"), FORWARD), g, Vector.ZERO_3);
		
		SimpleMovementComponent mv = generateMovement(g.getWorld().getBrickSize() / 2);
		BrickStorageComponent io = new BrickStorageComponent();
		
		addComponent(mv);
		addComponent(io);
		
		io.observe(o -> {
			Vector brick = g.getWorld().getBrickTopCenter(io.getRow(), io.getColumn(), io.getOffset());
			Vector offset = OFFSET.multiply(g.getWorld().getBrickSize());
			
			mv.offsetProperty().set(brick.add(offset));
		});
	}
	
	private static SimpleMovementComponent generateMovement(double scale)
	{
		return new SimpleMovementComponent(
					new TransformedSpline(PATH, v -> v.multiply(scale)),
					ANIMATION_DURATION,
					Vector.ZERO_3);
	}
	
	private static final Vector OFFSET = new Vector(0, 0.3, 0);
	private static final int ANIMATION_DURATION = 5 * 1000;
	private static final Vector FORWARD = new Vector(-1, 0, 0);
	private static final Spline PATH = CubicHermiteSpline.ofClosedPath(
		new Vector( 0.0,  0.0,  0.0),
		new Vector(-0.5,  0.1, -0.5),
		new Vector(-1.0,  0.3,  0.0),
		new Vector(-0.5,  0.1,  0.5),
		new Vector( 0.0,  0.0,  0.0),
		new Vector( 0.5, -0.1, -0.5),
		new Vector( 1.0, -0.3,  0.0),
		new Vector( 0.5, -0.1,  0.5)
	);
}
