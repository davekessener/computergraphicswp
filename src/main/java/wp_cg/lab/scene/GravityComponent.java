package wp_cg.lab.scene;

import wp_cg.backend.math.Vector;

public class GravityComponent implements UpdateableComponent
{
	private final double mAccel;
	
	public GravityComponent(double a)
	{
		mAccel = a;
	}
	
	@Override
	public void update(ModularGameObject self, double d)
	{
		self.require(MassComponent.class).accelerate(new Vector(0, mAccel, 0));
	}
}
