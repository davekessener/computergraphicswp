package wp_cg.lab.scene;

import wp_cg.backend.math.Vector;

public class MassComponent implements UpdateableComponent
{
	private final double mMass;
	private final Vector mAccel, mVelocity;
	
	public MassComponent(double mass)
	{
		mMass = mass;
		mAccel = new Vector(Vector.ZERO_3);
		mVelocity = new Vector(Vector.ZERO_3);
	}
	
	public void applyForce(Vector f) { mAccel.addSelf(f.multiply(1.0 / mMass)); }
	public void accelerate(Vector f) { mAccel.addSelf(f); }
	
	@Override
	public void update(ModularGameObject self, double d)
	{
		self.updatePosition(mVelocity.multiply(d));
		
		mVelocity.addSelf(mAccel.multiply(d));
		mAccel.set(0, 0, 0);
	}
}
