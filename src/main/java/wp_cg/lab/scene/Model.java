package wp_cg.lab.scene;

import wp_cg.backend.math.Vector;
import wp_cg.backend.scenegraph.INode;

public class Model
{
	public final INode node;
	public final Vector forward;
	
	public Model(INode n, Vector f)
	{
		node = n;
		forward = new Vector(f);
	}
}
