package wp_cg.lab.scene;

import wp_cg.backend.math.Vector;
import wp_cg.backend.scenegraph.RotationNode;
import wp_cg.lab.math.Line;
import wp_cg.lab.util.Utils;

public class ModelComponent implements UpdateableComponent
{
	private final Model mModel;
	private final RotationNode mTransform;
	
	public ModelComponent(ModularGameObject self, Model m)
	{
		mModel = m;
		mTransform = new RotationNode(Vector.VECTOR_3_Y, 0);
		
		self.setup(mModel.node.getBoundingBox());
		
		mTransform.addChild(mModel.node);
		self.addChild(mTransform);
	}
	
	public Model model() { return mModel; }
	public RotationNode transform() { return mTransform; }

	@Override
	public void update(ModularGameObject self, double d)
	{
		MovementComponent movement = self.require(MovementComponent.class);
		
    	Line c = movement.getOrientation();
    	
    	Vector facing = c.getDirection();
    	Vector forward = mModel.forward;
    	
    	double angle = Utils.computeDirectedAngle(forward, facing);
    	
    	mTransform.setAxis(Vector.VECTOR_3_Y);
    	mTransform.setAngle(angle);
    	
    	self.setPosition(c.getBase());
	}
}
