package wp_cg.lab.scene;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import wp_cg.backend.math.Vector;
import wp_cg.platformer.Game;
import wp_cg.platformer.GameObject;

public class ModularGameObject extends GameObject
{
	private final Game mGame;
	private final List<Component> mComponents;
	private long mLast;
	
	public ModularGameObject(Type type, Game w, Vector pos)
	{
		super(type, pos);
		
		mGame = w;
		mComponents = new ArrayList<>();
		mLast = System.currentTimeMillis();
	}
	
	protected Game getGame( ) { return mGame; }
	protected void addComponent(Component c) { mComponents.add(c); }
	
	public void kill()
	{
		mGame.removeGameObject(this);
	}

	@SuppressWarnings("unchecked")
	public <T extends Component> Optional<T> component(Class<? extends T> c)
	{
		return (Optional<T>) mComponents.stream().filter(e -> c.isAssignableFrom(e.getClass())).findFirst();
	}
	
	public <T extends Component> T require(Class<? extends T> c)
	{
		return component(c).get();
	}

	@Override
	public void updateGameState()
	{
		long now = System.currentTimeMillis();
		double d = (mLast - now) / 1000.0;
		
		mComponents.forEach(c -> {
			if(c instanceof UpdateableComponent)
			{
				((UpdateableComponent) c).update(this, d);
			}
		});
		
		mLast = now;
	}
}
