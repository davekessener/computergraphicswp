package wp_cg.lab.scene;

import wp_cg.lab.math.Line;

public interface MovementComponent extends Component
{
	public abstract Line getOrientation( );
}
