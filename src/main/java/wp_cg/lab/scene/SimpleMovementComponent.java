package wp_cg.lab.scene;

import java.util.function.DoubleFunction;

import wp_cg.backend.math.Vector;
import wp_cg.lab.math.Line;
import wp_cg.lab.util.Property;
import wp_cg.lab.util.SimpleProperty;

public class SimpleMovementComponent implements MovementComponent
{
	private final DoubleFunction<Line> mTransform;
	private final Property<Vector> mOffset;
	private final int mTime;
	private final long mStart;
	
	public SimpleMovementComponent(DoubleFunction<Line> f, int time, Vector o)
	{
		mTransform = f;
		mOffset = new SimpleProperty<>(new Vector(o));
		mTime = time;
		mStart = System.currentTimeMillis();
	}
	
	public Property<Vector> offsetProperty() { return mOffset; }

	@Override
	public Line getOrientation()
	{
		return mTransform.apply(current()).translate(mOffset.get());
	}
	
	private double current()
	{
		return (((int) (System.currentTimeMillis() - mStart)) % mTime) / (double) mTime;
	}
}
