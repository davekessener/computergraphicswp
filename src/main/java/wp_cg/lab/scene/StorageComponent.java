package wp_cg.lab.scene;

import org.json.simple.JSONObject;

public interface StorageComponent extends Component
{
	public abstract void load(JSONObject o);
	public abstract JSONObject save( );
	
	public default void update(ModularGameObject self, double d) { }
}
