package wp_cg.lab.scene;

public interface UpdateableComponent extends Component
{
	public abstract void update(ModularGameObject self, double d);
}
