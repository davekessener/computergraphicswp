package wp_cg.lab.scene;

public class VoidComponent implements UpdateableComponent
{
	@Override
	public void update(ModularGameObject self, double d)
	{
		if(self.getPosition().y() < MAX_DEPTH)
		{
			self.kill();
		}
	}
	
	private static final int MAX_DEPTH = -2;
}
