package wp_cg.lab.scenegraph;

import java.util.function.DoubleFunction;

import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.scenegraph.INode;
import wp_cg.backend.scenegraph.TransformationNode;
import wp_cg.lab.math.Line;
import wp_cg.lab.util.Utils;

public class FrameAnimationNode extends TransformationNode
{
	private final Vector mForward;
	private final DoubleFunction<Line> mTransform;
	private final int mSteps;
	private int mIdx;
	
	public FrameAnimationNode(INode n, Vector f, DoubleFunction<Line> cb, int time)
	{
		this.addChild(n);
		
		mForward = f;
		mTransform = cb;
		mSteps = time;
		mIdx = 0;
	}
	
	@Override
	public synchronized void traverse(Matrix mm)
	{
		if(++mIdx == mSteps)
		{
			mIdx = 0;
		}
		
		Line c = mTransform.apply(mIdx / (double) mSteps);
		Vector rotation_axis = mForward.cross(c.getDirection());
		double rotation_angle = Utils.computeDirectedAngle(mForward, c.getDirection());
		Matrix rotation = Matrix.createRotationMatrix4(rotation_axis, rotation_angle);
		Matrix translation = Matrix.createTranslationMatrix4(c.getBase());
		
		this.setTransformation(rotation.multiply(translation));
		
		super.traverse(mm);
	}
}
