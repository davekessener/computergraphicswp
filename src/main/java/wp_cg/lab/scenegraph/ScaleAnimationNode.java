package wp_cg.lab.scenegraph;

import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.scenegraph.INode;
import wp_cg.backend.scenegraph.InnerNode;
import wp_cg.backend.scenegraph.ScaleNode;

public class ScaleAnimationNode extends InnerNode
{
	private final int mFrameDelta;
	private int mIdx, mCount;
	
	public ScaleAnimationNode(int d)
	{
		mFrameDelta = d;
		mIdx = mCount = 0;
	}
	
	@Override
	public synchronized void traverse(Matrix mm)
	{
		if(++mCount == mFrameDelta)
		{
			mCount = 0;
			
			if(++mIdx == getNumberOfChildren())
			{
				mIdx = 0;
			}
		}
		
		getChild(mIdx).traverse(mm);
	}
	
	public static ScaleAnimationNode animate(INode base, int time, double d, int n)
	{
		ScaleAnimationNode node = new ScaleAnimationNode(time / n);
		
		for(int i = 0 ; i < n ; ++i)
		{
			double s = scale(i / (double) n, d);
			ScaleNode transform = new ScaleNode(new Vector(s, s, s));
			
			transform.addChild(base);
			node.addChild(transform);
		}
		
		return node;
	}
	
	private static double scale(double p, double d)
	{
		return (1 - d) + (1 + Math.cos(2 * p * Math.PI)) * d * 0.5;
	}
}
