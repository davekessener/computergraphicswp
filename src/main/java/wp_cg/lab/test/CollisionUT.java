package wp_cg.lab.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.stream.Stream;

import org.junit.Test;

import wp_cg.backend.math.AxisAlignedBoundingBox;
import wp_cg.backend.math.Vector;
import wp_cg.lab.math.Line;
import wp_cg.lab.util.Utils;

public class CollisionUT
{
	private static final Vector[] TRANS = new Vector[] {
		new Vector(0, 0, 0),
		new Vector(10, 0, 0),
		new Vector(0, 10, 0),
		new Vector(-10, 0, 0),
		new Vector(12, -34, 56)
	};
		
	@Test
	public void testAABB()
	{
		Stream.of(TRANS).forEach(t -> {
			AxisAlignedBoundingBox aabb0 = new AxisAlignedBoundingBox();
			AxisAlignedBoundingBox aabb1 = new AxisAlignedBoundingBox();
			
			aabb0.add(new Vector(0, 0, 0));
			aabb0.add(new Vector(1, 1, 1));
			
			aabb0.move(t);
			
			aabb1.add(new Vector(0.5, 0.5, 0.5));
			aabb1.add(new Vector(1.5, 1.5, 1.5));
			
			aabb1.move(t);
			
			assertTrue(Utils.overlaps(aabb0, aabb1));
			
			aabb1 = new AxisAlignedBoundingBox();
			
			aabb1.add(new Vector(0.5, 1.5, 0.5));
			aabb1.add(new Vector(1.5, 2.5, 1.5));

			aabb1.move(t);
			
			assertFalse(Utils.overlaps(aabb0, aabb1));
	
			aabb1 = new AxisAlignedBoundingBox();
			
			aabb1.add(new Vector(1.5, 0.5, 0.5));
			aabb1.add(new Vector(2.5, 1.5, 1.5));

			aabb1.move(t);
			
			assertFalse(Utils.overlaps(aabb0, aabb1));
			
			aabb1 = new AxisAlignedBoundingBox();
			
			aabb1.add(new Vector(0.5, 0.5, 1.5));
			aabb1.add(new Vector(1.5, 1.5, 2.5));

			aabb1.move(t);
			
			assertFalse(Utils.overlaps(aabb0, aabb1));
	
			aabb1 = new AxisAlignedBoundingBox();
	
			aabb1.add(new Vector(0.25, -1, 0.25));
			aabb1.add(new Vector(0.75, 2, 0.75));

			aabb1.move(t);
			
			assertTrue(Utils.overlaps(aabb0, aabb1));
		});
	}
	
	@Test
	public void testRay()
	{
		Stream.of(TRANS).forEach(t -> {
			AxisAlignedBoundingBox aabb = new AxisAlignedBoundingBox();
			
			aabb.add(new Vector(0, 0, 0));
			aabb.add(new Vector(1, 1, 1));
			
			aabb.move(t);
			
			assertTrue( Utils.collides((new Line(new Vector( 0.5,  0.5,  0.5), new Vector(1, 1, 1))).translate(t), aabb));
			assertFalse(Utils.collides((new Line(new Vector( 1.5,  1.5,  1.5), new Vector(1, 1, 1))).translate(t), aabb));
			assertFalse(Utils.collides((new Line(new Vector(-1.5, -1.5, -1.5), new Vector(1, 1, 1))).translate(t), aabb));
		});
	}
}
