package wp_cg.lab.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import wp_cg.backend.math.Vector;
import wp_cg.lab.math.CubicHermiteSpline;
import wp_cg.lab.math.Line;
import wp_cg.lab.math.Spline;

public class HermiteUT
{
	private static final Vector[] POINTS = new Vector[] {
		new Vector(-1, 0),
		new Vector(0, 1),
		new Vector(1, 0),
		new Vector(0, -1)
	};
	
	private static final Spline CIRCLE = CubicHermiteSpline.ofClosedPath(POINTS);
	
	@Test
	public void testIdentity()
	{
		Vector p = new Vector(3, 3);
		Vector m = new Vector(0, 0);
		
		Spline s = new CubicHermiteSpline(new Line(p, m), new Line(p, m));
		
		Line r0 = s.apply(0);
		Line r1 = s.apply(0.5);
		Line r2 = s.apply(1);
		
		assertEquals(r0.getBase(), p);
		assertEquals(r0.getDirection(), m);
		assertEquals(r1.getBase(), p);
		assertEquals(r1.getDirection(), m);
		assertEquals(r2.getBase(), p);
		assertEquals(r2.getDirection(), m);
	}
	
	@Test
	public void testLinearSegment()
	{
		Vector p0 = new Vector(3, 2);
		Vector p1 = new Vector(7, -2);
		Vector m = p1.subtract(p0);
		
		Spline s = new CubicHermiteSpline(new Line(p0, m), new Line(p1, m));
		
		Line r0 = s.apply(0.00);
		Line r1 = s.apply(0.25);
		Line r2 = s.apply(0.50);
		Line r3 = s.apply(0.75);
		Line r4 = s.apply(1.00);
		
		assertEquals(r0.getBase(), p0);
		assertEquals(r1.getBase(), p0.add(m.multiply(0.25)));
		assertEquals(r2.getBase(), p0.add(m.multiply(0.5)));
		assertEquals(r3.getBase(), p0.add(m.multiply(0.75)));
		assertEquals(r4.getBase(), p1);
		
		assertEquals(r0.getDirection(), m);
		assertEquals(r1.getDirection(), m);
		assertEquals(r2.getDirection(), m);
		assertEquals(r3.getDirection(), m);
		assertEquals(r4.getDirection(), m);
	}
	
	@Test
	public void testSegment()
	{
		Vector m0 = POINTS[1].subtract(POINTS[3]);
		Vector m1 = POINTS[2].subtract(POINTS[0]);
		
		Spline s = new CubicHermiteSpline(new Line(POINTS[0], m0), new Line(POINTS[1], m1));
		
		Line r0 = s.apply(0.0);
		Line r1 = s.apply(0.25);
		Line r2 = s.apply(0.5);
		Line r3 = s.apply(1.0);
		
		assertEquals(r0.getBase(), POINTS[0]);
		assertEquals(r1.getBase(), new Vector(-0.9375, 0.4375));
		assertEquals(r2.getBase(), new Vector(-0.75, 0.75));
		assertEquals(r3.getBase(), POINTS[1]);
		
		assertEquals(r0.getDirection(), m0);
		assertEquals(r2.getDirection(), new Vector(1, 1));
		assertEquals(r3.getDirection(), m1);
	}
	
	@Test
	public void testPoints()
	{
		Line p0 = CIRCLE.apply(0.0 / 4);
		Line p1 = CIRCLE.apply(1.0 / 4);
		Line p2 = CIRCLE.apply(2.0 / 4);
		Line p3 = CIRCLE.apply(3.0 / 4);
		Line p4 = CIRCLE.apply(4.0 / 4);
		
		assertEquals(p0.getBase(), POINTS[0]);
		assertEquals(p1.getBase(), POINTS[1]);
		assertEquals(p2.getBase(), POINTS[2]);
		assertEquals(p3.getBase(), POINTS[3]);
		assertEquals(p4.getBase(), POINTS[0]);
	}
}
