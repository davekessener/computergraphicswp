package wp_cg.lab.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ImmutableMapBuilder<K, V>
{
	private final Map<K, V> mMap;
	
	public ImmutableMapBuilder() { this(new HashMap<>()); }
	public ImmutableMapBuilder(Map<K, V> m)
	{
		mMap = m;
	}
	
	public ImmutableMapBuilder<K, V> put(K k, V v)
	{
		mMap.put(k, v);
		
		return this;
	}
	
	public Map<K, V> build()
	{
		return Collections.unmodifiableMap(mMap);
	}
}
