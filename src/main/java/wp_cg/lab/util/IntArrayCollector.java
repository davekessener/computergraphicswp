package wp_cg.lab.util;

import java.util.stream.IntStream;

public class IntArrayCollector
{
	private final int[] mContent;
	private int mIdx;
	
	public IntArrayCollector(int s)
	{
		mContent = new int[s];
		mIdx = 0;
	}
	
	public void add(int v)
	{
		if(mIdx == mContent.length)
			throw new IllegalStateException();
		
		mContent[mIdx++] = v;
	}
	
	public int[] get()
	{
		if(mIdx != mContent.length)
			throw new IllegalStateException();
		
		return mContent;
	}
	
	public static int[] collect(int l, IntStream in)
	{
		return in.collect(
			() -> new IntArrayCollector(l),
			(a, e) -> a.add(e),
			(a, b) -> { throw new UnsupportedOperationException(); })
		.get();
	}
}
