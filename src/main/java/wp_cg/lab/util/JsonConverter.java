package wp_cg.lab.util;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public final class JsonConverter
{
	private static final JsonConverter sInstance = new JsonConverter();
	
	private final Relay mRelay;
	
	private JsonConverter( )
	{
		mRelay = new Relay(this);
	}
	
	public static JSONObject convert(Object o)
	{
		return (JSONObject) sInstance.doConvert(o);
	}
	
	private Object doConvert(Object o)
	{
		return mRelay.call(o);
	}
	
	@SuppressWarnings("unchecked")
	@Overloaded
	private JSONObject doConvert(Map<String, Object> m)
	{
		JSONObject json = new JSONObject();
		
		m.entrySet().forEach(e -> json.put(e.getKey(), doConvert(e.getValue())));
		
		return json;
	}
	
	@SuppressWarnings("unchecked")
	@Overloaded
	private JSONArray doConvert(List<Object> a)
	{
		JSONArray json = new JSONArray();
		
		a.forEach(o -> json.add(doConvert(o)));
		
		return json;
	}
	
	@Overloaded
	private Long doConvert(Integer i)
	{
		return i.longValue();
	}
	
	@Overloaded
	private Object doConvertDefault(Object o)
	{
		return o;
	}
}
