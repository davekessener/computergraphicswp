package wp_cg.lab.util;

import java.util.ArrayList;
import java.util.List;

public abstract class Observable
{
	private final List<Observer> mObservers = new ArrayList<>();
	
	public Observable observe(Observer o)
	{
		mObservers.add(o);
		
		return this;
	}
	
	protected void change()
	{
		mObservers.forEach(o -> o.onChange(this));
	}
}
