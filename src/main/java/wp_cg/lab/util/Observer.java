package wp_cg.lab.util;

public interface Observer
{
	public abstract void onChange(Observable o);
}
