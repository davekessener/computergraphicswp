package wp_cg.lab.util;

public class OverloadException extends RuntimeException
{
	private static final long serialVersionUID = -4836631803578592215L;

	public OverloadException(Exception e) { super(e); }
}
