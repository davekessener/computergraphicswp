package wp_cg.lab.util;

public interface Property<T>
{
	public abstract T get( );
	public abstract void set(T v);
	public abstract boolean valid( );
}
