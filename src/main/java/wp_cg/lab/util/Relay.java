package wp_cg.lab.util;

import java.lang.reflect.Method;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Relay
{
	private final Object mSelf;

	protected Relay( )
	{
		mSelf = this;
	}

	public Relay(Object self)
	{
		if(self == null)
			throw new NullPointerException();
		
		mSelf = self;
	}

	@SuppressWarnings("unchecked")
	public <R> R call(Object ... a)
	{
		try
		{
			return (R) Stream.of(mSelf.getClass().getDeclaredMethods())
				.filter(m -> m.isAnnotationPresent(Overloaded.class))
				.filter(m -> m.getParameterCount() == a.length)
				.filter(new TypeFilter(a))
				.map(m -> new WeightedOption(calculateFitness(m, a), m))
				.filter(WeightedOption::valid)
				.sorted((x, y) -> Integer.compare(x.weight, y.weight))
				.findFirst().get()
				.invoke(mSelf, a);
		}
		catch(Exception e)
		{
			throw new OverloadException(e);
		}
	}

	private static int calculateFitness(Method m, Object[] a)
	{
		Class<?>[] p = m.getParameterTypes();

		if(p.length != a.length)
			throw new IllegalStateException("" + p.length + " != " + a.length);
		
		return (int) IntStream.range(0, a.length)
			.map(i -> quantifyRelation(p[i], a[i].getClass()))
			.sum();
	}

	private static int quantifyRelation(Class<?> parent, Class<?> child)
	{
		if(!parent.isAssignableFrom(child))
			return -1;
		
		int r = 0;
		while(child != null)
		{
			if(parent.equals(child))
				break;
			
			++r;
			
			if(Stream.of(child.getInterfaces()).anyMatch(i -> parent.equals(i)))
				break;
			
			child = child.getSuperclass();
		}

		if(child == null)
			return -1;
		
		return r;
	}

	private static class WeightedOption
	{
		public final int weight;
		public final Method method;

		public WeightedOption(int w, Method m)
		{
			this.weight = w;
			this.method = m;
		}
		
		public Object invoke(Object self, Object ... a) throws Exception
		{
			this.method.setAccessible(true);
			
			return this.method.invoke(self, a);
		}

		public boolean valid( ) { return weight >= 0; }
	}

	private static class TypeFilter implements Predicate<Method>
	{
		private final Object[] mArgs;

		public TypeFilter(Object[]  a)
		{
			mArgs = a;
		}

		@Override
		public boolean test(Method m)
		{
			Class<?>[] p = m.getParameterTypes();

			if(mArgs.length != p.length)
				return false;
			
			for(int i = 0 ; i < p.length ; ++i)
			{
				if(!p[i].isAssignableFrom(mArgs[i].getClass()))
					return false;
			}

			return true;
		}
	}
}
