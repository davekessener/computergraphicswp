package wp_cg.lab.util;

public class SimpleProperty<T> implements Property<T>
{
	private T mValue;
	
	public SimpleProperty( ) { this(null); }
	public SimpleProperty(T v)
	{
		mValue = v;
	}
	
	@Override
	public T get()
	{
		return mValue;
	}
	
	@Override
	public void set(T v)
	{
		mValue = v;
	}
	
	@Override
	public boolean valid()
	{
		return true;
	}
}
