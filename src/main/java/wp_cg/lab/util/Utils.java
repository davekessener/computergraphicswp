package wp_cg.lab.util;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import wp_cg.backend.math.AxisAlignedBoundingBox;
import wp_cg.backend.math.Vector;
import wp_cg.backend.mesh.ObjReader;
import wp_cg.backend.mesh.TriangleMesh;
import wp_cg.backend.scenegraph.INode;
import wp_cg.backend.scenegraph.InnerNode;
import wp_cg.backend.scenegraph.TriangleMeshNode;
import wp_cg.lab.math.Line;

public final class Utils
{
	public static <T> Stream<T> shuffle(Stream<T> t) { return shuffle(t, new Random(System.currentTimeMillis())); }
	public static <T> Stream<T> shuffle(Stream<T> t, Random rng)
	{
		List<T> l = t.collect(Collectors.toList());
		
		Collections.shuffle(l, rng);
		
		return l.stream();
	}
	
	public static boolean overlaps(AxisAlignedBoundingBox aabb0, AxisAlignedBoundingBox aabb1)
	{
		for(int i = 0 ; i < 3 ; ++i)
		{
			if(    aabb0.getUR().get(i) <= aabb1.getLL().get(i)
				|| aabb1.getUR().get(i) <= aabb0.getLL().get(i))
					return false;
		}
		
		return true;
	}
	
	public static boolean collides(Line l, AxisAlignedBoundingBox aabb)
	{
		double min = Double.NEGATIVE_INFINITY, max = Double.POSITIVE_INFINITY;
		
		for(int i = 0 ; i < 3 ; ++i)
		{
			double px = l.getBase().get(i);
			double vx = l.getDirection().get(i);
			double x0 = aabb.getLL().get(i);
			double x1 = aabb.getUR().get(i);
			
			if(vx == 0)
			{
				if(x1 < px || px < x0)
				{
					return false;
				}
			}
			else
			{
				if(vx < 0)
				{
					double t = x0; x0 = x1; x1 = t;
				}
				
				double t0 = (x0 - px) / vx;
				double t1 = (x1 - px) / vx;
				
				min = Math.max(min, t0);
				max = Math.min(max, t1);
				
				if(min >= max)
				{
					return false;
				}
			}
		}
		
		return min <= 0 && max >= 0;
	}
	
	public static double computeAngle(Vector a, Vector b)
	{
		return Math.acos(a.multiply(b) / (a.getNorm() * b.getNorm()));
	}
	
	public static double computeDirectedAngle(Vector a, Vector b)
	{
		return Math.signum(a.cross(b).y()) * computeAngle(a, b);
	}
	
	public static Vector interpolate(Vector a, Vector b, double p)
	{
		if(p < 0 || p > 1)
			throw new IllegalArgumentException();
		
		return a.add(b.subtract(a).multiply(p));
	}
	
	public static List<TriangleMesh> readMeshes(String id)
	{
		return (new ObjReader()).read("meshes/" + id + ".obj");
	}
	
	public static INode readModel(String id)
	{
		return readMeshes(id).stream()
			.map(TriangleMeshNode::new)
			.collect(
				() -> new InnerNode(),
				(n, m) -> n.addChild(m),
				(a, b) -> { throw new UnsupportedOperationException(); });
	}
	
	public static class Pair<U, V>
	{
		public final U first;
		public final V second;
		
		public Pair(U u, V v)
		{
			first = u;
			second = v;
		}
	}
	
	private Utils( ) { }
}
