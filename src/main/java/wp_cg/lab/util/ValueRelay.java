package wp_cg.lab.util;

import java.lang.reflect.InvocationTargetException;
import java.util.stream.Stream;

public class ValueRelay
{
	private final Object mSelf;
	
	public ValueRelay(Object self)
	{
		if(self == null)
			throw new NullPointerException();
		
		mSelf = self;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T call(String v, Object ... a)
	{
		final String id = (v == null ? "" : v);
		
		try
		{
			return (T) Stream.of(mSelf.getClass().getDeclaredMethods())
				.filter(m -> m.isAnnotationPresent(Callback.class))
				.filter(m -> m.getAnnotation(Callback.class).value().equals(id))
				.map(m -> { m.setAccessible(true); return m; })
				.findFirst().get()
				.invoke(mSelf, a);
		}
		catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException | SecurityException e)
		{
			throw new RuntimeException(e);
		}
	}
}
