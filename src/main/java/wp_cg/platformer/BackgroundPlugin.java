/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer;

import wp_cg.backend.scenegraph.FullscreenTextureNode;
import wp_cg.backend.scenegraph.INode;

/**
 * Render the background image
 */
public class BackgroundPlugin extends PlatformerPlugin {
    @Override
    public void init() {
    }

    @Override
    public void updateGameState() {
    }

    @Override
    public INode getSceneGraphContent() {
        return new FullscreenTextureNode("jungle_background.jpg");
    }

    @Override
    public void handleJson(String key, Object value) {
    }

    @Override
    public String getPluginName() {
        return "BackgroundPlugin";
    }

    @Override
    public void handleGameEvent(GameEvent gameEvent) {
    }
}
