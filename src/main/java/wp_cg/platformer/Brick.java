/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer;

import org.json.simple.JSONObject;

/**
 * Represents a brick in the world.
 */
public class Brick implements JsonSerializable {
    private static final String JSON_ROW = "row";
    private static final String JSON_COLUMN = "column";
    private static final String JSON_OFFSET = "offset";
    private static final String JSON_TYPE = "type";

    /*
     * This enum describes the different available bricks.
     */
    public enum Type {
        NONE, GROUND, SWITCH, BREAKING
    }

    /**
     * Enum for the six sides of a brick.
     */
    public enum Side {
        X_NEG, X_POS, Y_NEG, Y_POS, Z_NEG, Z_POS
    }

    /**
     * Row the brick is located in (is set when the brick is added to the world).
     */
    private int row = -1;

    /**
     * Column the brick is located in (is set when the brick is added to the world).
     */
    private int column = -1;

    /**
     * Index of the brick in the slot. 0 = ground level, negative values below, positive values above.
     */
    private int offset = -1;

    /**
     * Type of the brick
     */
    private Type type;

    public Brick() {
    }

    public void setup(Type type, int row, int column, int offset) {
        this.type = type;
        this.row = row;
        this.column = column;
        this.offset = offset;
    }

    @Override
    public void fromJson(JSONObject brickObject) {
        int row = ((Long) brickObject.get(JSON_ROW)).intValue();
        int column = ((Long) brickObject.get(JSON_COLUMN)).intValue();
        int offset = ((Long) brickObject.get(JSON_OFFSET)).intValue();
        String typeString = (String) brickObject.get(JSON_TYPE);
        Type type = Type.valueOf(typeString);
        setup(type, row, column, offset);
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public int getOffset() {
        return offset;
    }

    public Type getType() {
        return type;
    }

    protected void setType(Type type) {
        this.type = type;
    }
}
