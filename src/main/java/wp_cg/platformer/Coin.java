/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer;

import java.util.List;

import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.mesh.ObjReader;
import wp_cg.backend.mesh.TriangleMesh;
import wp_cg.backend.scenegraph.TransformationNode;
import wp_cg.backend.scenegraph.TriangleMeshNode;

/**
 * Coins are places in the world an can be collected.
 */
public class Coin extends GameObject {
    /**
     * Coin animation rotation.
     */
    private TransformationNode orientationNode = new TransformationNode();

    /**
     * Coin animation angle.
     */
    private double alpha = 0;


    public Coin(Vector pos) {
        super(Type.COIN, pos);

        // Test triangle mesh functionality
        ObjReader reader = new ObjReader();
        List<TriangleMesh> meshes = reader.read("meshes/coin.obj");
        if (meshes.size() != 1) {
            throw new IllegalArgumentException("Illegal coin mesh");
        }
        TriangleMeshNode node = new TriangleMeshNode(meshes.get(0));
        setup(meshes.get(0).getBoundingBox());
        orientationNode.addChild(node);
        addChild(orientationNode);
    }

    public void updateGameState() {
        alpha += 0.05;
        orientationNode.setTransformation(Matrix.createRotationMatrix4(Vector.VECTOR_3_Y, alpha));
    }
}
