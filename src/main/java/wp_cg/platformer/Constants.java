/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer;

import wp_cg.backend.math.Vector;

/**
 * This static class contains all the 'magic numbers' of the platformer game
 */
public class Constants {
    /**
     * Integration step size
     */
    public static final double INTEGRATION_STEP_SIZE = 0.02;

    /**
     * Gravitational accelleration vector (integration step size = 0.1)
     */
    public static final Vector GRAVITY = new Vector(0, -0.981, 0);

    /**
     * Positive constant 'close to zero'
     */
    public static final double EPSILON = 1e-5;

    /**
     * Movement speed of the player
     */
    public static final double PLAYER_SPEED = 0.03;

    /**
     * Width of a player jump
     */
    public static final double PLAYER_JUMP_LENGTH = 0.2;

    /**
     * Height of a player jump
     */
    public static final double PLAYER_JUMP_HEIGHT = 0.2;

    /**
     * Initial number of lives of the player.
     */
    public static final int START_NUMBER_PLAYER_LIFES = 3;

    /**
     * Number of seconds the player is invulnerable after loosing a live.
     */
    public static final int SECONDS_INVULNERABLE = 3;
    /**
     * Grammar symbols.
     */
    public static final String GRAMMAR_START_BLOCK = "B";
    public static final String GRAMMAR_END_BLOCK = "E";
    public static final String GRAMMAR_REGULAR_BLOCK = "G";
    public static final String GRAMMAR_BREAKING_BLOCK = "Y";
    public static final String GRAMMAR_ENEMY = "X";
    public static final String GRAMMAR_COIN = "C";
    public static final String GRAMMAR_Z_DIR = "\u2192";
    public static final String GRAMMAR_X_DIR = "\u2193";
    public static final String GRAMMAR_SWITCH_Z_X = "\u21b2";
    public static final String GRAMMAR_SWITCH_X_Z = "\u21b3";
    public static final String GRAMMAR_LEFTRIGHT = "\u21c4";
    public static final String GRAMMAR_UPDOWN = "\u21c5";
}
