/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer;

import wp_cg.backend.math.Vector;
import wp_cg.backend.scenegraph.*;

public class DisplayStatePlugin extends PlatformerPlugin {

    private double STATE_ICON_WIDTH = 0.1;
    private double STATE_ICON_HEIGHT = 0.1;

    private InnerNode stateNode = new InnerNode();
    private SpriteNode spriteNode;
    private RenderTextNode stringNode;
    private SpriteNode spriteNodeCoin;
    private SpriteNode barNode;

    @Override
    public void init() {
    }

    @Override
    public void updateGameState() {
    }

    @Override
    public INode getSceneGraphContent() {
        rebuildNode();
        return stateNode;
    }

    private void rebuildNode() {

        STATE_ICON_HEIGHT = STATE_ICON_WIDTH * Camera.getInstance().getAspectRatio();

        double border = 0.1;
        double titleBarHeight = 0.08;
        double coinStateX = 0.5;
        double stateY = 1 - border - titleBarHeight - STATE_ICON_HEIGHT;
        double barOffset = 0.05;
        if (barNode == null) {
            barNode = new SpriteNode(-1, stateY - barOffset,
                    2, STATE_ICON_HEIGHT + 2 * barOffset,
                    "semitransparent_white_square.png", 0.98);
        }
        if (spriteNodeCoin == null) {
            spriteNodeCoin = new SpriteNode(coinStateX, stateY,
                    STATE_ICON_WIDTH, STATE_ICON_HEIGHT,
                    "coin.png", 0.97);
        }
        if (spriteNode == null) {
            spriteNode = new SpriteNode(0, 0,
                    STATE_ICON_WIDTH, STATE_ICON_HEIGHT,
                    "squirrel.png", 0.97);
        }
        if (stringNode == null) {
            stringNode = new RenderTextNode(coinStateX + STATE_ICON_WIDTH, stateY,
                    STATE_ICON_HEIGHT,
                    "x0", 0.97);
        }

        // Rebuild state node
        stateNode.clearChildren();
        stateNode.addChild(barNode);
        stateNode.addChild(stringNode);
        stateNode.addChild(spriteNodeCoin);
        for (int i = 0; i < getGame().getPlayer().getNumberOfLifes(); i++) {
            TranslationNode translationNode = new TranslationNode(new Vector(-1 + border + i * STATE_ICON_WIDTH,
                    stateY, 0));
            translationNode.addChild(spriteNode);
            stateNode.addChild(translationNode);
        }
    }

    @Override
    public void handleJson(String key, Object value) {
    }

    @Override
    public String getPluginName() {
        return "Game State";
    }

    @Override
    public void handleGameEvent(GameEvent gameEvent) {
        if (gameEvent.getType() == GameEvent.Type.PLAYER_STATE_CHANGED) {
            if (stringNode != null) {
                stringNode.setText(String.format("x%d", getGame().getPlayer().getNumberOfCoins()));
                rebuildNode();
            }
        }
    }
}
