/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer;

/**
 * Events in the game are encapsulated by instances of this class
 */
public class GameEvent {
    /**
     * Each event has a unique type.
     */
    public enum Type {
        SWITCH,
        COLLISION_COIN,
        COLLISION_MONSTER,
        HAZELNUT_HIT_MONSTER,
        BREAKING_BRICK,
        REGENERATE_WORLD,
        PLAYER_STATE_CHANGED
    }

    /**
     * Event type.
     */
    private final Type type;

    /**
     * The payload may contain additional information about the event.
     */
    private Object payload;

    public GameEvent(Type type) {
        this(type, null);
    }

    public GameEvent(Type type, Object payload) {
        this.type = type;
        this.payload = payload;
    }

    public Object getPayload() {
        return payload;
    }

    public Type getType() {
        return type;
    }
}
