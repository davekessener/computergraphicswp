/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import wp_cg.lab.util.Property;
import wp_cg.lab.util.SimpleProperty;

/**
 * This singleton instance handles all event and passes them to however is
 * interested.
 */
public class GameEventQueue
{

	public static final String EVENT = "EVENT";

	/**
	 * Singleton instance.
	 */
	private static GameEventQueue instance;

	/**
	 * List of observers
	 */
	private List<PropertyChangeListener> observer = new ArrayList<>();

	private boolean mFrozen = false;
	private final Property<List<GameEvent>> mBacklog = new SimpleProperty<>(new ArrayList<>());

	/**
	 * Private constructor
	 */
	private GameEventQueue()
	{
	}

	public void freeze()
	{
		mFrozen = true;
	}

	public void unfreeze()
	{
		mFrozen = false;
		
		List<GameEvent> bl = mBacklog.get();
		mBacklog.set(new ArrayList<>());
		
		bl.forEach(e -> emitEvent(e));
	}

	/**
	 * Access to the singleton instance.
	 */
	public static GameEventQueue getInstance()
	{
		if(instance == null)
		{
			instance = new GameEventQueue();
		}
		return instance;
	}

	public void emitEvent(GameEvent gameEvent)
	{
		if(mFrozen)
		{
			mBacklog.get().add(gameEvent);
		}
		else
		{
			observer.forEach(o -> o.propertyChange(new PropertyChangeEvent(this, EVENT, gameEvent, gameEvent)));
		}
	}

	public void addObserver(PropertyChangeListener observer)
	{
		this.observer.add(observer);
	}
}
