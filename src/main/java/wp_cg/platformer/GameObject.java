/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer;

import wp_cg.backend.math.AxisAlignedBoundingBox;
import wp_cg.backend.math.Vector;
import wp_cg.backend.scenegraph.TranslationNode;

/**
 * Base class for all game object (e.g. player, monster, ...) in the game. The game object can be directly put
 * in the scene graph.
 */
public abstract class GameObject extends TranslationNode {
    /**
     * Current position of the player. The position is on the ground (below the players feet).
     */
    protected Vector pos = new Vector(0, 0, 0);

    /**
     * Available types of game objects.
     */
    public enum Type {
        PLAYER, MONSTER, COIN, HAZELNUT, RIGID_BODY_BRICK
    }

    /**
     * Type of the game object.
     */
    private final Type type;

    /**
     * Bounding box of the coin mesh.
     */
    protected AxisAlignedBoundingBox bbox;

    public GameObject(Type type, Vector pos) {
        super(Vector.ZERO_3);
        this.type = type;
        this.pos.copy(pos);
        setTranslation(pos);
    }

    /**
     * Call this method to setup the bounding box of the mesh.
     */
    public void setup(AxisAlignedBoundingBox bbox) {
        this.bbox = bbox;
    }

    public Vector getPosition() {
        return pos;
    }

    public void setPosition(Vector newPos) {
        pos.copy(newPos);
        setTranslation(pos);
    }

    public void updatePosition(Vector offset) {
        pos.addSelf(offset);
        setTranslation(pos);
    }

    public Type getType() {
        return type;
    }

    /**
     * Game state update callback
     */
    public abstract void updateGameState();

    @Override
    public AxisAlignedBoundingBox getBoundingBox() {
        return bbox;
    }
}
