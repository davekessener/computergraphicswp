/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer;

import java.util.List;

import wp_cg.backend.math.Vector;
import wp_cg.backend.mesh.ObjReader;
import wp_cg.backend.mesh.TriangleMesh;
import wp_cg.backend.scenegraph.TriangleMeshNode;

/**
 * The player can throw one haselnut at a time
 */
public class Hazelnut extends GameObject {

  /**
   * Hazelnut movement speed.
   */
  private static final double SPEED = 0.06;

  /**
   * Throwing direction of the haselnut.
   */
  private Vector direction = new Vector(0, 0, 0);

  public Hazelnut(Vector pos, Vector direction) {
    super(Type.HAZELNUT, pos);
    this.direction.copy(direction);
    ObjReader reader = new ObjReader();
    List<TriangleMesh> meshes = reader.read(
        "meshes/haselnut.obj");
    setup(meshes.get(0).getBoundingBox());
    addChild(new TriangleMeshNode(meshes.get(0)));
  }

  @Override
  public void updateGameState() {
    updatePosition(direction.multiply(SPEED));
  }

  public Vector getDirection() {
    return direction;
  }
}
