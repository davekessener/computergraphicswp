/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */
package wp_cg.platformer;

import org.json.simple.JSONObject;

/**
 * This interface is implemented by all classed that can be read from a JSON object.
 */
public interface JsonSerializable {

    /**
     * Read the instance state from a JSON object.
     */
    void fromJson(JSONObject jsonObject);
}
