/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer;

import wp_cg.backend.scenegraph.INode;

/**
 * Shared abstract class for all plugins for a game.
 */
public abstract class PlatformerPlugin {

    /**
     * Reference to the game the plugin is used for.
     */
    private Game game;

    /**
     * This setup method is called automatically when the plugin is added to a game.
     */
    public void setup(Game game) {
        this.game = game;
    }
    
    protected Game getGame() { return game; }

    /**
     * Perform all initialization (not in the constructor!).
     */
    public abstract void init();

    /**
     * This method is called for each game state main loop tick.
     */
    public abstract void updateGameState();

    /**
     * Return a node containing all the scene graph content for the plugin
     */
    public abstract INode getSceneGraphContent();

    /**
     * Handle a JSON key-value pair.
     */
    public abstract void handleJson(String key, Object value);

    /**
     * Return a name for the plugin (just for debugging).
     */
    public abstract String getPluginName();

    /**
     * Using this method all plugins are informed about game events.
     */
    public abstract void handleGameEvent(GameEvent gameEvent);
}
