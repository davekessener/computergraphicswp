/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer;

import wp_cg.backend.misc.Logger;
import wp_cg.backend.misc.Scene;
import wp_cg.backend.jogl.OpenGL;
import wp_cg.backend.scenegraph.InnerNode;
import wp_cg.lab.grammar.Evaluator;
import wp_cg.lab.grammar.GamefieldConverter;
import wp_cg.lab.plugins.BreakingBrickPlugin;
import wp_cg.lab.plugins.CameraControllerPlugin;
import wp_cg.lab.plugins.CollisionPlugin;
import wp_cg.lab.plugins.FlyingMonsterPlugin;
import wp_cg.lab.plugins.WorldMeshGeneratorPlugin;
import wp_cg.platformer.levelgenerator.Gamefield;
import wp_cg.platformer.levelgenerator.Grammar;

/**
 * This scene is the entry point for the platformer game.
 */
public class PlatformerScene extends Scene
{

	/**
	 * World to be used.
	 */
	private Game game;

	/**
	 * Represents the state of the key (pressed key set).
	 */
	private int keyState;

	private int frameCounter = 0;
	private long timeSinceLastMeasurement = 0;

	/**
	 * Key constants
	 */
	private static final int KEY_LEFT = 2;
	private static final int KEY_RIGHT = 4;
	private static final int KEY_JUMP = 8;
	private static final int KEY_DOWN = 16;

	public PlatformerScene(String vShaderFilename, String fShaderFilename)
	{
		super(100, vShaderFilename, fShaderFilename);
	}

	@Override
	public void onSetup(InnerNode rootNode)
	{

		OpenGL.instance().enableBlending(true);

		GameEventQueue.getInstance().freeze();

		game = new Game(getRoot());

		// Default
		game.addPlugin(new BackgroundPlugin());
		game.addPlugin(new DisplayStatePlugin());

		// Aufgabe 1 + 2
		game.addPlugin(new WorldMeshGeneratorPlugin());
		// Aufgabe 2
		// game.addPlugin(new PlayerAnimationPlugin());
		// Aufgabe 3
		game.addPlugin(new CameraControllerPlugin());
		// Aufgabe 4
		game.addPlugin(new FlyingMonsterPlugin());
		// Aufgabe 5
		game.addPlugin(new CollisionPlugin());
		// Aufgabe 6
		game.addPlugin(new BreakingBrickPlugin());

		// Level laden/Aufgabe 7
//		game.fromJson("misc/platformer/level01.json");

		Grammar grammar = Grammar.fromGrammarFile("misc/platformer/grammar.grammar");
		Evaluator eval = new Evaluator(grammar);
		Gamefield gamefield = new Gamefield(25, 25);

		Logger.getInstance().log(gamefield.toString());

		gamefield = eval.derive(gamefield, 10);

		Logger.getInstance().log(gamefield.toString());

//		GamefieldImporter gameFieldImporter = new GamefieldImporter(game, gamefield);
//
//		gameFieldImporter.importFromGameField();
		
		game.fromJson(GamefieldConverter.convert(gamefield));

		game.initPlugins();

		GameEventQueue.getInstance().unfreeze();
	}

	@Override
	public void onTimerTick(int counter)
	{
	}

	@Override
	public void onSceneRedraw()
	{
		frameCounter++;
		int NUMBER_OF_FRAMES = 100;
		if(frameCounter >= NUMBER_OF_FRAMES)
		{
			long currentTime = System.currentTimeMillis();
			double seconds = (currentTime - timeSinceLastMeasurement) / 1000.0;
			int fps = (int) (NUMBER_OF_FRAMES / seconds);
			Logger.getInstance().log("FPS: " + fps);
			timeSinceLastMeasurement = currentTime;
			frameCounter = 0;
		}
	}

	@Override
	public void onKeyPressed(int keyCode)
	{
		super.onKeyPressed(keyCode);
		switch (keyCode)
		{
		case 32: // Space
			game.throwHazelnut();
			break;
		case 80: // 'p'
			game.getDynamicGameState().togglePause();
			break;
		case 38:
			// Move jump
			keyState = keyState | KEY_JUMP;
			break;
		case 37:
			// Move left
			keyState = keyState | KEY_LEFT;
			break;
		case 39:
			// Move right
			keyState = keyState | KEY_RIGHT;
			break;
		case 40:
			// Move down
			keyState = keyState | KEY_DOWN;
			break;
		default:
			Logger.getInstance().log("Unhandled key pressed: " + keyCode);
		}
		playerMoveStateFromKeyState();
	}

	@Override
	public void onKeyReleased(int keyCode)
	{
		super.onKeyReleased(keyCode);
		switch (keyCode)
		{
		case 38:
			// Jump
			keyState = keyState & ~KEY_JUMP;
			break;
		case 37:
			// Move left
			keyState = keyState & ~KEY_LEFT;
			break;
		case 39:
			// Move right
			keyState = keyState & ~KEY_RIGHT;
			break;
		case 40:
			// Move down
			keyState = keyState & ~KEY_DOWN;
			break;
		default:
			// Logger.getInstance().log("Unhandled key released: " + keyCode);
		}
		playerMoveStateFromKeyState();
	}

	/**
	 * Determines the player move state from the key state.
	 */
	private void playerMoveStateFromKeyState()
	{
		if(((keyState & KEY_LEFT) != 0) && ((keyState & KEY_JUMP) != 0))
		{
			game.getPlayer().setMoveState(Player.Move.JUMP_LEFT);
		} else if(((keyState & KEY_RIGHT) != 0) && ((keyState & KEY_JUMP) != 0))
		{
			game.getPlayer().setMoveState(Player.Move.JUMP_RIGHT);
		} else if((keyState & KEY_LEFT) != 0)
		{
			game.getPlayer().setMoveState(Player.Move.LEFT);
		} else if((keyState & KEY_RIGHT) != 0)
		{
			game.getPlayer().setMoveState(Player.Move.RIGHT);
		} else if((keyState & KEY_JUMP) != 0)
		{
			game.getPlayer().setMoveState(Player.Move.JUMP);
		} else if((keyState & KEY_DOWN) != 0)
		{
			game.getPlayer().setMoveState(Player.Move.DOWN);
		} else
		{
			game.getPlayer().setMoveState(Player.Move.IDLE);
		}
	}
}
