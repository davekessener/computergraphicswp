/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import wp_cg.backend.math.Matrix;
import wp_cg.backend.math.Vector;
import wp_cg.backend.mesh.*;
import wp_cg.backend.misc.Logger;
import wp_cg.backend.scenegraph.TransformationNode;
import wp_cg.backend.scenegraph.TriangleMeshNode;
import wp_cg.lab.scenegraph.ScaleAnimationNode;

/**
 * Container for all the player-related information.
 */
public class Player extends GameObject {

    public static final Vector JUMP_VELOCITY = Vector.VECTOR_3_Y.multiply(0.5);

    /**
     * This velocity vector is used to controll the non-interactive part of movement (e.g. jump curve)
     */
    private Vector velocity = new Vector(0, 0, 0);

    /**
     * Move direction.
     */
    public enum Move {
        IDLE, LEFT, RIGHT, JUMP, JUMP_LEFT, JUMP_RIGHT, DOWN
    }

    /**
     * Current movement state of the player
     */
    private Move moveState = Move.IDLE;

    /**
     * This parameter describes the jump curve in [0;1]
     */
    private double jumpParameter = 0;

    /**
     * This flag indicates that the player is currently jumping
     */
    private boolean isJumping = false;

    /**
     * Start point and direction of the jump
     */
    private Vector jumpStart = new Vector(0, 0, 0), jumpDirection = new Vector(0, 0, 0);

    /**
     * Orients the player mesh.
     */
    private TransformationNode playerOrientationNode;

    /**
     * Counter for the number of collected coins.
     */
    private int numberOfCollectedCoins = 0;

    /**
     * Number of lives of the player
     */
    private int numberOfLifes = Constants.START_NUMBER_PLAYER_LIFES;

    /**
     * After loosing a life, the player is invulnerable for some time. This flag indicates this state.
     */
    private boolean playerIsInvulnerable = false;

    /**
     * The player looks into this direction.
     */
    private Vector orientation = new Vector(1,0,0);

    public Player(Vector pos) {
        super(Type.PLAYER, pos);
        playerOrientationNode = new TransformationNode(Matrix.createIdentityMatrix4());
        ObjReader reader = new ObjReader();
        List<TriangleMesh> meshes = reader.read("meshes/player_squirrel.obj");
        TriangleMesh baseMesh = (TriangleMesh) meshes.get(0);

        // technically not correct, if player is rotated!
        setup(baseMesh.getBoundingBox());

        // Non-animated mesh
        playerOrientationNode.addChild(ScaleAnimationNode.animate(new TriangleMeshNode(baseMesh), 60 * 4, 0.1, 20));
//        playerOrientationNode.addChild(new TriangleMeshNode(baseMesh));

        addChild(playerOrientationNode);
    }

    public void updateOrientation(Vector orientation) {
    	this.orientation.set(orientation.x(), orientation.y(), orientation.z());
    	
        playerOrientationNode.setTransformation(Matrix.makeHomogenious(new Matrix(
                orientation,
                Vector.VECTOR_3_Y,
                orientation.cross(Vector.VECTOR_3_Y))));
    }

    public Vector getVelocity() {
        return velocity;
    }

    public Move getMoveState() {
        return moveState;
    }

    public void setMoveState(Move newMove) {
        moveState = newMove;
    }

    public void setVelocity(Vector velocity) {
        this.velocity.copy(velocity);
    }

    public void updateVelocity(Vector update) {
        velocity.addSelf(update);
    }

    public void replaceMesh(AnimatedMesh animMeshNode) {
        playerOrientationNode.clearChildren();
        playerOrientationNode.addChild(animMeshNode);
    }

    @Override
    public void updateGameState() {
        if (isJumping) {
            setPosition(evalJumpPosition());
        }
    }

    /**
     * Evaluate the player position during the jump
     *
     * @return
     */
    private Vector evalJumpPosition() {
        jumpParameter += Constants.INTEGRATION_STEP_SIZE * 3;
        if (jumpParameter > 1) {
            isJumping = false;
        }

        return jumpStart.
                add(jumpDirection.multiply(jumpParameter * Constants.PLAYER_JUMP_LENGTH).
                        add(Vector.VECTOR_3_Y.multiply((1 - (jumpParameter * 2 - 1) * (jumpParameter * 2 - 1)) * Constants.PLAYER_JUMP_HEIGHT)));
    }

    public boolean isJumping() {
        return isJumping;
    }

    public void jump(Vector jumpStart, Vector jumpDirection) {
        this.jumpStart.copy(jumpStart);
        this.jumpDirection.copy(jumpDirection);
        this.isJumping = true;
        jumpParameter = 0;
    }

    public void reset() {
        numberOfCollectedCoins = 0;
        numberOfLifes = Constants.START_NUMBER_PLAYER_LIFES;
        GameEventQueue.getInstance().emitEvent(new GameEvent(GameEvent.Type.PLAYER_STATE_CHANGED, null));
        playerIsInvulnerable = false;
    }

    public void coinCollected() {
        numberOfCollectedCoins++;
        Logger.getInstance().log("Number of collected coins: " + numberOfCollectedCoins);
        GameEventQueue.getInstance().emitEvent(new GameEvent(GameEvent.Type.PLAYER_STATE_CHANGED, null));
    }

    /**
     * The player looses a live
     */
    public void playerLoosesLife() {
        if (!playerIsInvulnerable) {
            numberOfLifes--;
            playerIsInvulnerable = true;
            GameEventQueue.getInstance().emitEvent(new GameEvent(GameEvent.Type.PLAYER_STATE_CHANGED, null));
            Logger.getInstance().log("Player lost a life. Lifes remaining: " + numberOfLifes + ". Is invulnerable now.");
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    playerIsInvulnerable = false;
                    Logger.getInstance().log("Player is vulnerable again.");
                }
            }, Constants.SECONDS_INVULNERABLE * 1000);
        }
    }

    public int getNumberOfLifes() {
        return numberOfLifes;
    }

    public int getNumberOfCoins() {
        return numberOfCollectedCoins;
    }

    public Vector getOrientation() {
        return orientation;
    }
}

