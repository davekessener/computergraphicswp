/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import wp_cg.backend.math.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * A special brick which allows to switch between two segments.
 */
public class SwitchBrick extends Brick {
    private static final String JSON_SWITCHES = "switches";
    private static final String JSON_FROM = "from";
    private static final String JSON_TO = "to";

    /**
     * Inner class to represent a switch between two game state segments.
     */
    public class Switch {
        //public int from, to;
        public Vector from = new Vector(0, 0, 0), to = new Vector(0, 0, 0);

        public Switch(Vector from, Vector to) {
            this.from.copy(from);
            this.to.copy(to);
        }
    }

    private List<Switch> switches = new ArrayList<Switch>();

    public SwitchBrick() {
    }

    public int getNumberOfSwitches() {
        return switches.size();
    }

    public Switch getSwitch(int index) {
        return switches.get(index);
    }


    @Override
    public void fromJson(JSONObject jsonObject) {
        super.fromJson(jsonObject);
        JSONArray jsonSwitches = (JSONArray) jsonObject.get(JSON_SWITCHES);
        for (int i = 0; i < jsonSwitches.size(); i++) {
            JSONObject jsonSwitch = (JSONObject) jsonSwitches.get(i);
            JSONArray fromArray = (JSONArray) jsonSwitch.get(JSON_FROM);
            JSONArray toArray = (JSONArray) jsonSwitch.get(JSON_TO);
            Vector from = vectorFromJson(fromArray);
            Vector to = vectorFromJson(toArray);
            addSwitch(from, to);
        }
        setType(Type.SWITCH);
    }

    /**
     * Create a 3D vector from a Json array.
     */
    public static Vector vectorFromJson(JSONArray fromArray) {
        return new Vector(((Double) fromArray.get(0)), ((Double) fromArray.get(1)), ((Double) fromArray.get(2)));
    }

    public void addSwitch(Vector from, Vector to) {
        switches.add(new Switch(from, to));
    }

    /**
     * Handle all switches.
     */
    public void forEach(Consumer<Switch> switchConsumer) {
        switches.forEach(switchConsumer);
    }
}
