/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer.levelgenerator;

import wp_cg.lab.math.IVec3;
import wp_cg.platformer.Constants;

/**
 * The Gamefield represents a word in the grammar and contains a grid of
 * symbols.
 */
public class Gamefield
{
	/**
	 * Data structure for the grid cells
	 */
	private class Cell
	{
		public String symbol = null;
		public boolean isNew = false;
	}

	/**
	 * z x x cells.
	 */
	private final Cell[][] mCells;

	public Gamefield(int resX, int resZ)
	{
		if(resX <= 0 || resZ <= 0)
		{
			throw new IllegalArgumentException();
		}
		
		mCells = new Cell[resZ][resX];
		
		for(int x = 0 ; x < mCells.length ; x++)
		{
			for(int z = 0 ; z < mCells[0].length ; z++)
			{
				mCells[z][x] = new Cell();
			}
		}
		
		set(0, 0, "B");
		set(1, 0, "\u2192");
		set(2, 0, "E");
		
		reset();
	}

	@Override
	public String toString()
	{
		String result = "\n+";
		
		for(int x = 0 ; x < mCells.length ; x++)
		{
			result += "-";
		}
		
		result += "> z\n|\n";
		
		for(int x = 0 ; x < mCells.length ; x++)
		{
			result += "|   ";
			
			for(int z = 0 ; z < mCells[0].length ; z++)
			{
				String s = getSymbol(mCells[z][x].symbol);
				
				if(s.equals(Constants.GRAMMAR_SWITCH_X_Z))
				{
					s = "V";
				}
				else if(s.equals(Constants.GRAMMAR_SWITCH_Z_X))
				{
					s = ">";
				}
				else if(s.equals(Constants.GRAMMAR_LEFTRIGHT))
				{
					s = "-";
				}
				else if(s.equals(Constants.GRAMMAR_UPDOWN))
				{
					s = "|";
				}
				
				result += s;
			}
			
			result += "\n";
		}
		
		result += "v\nx\n";
		
		return result;
	}

	/**
	 * Returns the symbol used to print the game field cell.
	 */
	private String getSymbol(String s)
	{
		return (s == null) ? " " : s;
	}

	/**
	 * Sets the cell value at (x,z)
	 */
	public void set(int x, int z, String symbol)
	{
		mCells[z][x].symbol = symbol;
		mCells[z][x].isNew = true;
	}

	/**
	 * Return the resolution of the field in z-direction.
	 */
	public int getResZ()
	{
		return mCells.length;
	}

	/**
	 * Return the resolution of the field in x-direction.
	 */
	public int getResX()
	{
		return mCells[0].length;
	}

	public String getSymbol(int x, int z)
	{
		return mCells[z][x].symbol;
	}
	
	public boolean contains(IVec3 v)
	{
		return v.X >= 0 && v.Z >= 0 && v.X < getResX() && v.Z < getResZ();
	}
	
	public boolean areFree(IVec3 start, IVec3 end)
	{
		if(!contains(start) || !contains(end))
			throw new IllegalArgumentException();

		return IVec3.stream(start, new IVec3(end.X + 1, start.Y + 1, end.Z + 1))
					.allMatch(i -> i.equals(start) || getSymbol(i.X, i.Z) == null);
	}

	/**
	 * Check if this cell was newly created in the current derivation.
	 */
	public boolean isNew(int x, int z)
	{
		return mCells[z][x].isNew;
	}

	/**
	 * Reset all isNew flags.
	 */
	public void reset()
	{
		for(int x = 0 ; x < mCells.length ; x++)
		{
			for(int z = 0 ; z < mCells[0].length ; z++)
			{
				mCells[x][z].isNew = false;
			}
		}
	}
}
