/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer.levelgenerator;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import wp_cg.backend.misc.AssetPath;
import wp_cg.backend.misc.Logger;
import wp_cg.lab.grammar.Rule;

public class Grammar
{
	private final List<IRule> mRules;

	private Grammar(List<IRule> rules)
	{
		mRules = Collections.unmodifiableList(rules);
	}
	
	public Stream<IRule> rules() { return mRules.stream(); }

	public static Grammar fromGrammarFile(String fn)
	{
		List<IRule> rules = new ArrayList<>();
		String path = AssetPath.getInstance().getPathToAsset(fn);

		try (
				FileInputStream raw = new FileInputStream(path);
				InputStreamReader stream = new InputStreamReader(raw, "UTF-8");
				BufferedReader in = new BufferedReader(stream)
			)
		{
			for(String line ; (line = in.readLine()) != null ;)
			{
				rules.add(Rule.fromString(line));
			}
		}
		catch(IOException e)
		{
			Logger.getInstance().log("Failed to parseFile rule file" + fn);

			throw new RuntimeException(e);
		}

		return new Grammar(rules);
	}
}
