/**
 * Prof. Philipp Jenke
 * Hochschule für Angewandte Wissenschaften (HAW), Hamburg
 *
 * Basis Framework für das "WP Computergrafik".
 */

package wp_cg.platformer.levelgenerator;

import java.util.List;
import java.util.function.BiConsumer;

import wp_cg.lab.math.IVec3;

/**
 * Shared interface for all rules of the form pred --> succ : dir
 */
public interface IRule {

    /**
     * Expand directions for rule successors.
     */
    enum DIR {
        X, Z
    }

    /**
     * Returns true if the rule can be applied to the symbol (symbol equals rule predecessor).
     */
    boolean canBeAppliedTo(String symbol);

    /**
     * Returns the List of sucessor symbols
     */
    List<String> getSucc();

    /**
     * Returns the expand direction of the successors.
     */
    DIR getDir();
    
    public default IVec3 calculateEnd(IVec3 start)
    {
    	boolean in_x = (getDir() == DIR.X);
    	int dx = (in_x ? 1 : 0), dz = (in_x ? 0 : 1);
    	int c = getSucc().size() - 1;
    	
    	return start.add(new IVec3(dx * c, 1, dz * c));
    }
    
    public default void forEachSucc(IVec3 start, BiConsumer<IVec3, String> f)
    {
    	boolean in_x = (getDir() == DIR.X);
    	int dx = (in_x ? 1 : 0), dz = (in_x ? 0 : 1);
    	int count = 0;
    	
    	for(String s : getSucc())
    	{
    		f.accept(start.add(new IVec3(dx * count, 0, dz * count)), s);
    		
    		++count;
    	}
    }
}
